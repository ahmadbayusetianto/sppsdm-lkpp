<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSyaratPelatihanFungsionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('bangprof')->create('t_syarat_pelatihan_fungsional', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->integer('m_kategori_layanan_id');
            $table->integer('m_pelatihan_id');
            $table->integer('m_kategori_jabatan_id');
            $table->integer('m_tingkat_penting_id');
            $table->timestamps();
            $table->bigInteger('created_by');
            $table->bigInteger('updated_by');
            $table->bigInteger('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_syarat_pelatihan_fungsional');
    }
}
