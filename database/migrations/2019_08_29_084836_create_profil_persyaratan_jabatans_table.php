<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilPersyaratanJabatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('bangprof')->create('p_profil_persyaratan_jabatan', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->integer('p_profil_ikhtisar_jabatan_id');
            $table->integer('t_syarat_pendidikan_id');
            $table->integer('t_syarat_pelatihan_manajerial_id');
            $table->integer('t_syarat_pelatihan_teknis_id');
            $table->integer('t_syarat_pelatihan_fungsional_id');
            $table->integer('t_syarat_sertifikasi_id');
            $table->integer('t_syarat_pengalaman_id');
            $table->integer('t_syarat_pangkat_id');
            $table->timestamps();
            $table->bigInteger('created_by');
            $table->bigInteger('updated_by');
            $table->bigInteger('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('p_profil_persyaratan_jabatan');
    }
}
