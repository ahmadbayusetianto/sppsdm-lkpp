<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIkhtisarJabatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('bangprof')->create('t_ikhtisar_jabatan', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->string('kode');
            $table->integer('m_kompetensi_jenis_id');
            $table->string('m_ruang_lingkup_id');
            $table->timestamps();
            $table->bigInteger('created_by');
            $table->bigInteger('updated_by');
            $table->bigInteger('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_ikhtisar_jabatan');
    }
}
