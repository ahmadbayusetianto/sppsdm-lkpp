<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegulasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::create('m_regulasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomor');
            $table->string('tahun');
            $table->text('tentang');
            $table->timestamps();
            $table->bigIncrements('created_by');
            $table->bigIncrements('updated_by');
            $table->bigIncrements('deleted_by');
        });*/

        Schema::connection('bangprof')->create('m_regulasi', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->string('nomor');
            $table->string('tahun');
            $table->text('tentang');
            $table->text('dokumen');
            $table->timestamps();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_regulasi');
    }
}
