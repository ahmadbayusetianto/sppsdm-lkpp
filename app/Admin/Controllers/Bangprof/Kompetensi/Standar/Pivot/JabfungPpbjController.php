<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Standar\Pivot;

//master >> KAMUS
use App\Models\Bangprof\Kompetensi\Kamus\Master\Perilaku;
use App\Models\Bangprof\Kompetensi\Kamus\Master\KompetensiJabatan;
use App\Models\Bangprof\Kompetensi\Kamus\Master\Jenis;
use App\Models\Bangprof\Kompetensi\Kamus\Master\RuangLingkup;
use App\Models\Bangprof\Kompetensi\Kamus\Master\Deskripsi;

//master >> STANDAR
use App\Models\Bangprof\Kompetensi\Standar\Master\Jenjang;
use App\Models\Bangprof\Kompetensi\Standar\Master\BidangIlmu;
use App\Models\Bangprof\Kompetensi\Standar\Master\Pangkat;
use App\Models\Bangprof\Kompetensi\Standar\Master\Pelatihan;
use App\Models\Bangprof\Kompetensi\Standar\Master\Sertifikasi;
use App\Models\Bangprof\Kompetensi\Standar\Master\TingkatPenting;
use App\Models\Bangprof\Kompetensi\Standar\Master\PengalamanKerja;

//transaksi >> KAMUS
use App\Models\Bangprof\Kompetensi\Kamus\Transaksi\KompetensiPerilaku;

//transaksi >> STANDAR
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\ProfilStandarKompetensi;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\ProfilPersyaratanJabatan;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\ProfilIkhtisarJabatan;

use App\Models\Bangprof\Kompetensi\Standar\Transaksi\IkhtisarJabatan;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\SyaratPendidikan;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\SyaratPangkat;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\SyaratPelatihanManajerial;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\SyaratPelatihanTeknis;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\SyaratPelatihanFungsional;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\SyaratSertifikasi;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\SyaratPengalamanKerja;

//template
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Widgets\Tab;
use Encore\Admin\Widgets\Table;
use Illuminate\Support\Facades\DB;

class JabfungPpbjController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Standar Kompetensi Jabatan Fungsional PPBJ';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $tab        = new Tab();

        $konten1    = ProfilIkhtisarJabatan::select('p_profil_ikhtisar_jabatan.id', 'mj.jabatan', 'mj.bidang', 'mkj.kategori', 
                        'mkj.tingkat', 'p_profil_ikhtisar_jabatan.t_ikhtisar_jabatan_id')
                        ->join('m_jabatan AS mj', 'mj.id', '=', 'p_profil_ikhtisar_jabatan.m_jabatan_id')
                        ->join('m_kategori_jabatan AS mkj', 'mkj.id', '=', 'p_profil_ikhtisar_jabatan.m_kategori_jabatan_id')
                        ->get();

        foreach ($konten1 as $row1)
        {
            # IKHTISAR JABATAN
            $konten2 =  IkhtisarJabatan::select('t_ikhtisar_jabatan.kode', 'mkj.alias', 't_ikhtisar_jabatan.m_ruang_lingkup_id')
                        ->join('m_kompetensi_jenis AS mkj', 'mkj.id', '=', 't_ikhtisar_jabatan.m_kompetensi_jenis_id')
                        ->find($row1->t_ikhtisar_jabatan_id);

            $ikhtisarjabatan   = '';
            
            foreach ($konten2 as $row2) 
            {
                $konten3 = RuangLingkup::select('ruang_lingkup')->find($row2->m_ruang_lingkup_id);

                $ruang  = '';
                
                foreach ($konten3 as $row3) 
                {   
                    $ruang .= '<li>'.$row3['ruang_lingkup'].'</li>';
                }

                $ikhtisarjabatan .= '<td width="20%" style="padding:5px">'.$row2['kode'].' <b>'.$row2['alias'].' :</b><ol style="margin-left:-25px">'.$ruang.'</ol></td>';
            }

            # END OF IKHTISAR JABATAN
            # ------------------------------------------------------------------------
            # STANDAR KOMPETENSI >> MANAJERIAL
            $konten4 = ProfilStandarKompetensi::select('tkl.id AS kompetensi_level_id', 'tkn.m_kompetensi_jabatan_id', 'tkj.m_kompetensi_jenis_id', 'tkl.m_deskripsi_id')
                        ->join('t_kompetensi_level AS tkl', 'tkl.id', '=', 'p_profil_standar_kompetensi.t_kompetensi_level_id')
                        ->join('t_kompetensi_jenis AS tkj', 'tkj.id', '=', 'tkl.t_kompetensi_jenis_id')
                        ->join('t_kompetensi_nama AS tkn', 'tkn.id', '=', 'tkj.t_kompetensi_nama_id')
                        ->where('p_profil_ikhtisar_jabatan_id', $row1->id)
                        ->where('tkn.m_kompetensi_jabatan_id', '1')
                        ->get();

            $manajerial = '';

            foreach ($konten4 as $row4) 
            {
                unset($indikator_manajerial);

                $konten5 = KompetensiPerilaku::select('mp.perilaku')
                            ->join('m_perilaku AS mp', 'mp.id', 't_kompetensi_perilaku.m_perilaku_id')
                            ->where('t_kompetensi_perilaku.t_kompetensi_level_id', $row4['kompetensi_level_id'])
                            ->get();

                foreach ($konten5 as $row5) 
                {
                    $indikator_manajerial[] = '<li>'.$row5['perilaku'].'</li>';
                }

                $manajerial .= '<tr>
                                    <td class="text-center">'.Jenis::select('alias')->find($row4['m_kompetensi_jenis_id'])->alias.'</td>
                                    <td class="text-center">'.Deskripsi::select('level')->find($row4['m_deskripsi_id'])->level.'</td>
                                    <td style="padding:5px">'.Deskripsi::select('deskripsi')->find($row4['m_deskripsi_id'])->deskripsi.'</td>
                                    <td><ol style="margin-left:-20px">'.implode('', $indikator_manajerial).'</ol></td>
                                </tr>';

                /*$manajerial .= '<tr>
                                    <td>a</td>
                                    <td class="text-center">b</td>
                                    <td></td>
                                    <td width="4%" class="text-center">'.Perilaku::select('kode')->find($row4['m_perilaku_id'])->kode.'</td>
                                    <td>'.Perilaku::select('perilaku')->find($row4['m_perilaku_id'])->perilaku.'</td>
                                </tr>';*/
            }
            
            # END OF STANDAR KOMPETENSI >> MANAJERIAL

            # STANDAR KOMPETENSI >> SOSIAL KULTURAL
            $konten6 = ProfilStandarKompetensi::select('tkl.id AS kompetensi_level_id', 'tkn.m_kompetensi_jabatan_id', 'tkj.m_kompetensi_jenis_id', 'tkl.m_deskripsi_id')
                        ->join('t_kompetensi_level AS tkl', 'tkl.id', '=', 'p_profil_standar_kompetensi.t_kompetensi_level_id')
                        ->join('t_kompetensi_jenis AS tkj', 'tkj.id', '=', 'tkl.t_kompetensi_jenis_id')
                        ->join('t_kompetensi_nama AS tkn', 'tkn.id', '=', 'tkj.t_kompetensi_nama_id')
                        ->where('p_profil_ikhtisar_jabatan_id', $row1->id)
                        ->where('tkn.m_kompetensi_jabatan_id', '2')
                        ->get();

            $sosial = '';

            foreach ($konten6 as $row6) 
            {
                unset($indikator_sosial);

                $konten7 = KompetensiPerilaku::select('mp.perilaku')
                            ->join('m_perilaku AS mp', 'mp.id', 't_kompetensi_perilaku.m_perilaku_id')
                            ->where('t_kompetensi_perilaku.t_kompetensi_level_id', $row6['kompetensi_level_id'])
                            ->get();

                foreach ($konten7 as $row7)
                {
                    $indikator_sosial[] = '<li>'.$row7['perilaku'].'</li>';
                }

                $sosial .= '<tr>
                                <td class="text-center">'.Jenis::select('alias')->find($row6['m_kompetensi_jenis_id'])->alias.'</td>
                                <td class="text-center">'.Deskripsi::select('level')->find($row6['m_deskripsi_id'])->level.'</td>
                                <td style="padding:5px">'.Deskripsi::select('deskripsi')->find($row6['m_deskripsi_id'])->deskripsi.'</td>
                                <td><ol style="margin-left:-20px">'.implode('', $indikator_sosial).'</ol></td>
                            </tr>';
            }
            
            # END OF STANDAR KOMPETENSI >> SOSIAL KULTURAL

            # STANDAR KOMPETENSI >> TEKNIS
            $konten8 = ProfilStandarKompetensi::select('tkl.id AS kompetensi_level_id', 'tkn.m_kompetensi_jabatan_id', 'tkj.m_kompetensi_jenis_id', 'tkl.m_deskripsi_id')
                        ->join('t_kompetensi_level AS tkl', 'tkl.id', '=', 'p_profil_standar_kompetensi.t_kompetensi_level_id')
                        ->join('t_kompetensi_jenis AS tkj', 'tkj.id', '=', 'tkl.t_kompetensi_jenis_id')
                        ->join('t_kompetensi_nama AS tkn', 'tkn.id', '=', 'tkj.t_kompetensi_nama_id')
                        ->where('p_profil_ikhtisar_jabatan_id', $row1->id)
                        ->where('tkn.m_kompetensi_jabatan_id', '3')
                        ->get();

            $teknis = '';

            foreach ($konten8 as $row8)
            {
                unset($indikator_teknis);

                $konten9 = KompetensiPerilaku::select('mp.perilaku')
                            ->join('m_perilaku AS mp', 'mp.id', 't_kompetensi_perilaku.m_perilaku_id')
                            ->where('t_kompetensi_perilaku.t_kompetensi_level_id', $row8['kompetensi_level_id'])
                            ->get();

                foreach ($konten9 as $row9)
                {
                    $indikator_teknis[] = '<li>'.$row9['perilaku'].'</li>';
                }

                $teknis .= '<tr>
                                <td class="text-center">'.Jenis::select('alias')->find($row8['m_kompetensi_jenis_id'])->alias.'</td>
                                <td class="text-center">'.Deskripsi::select('level')->find($row8['m_deskripsi_id'])->level.'</td>
                                <td style="padding:5px">'.Deskripsi::select('deskripsi')->find($row8['m_deskripsi_id'])->deskripsi.'</td>
                                <td><ol style="margin-left:-20px">'.implode('', $indikator_teknis).'</ol></td>
                            </tr>';
            }
            # END OF STANDAR KOMPETENSI >> SOSIAL KULTURAL

            # PERSYARATAN JABATAN
            $konten10 = ProfilPersyaratanJabatan::select('p_profil_persyaratan_jabatan.p_profil_ikhtisar_jabatan_id', 
                        'tsp.id AS pendidikan_id', 'tst.id AS pangkat_id', 
                        'spm.id AS manajerial_id', 'p_profil_persyaratan_jabatan.tingkat_penting_manajerial',
                        'spt.id AS teknis_id', 'p_profil_persyaratan_jabatan.tingkat_penting_teknis',
                        'spf1.id AS fungsional1_id', 'p_profil_persyaratan_jabatan.tingkat_penting_fungsional1',
                        'spf2.id AS fungsional2_id', 'p_profil_persyaratan_jabatan.tingkat_penting_fungsional2',
                        'spf3.id AS fungsional3_id', 'p_profil_persyaratan_jabatan.tingkat_penting_fungsional3',
                        'ss1.id AS sertifikasi1_id', 'p_profil_persyaratan_jabatan.tingkat_penting_sertifikasi1',
                        'ss2.id AS sertifikasi2_id', 'p_profil_persyaratan_jabatan.tingkat_penting_sertifikasi2',
                        'spk.id AS pengalaman_kerja_id', 'p_profil_persyaratan_jabatan.tingkat_penting_pengalaman_kerja')
                        ->join('t_syarat_pendidikan AS tsp', 'tsp.id', '=', 'p_profil_persyaratan_jabatan.t_syarat_pendidikan_id')
                        ->join('t_syarat_pelatihan_manajerial AS spm', 'spm.id', '=', 'p_profil_persyaratan_jabatan.t_syarat_pelatihan_manajerial_id')
                        ->join('t_syarat_pelatihan_teknis AS spt', 'spt.id', '=', 'p_profil_persyaratan_jabatan.t_syarat_pelatihan_teknis_id')
                        ->join('t_syarat_pelatihan_fungsional AS spf1', 'spf1.id', '=', 'p_profil_persyaratan_jabatan.t_syarat_pelatihan_fungsional1')
                        ->leftJoin('t_syarat_pelatihan_fungsional AS spf2', 'spf2.id', '=', 'p_profil_persyaratan_jabatan.t_syarat_pelatihan_fungsional2')
                        ->leftJoin('t_syarat_pelatihan_fungsional AS spf3', 'spf3.id', '=', 'p_profil_persyaratan_jabatan.t_syarat_pelatihan_fungsional3')
                        ->join('t_syarat_sertifikasi AS ss1', 'ss1.id', '=', 'p_profil_persyaratan_jabatan.t_syarat_sertifikasi1')
                        ->leftJoin('t_syarat_sertifikasi AS ss2', 'ss2.id', '=', 'p_profil_persyaratan_jabatan.t_syarat_sertifikasi2')
                        ->join('t_syarat_pengalaman_kerja AS spk', 'spk.id', '=', 'p_profil_persyaratan_jabatan.t_syarat_pengalaman_kerja_id')
                        ->join('t_syarat_pangkat AS tst', 'tst.id', '=', 'p_profil_persyaratan_jabatan.t_syarat_pangkat_id')
                        ->where('p_profil_persyaratan_jabatan.p_profil_ikhtisar_jabatan_id', $row1->id)
                        ->get();
            
            foreach ($konten10 as $row10) 
            {
                # SYARAT PENDIDIKAN
                $jenjang  = SyaratPendidikan::select('mj.jenjang')
                            ->join('m_jenjang AS mj', 'mj.id', '=', 't_syarat_pendidikan.m_jenjang_id')
                            ->find($row10['pendidikan_id'])->jenjang;

                $konten11 = SyaratPendidikan::select('m_bidang_ilmu_id')->find($row10['pendidikan_id']);
                
                $ilmu = BidangIlmu::select('bidang_ilmu')->find($konten11['m_bidang_ilmu_id']);

                unset($bi);

                foreach ($ilmu as $row) 
                {
                    $bi[] = $row['bidang_ilmu'];
                }
                #END OF SYARAT PENDIDIKAN

                # PELATIHAN >> MANAJERIAL
                $pm             = SyaratPelatihanManajerial::select('m_pelatihan_id', 'level')->find($row10['manajerial_id']);
                $p_manajerial   = Pelatihan::select('pelatihan')->find($pm['m_pelatihan_id'])->pelatihan.' Level '.$pm['level'];
                $t_manajerial   = TingkatPenting::select('tingkat_penting')->find($row10['tingkat_penting_manajerial'])->tingkat_penting;
                # END OF MANAJERIAL

                # PELATIHAN >> TEKNIS
                $pt             = SyaratPelatihanTeknis::select('m_pelatihan_id', 'level')->find($row10['teknis_id']);
                $p_teknis       = Pelatihan::select('pelatihan')->find($pt['m_pelatihan_id'])->pelatihan;
                $t_teknis       = TingkatPenting::select('tingkat_penting')->find($row10['tingkat_penting_teknis'])->tingkat_penting;
                # END OF TEKNIS

                # PELATIHAN >> FUNGSIONAL
                if($row1->id == '1')
                {
                    $p_rowspan      = 5;
                    $pf1            = SyaratPelatihanFungsional::select('m_pelatihan_id')->find($row10['fungsional1_id']);
                    $p_fungsional1  = Pelatihan::select('pelatihan')->find($pf1['m_pelatihan_id'])->pelatihan;
                    $t_fungsional1  = TingkatPenting::select('tingkat_penting')->find($row10['tingkat_penting_fungsional1'])->tingkat_penting;

                    $pf2            = SyaratPelatihanFungsional::select('m_pelatihan_id')->find($row10['fungsional2_id']);
                    $p_fungsional2  = Pelatihan::select('pelatihan')->find($pf2['m_pelatihan_id'])->pelatihan;
                    $t_fungsional2  = TingkatPenting::select('tingkat_penting')->find($row10['tingkat_penting_fungsional2'])->tingkat_penting;

                    $pf3            = SyaratPelatihanFungsional::select('m_pelatihan_id')->find($row10['fungsional3_id']);
                    $p_fungsional3  = Pelatihan::select('pelatihan')->find($pf3['m_pelatihan_id'])->pelatihan;
                    $t_fungsional3  = TingkatPenting::select('tingkat_penting')->find($row10['tingkat_penting_fungsional3'])->tingkat_penting;

                    $p_fungsional   = '<tr>
                                            <td rowspan="3" style="padding:5px">3. Fungsional</td>
                                            <td style="padding:5px">'.$p_fungsional1.'</td>
                                            <td style="padding:5px">'.$t_fungsional1.'</td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px">'.$p_fungsional2.'</td>
                                            <td style="padding:5px">'.$t_fungsional2.'</td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px">'.$p_fungsional3.' '.$row1->tingkat.'</td>
                                            <td style="padding:5px">'.$t_fungsional3.'</td>
                                        </tr>';
                }
                else
                {
                    $p_rowspan      = 3;
                    $pf1            = SyaratPelatihanFungsional::select('m_pelatihan_id')->find($row10['fungsional1_id']);
                    $p_fungsional1  = Pelatihan::select('pelatihan')->find($pf1['m_pelatihan_id'])->pelatihan;
                    $t_fungsional1  = TingkatPenting::select('tingkat_penting')->find($row10['tingkat_penting_fungsional1'])->tingkat_penting;

                    $p_fungsional   = '<tr>
                                            <td style="padding:5px">3. Fungsional</td>
                                            <td style="padding:5px">'.$p_fungsional1.' '.$row1->tingkat.'</td>
                                            <td style="padding:5px">'.$t_fungsional1.'</td>
                                        </tr>';
                }
                # END OF FUNGSIONAL

                # SERTIFIKASI >> FUNGSIONAL
                if($row1->id == '1')
                {
                    $rowspan        = 2;
                    
                    $ss1            = SyaratSertifikasi::select('m_sertifikasi_id')->find($row10['sertifikasi1_id']);
                    $p_sertifikasi1 = Sertifikasi::select('sertifikasi')->find($ss1['m_sertifikasi_id'])->sertifikasi;
                    $t_sertifikasi1 = TingkatPenting::select('tingkat_penting')->find($row10['tingkat_penting_sertifikasi1'])->tingkat_penting;

                    $ss2            = SyaratSertifikasi::select('m_sertifikasi_id')->find($row10['sertifikasi2_id']);
                    $p_sertifikasi2 = Sertifikasi::select('sertifikasi')->find($ss2['m_sertifikasi_id'])->sertifikasi;
                    $t_sertifikasi2 = TingkatPenting::select('tingkat_penting')->find($row10['tingkat_penting_sertifikasi2'])->tingkat_penting;

                    $sertifikasi    = '<tr>
                                            <td rowspan="2" style="padding:5px">C. Sertifikasi</td>
                                            <td rowspan="2" style="padding:5px">Fungsional</td>
                                            <td style="padding:5px">'.$p_sertifikasi1.'</td>
                                            <td style="padding:5px">'.$t_sertifikasi1.'</td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px">'.$p_sertifikasi2.' '.$row1->tingkat.'</td>
                                            <td style="padding:5px">'.$t_sertifikasi2.'</td>
                                        </tr>';
                }
                else
                {
                    $ss1            = SyaratSertifikasi::select('m_sertifikasi_id')->find($row10['sertifikasi1_id']);
                    $p_sertifikasi1 = Sertifikasi::select('sertifikasi')->find($ss1['m_sertifikasi_id'])->sertifikasi;
                    $t_sertifikasi1 = TingkatPenting::select('tingkat_penting')->find($row10['tingkat_penting_sertifikasi1'])->tingkat_penting;

                    $sertifikasi    = '<tr>
                                            <td style="padding:5px">C. Sertifikasi</td>
                                            <td style="padding:5px">Fungsional</td>
                                            <td style="padding:5px">'.$p_sertifikasi1.' '.$row1->tingkat.'</td>
                                            <td style="padding:5px">'.$t_sertifikasi1.'</td>
                                        </tr>';
                }

                # SYARAT PENGALAMAN KERJA
                $pengalaman     = SyaratPengalamanKerja::select('m_pengalaman_kerja_id')->find($row10['pengalaman_kerja_id']);
                $p_pengalaman   = PengalamanKerja::selectRaw('IF(tahun2 IS NULL, CONCAT(tahun1, " ", ket1), CONCAT("- ", tahun1, " ", ket1, "<br>- ", tahun2, " ", ket2)) as thn')->find($pengalaman['m_pengalaman_kerja_id'])->thn;
                $t_pengalaman   = TingkatPenting::select('tingkat_penting')->find($row10['pengalaman_kerja_id'])->tingkat_penting;
                # END OF SYARAT PENGALAMAN KERJA                

                # SYARAT PANGKAT
                $pangkat        = SyaratPangkat::select('pangkat_min', 'pangkat_maks')->find($row10['pangkat_id']);
                $pangkatmin     = Pangkat::selectRaw('CONCAT(pangkat, " (", golongan, ruang, ")") AS min')->find($pangkat['pangkat_min'])->min;
                $pangkatmaks    = Pangkat::selectRaw('CONCAT(pangkat, " (", golongan, ruang, ")") AS maks')->find($pangkat['pangkat_maks'])->maks;
                # END OF SYARAT PANGKAT

                # INDIKATOR KINERJA JABATAN
                $ikhtisar = ProfilIkhtisarJabatan::select('t_ikhtisar_jabatan_id')
                            ->where('id', $row10['p_profil_ikhtisar_jabatan_id'])
                            ->get();

                foreach ($ikhtisar as $row) 
                {
                    $konten1 = IkhtisarJabatan::find($row->t_ikhtisar_jabatan_id);

                    $konten2 = '';
                    
                    foreach ($konten1 as $baris) 
                    {
                        $konten4 = '';    
                        $konten3 = RuangLingkup::find($baris->m_ruang_lingkup_id);
                        
                        foreach($konten3 as $brs)
                        {
                            $konten4 .= '<li>'.$brs->ruang_lingkup.'</li>';
                        }

                        $konten2 .= '<b>Dokumen Hasil '.Jenis::find($baris->m_kompetensi_jenis_id)->alias.' :</b><ol style="margin-left:-25px;">'.$konten4.'</ol>';
                    }
                }
                # END OF INDIKATOR KINERJA JABATAN
            }
            # END OF PERSYARATAN JABATAN
            # ------------------------------------------------------------------------
            $tab->add($row1->tingkat, 
                '<div class="row" style="margin:0px">
                    <table border="0" width="100%">
                        <tr>
                            <td width="15%">Nama Jabatan</td>
                            <td>:</td>
                            <td width="84%">'.$row1->jabatan.' '.$row1->tingkat.'</td>
                        </tr>
                        <tr>
                            <td>Kelompok Jabatan</td>
                            <td>:</td>
                            <td>'.$row1->kategori.'</td>
                        </tr>
                        <tr>
                            <td>Urusan Pemerintahan</td>
                            <td>:</td>
                            <td>'.$row1->bidang.'</td>
                        </tr>
                        <tr>
                            <td>Kode Jabatan</td>
                            <td>:</td>
                            <td>?</td>
                        </tr>
                    </table>
                </div>
                <div><hr></div>
                <div class="row" style="margin:0px">
                    <div class="alert" style="background:#eadbae; margin-bottom:5px; padding-top:5px; padding-bottom:5px;">Ikhtisar Jabatan</div>
                    <table border="0" width="100%">
                        <tr valign="top">
                            <td style="padding:5px">Pengelola Pengadaan Barang/Jasa Pertama, melaksanakan tugas Pengadaan Barang/Jasa dengan lingkup pekerjaan:
                            </td>'
                            .$ikhtisarjabatan.'
                        </tr>
                    </table>
                </div>
                <br>
                <div class="row" style="margin:0px">
                    <div class="alert" style="background:#eadbae; margin-bottom:5px; padding-top:5px; padding-bottom:5px;">Standar Kompetensi</div>
                    <table border="1" width="100%">
                        <tr>
                            <td colspan="5" style="padding:5px; background:#ede7d1">A. Kompetensi Manajerial</td>
                        </tr>
                        <tr>
                            <td width="15%" class="text-center" style="padding:5px">Kompetensi</td>
                            <td width="5%" class="text-center" style="padding:5px">Level</td>
                            <td width="35%" class="text-center" style="padding:5px">Deskripsi</td>
                            <td class="text-center" colspan="2" style="padding:5px">Indikator Kompetensi</td>
                        </tr>'
                        .$manajerial.'                    
                        <tr>
                            <td colspan="5" style="padding:5px; background:#ede7d1">B. Sosial Kultural</td>
                        </tr>
                        <tr>
                            <td class="text-center" style="padding:5px">Kompetensi</td>
                            <td class="text-center" style="padding:5px">Level</td>
                            <td class="text-center" style="padding:5px">Deskripsi</td>
                            <td class="text-center" colspan="2" style="padding:5px">Indikator Kompetensi</td>
                        </tr>'
                        .$sosial.'
                        <tr>
                            <td colspan="5" style="padding:5px; background:#ede7d1">C. Teknis</td>
                        </tr>
                        <tr>
                            <td class="text-center" style="padding:5px">Kompetensi</td>
                            <td class="text-center" style="padding:5px">Level</td>
                            <td class="text-center" style="padding:5px">Deskripsi</td>
                            <td class="text-center" colspan="2" style="padding:5px">Indikator Kompetensi</td>
                        </tr>'
                        .$teknis.'
                    </table>
                </div>
                <br>
                <div class="row" style="margin:0px">
                    <div class="alert" style="background:#eadbae; margin-bottom:5px; padding-top:5px; padding-bottom:5px;">Persyaratan Jabatan</div>
                    <table border="1" width="100%">
                        <tr>
                            <td colspan="2" class="text-center" style="padding:5px">Jenis Persyaratan</td>
                            <td width="20%" class="text-center">Uraian</td>
                            <td class="text-center">Tingkat Pentingnya Terhadap Jabatan</td>
                        </tr>
                        <tr>
                            <td rowspan="2" width="20%" style="padding:5px">A. Pendidikan</td>
                            <td width="15%" style="padding:5px">1. Jenjang</td>
                            <td colspan="2" style="padding:5px">'.$jenjang.'</td>
                        </tr>
                        <tr>
                            <td style="padding:5px">2. Bidang Ilmu</td>
                            <td colspan="2" style="padding:5px">'.implode(', ', $bi).'.</td>
                        </tr>
                        <tr>
                            <td rowspan="'.$p_rowspan.'" style="padding:5px">B. Pelatihan</td>
                            <td style="padding:5px">1. Manajerial</td>
                            <td style="padding:5px">'.$p_manajerial.'</td>
                            <td style="padding:5px">'.$t_manajerial.'</td>
                        </tr>
                        <tr>
                            <td style="padding:5px">2. Teknis</td>
                            <td style="padding:5px">'.$p_teknis.'</td>
                            <td style="padding:5px">'.$t_teknis.'</td>
                        </tr>
                        '.$p_fungsional.$sertifikasi.'
                        <tr>
                            <td colspan="2" style="padding:5px">D. Pengalaman Kerja</td>
                            <td style="padding:5px">'.$p_pengalaman.'</td>
                            <td style="padding:5px">'.$t_pengalaman.'</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding:5px">E. Pangkat</td>
                            <td colspan="2" style="padding:5px">'.$pangkatmin.' - '.$pangkatmaks.'</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding:5px">F. Indikator Kinerja Jabatan</td>
                            <td colspan="2" style="padding:5px">'.$konten2.'</td>
                        </tr>
                    </table>
                </div>
                ');
        }

        return $tab->render();
    }
}