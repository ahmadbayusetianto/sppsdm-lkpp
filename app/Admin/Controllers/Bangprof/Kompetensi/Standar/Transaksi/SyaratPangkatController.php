<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Standar\Transaksi;

use App\Models\Bangprof\Kompetensi\Standar\Master\Pangkat;

use App\Models\Bangprof\Kompetensi\Standar\Transaksi\SyaratPangkat;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;

use Illuminate\Support\Facades\Route;
use App\Admin\Actions\Post\Replicate;
use App\Admin\Actions\Post\BatchReplicate;

class SyaratPangkatController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Standar > Syarat Pangkat';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new SyaratPangkat);

        $grid->column('id', __('Id'))->hide();
        
        $grid->column('pangkat_min', __('Min. Pangkat'))->display(function($id){
            return Pangkat::find($id)->golongan.'/'.Pangkat::find($id)->ruang.' '.Pangkat::find($id)->pangkat;
        });
        
        $grid->column('pangkat_maks', __('Maks. Pangkat'))->display(function($id){
            return Pangkat::find($id)->golongan.'/'.Pangkat::find($id)->ruang.' '.Pangkat::find($id)->pangkat;
        });
        
        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();
        $grid->column('deleted_at', __('Deleted at'))->hide();

        $grid->actions(function ($actions) {
            $actions->add(new Replicate);
        });

        $grid->batchActions(function ($batch) {
            $batch->add(new BatchReplicate());
            $batch->disableDelete();
        });
        
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(SyaratPangkat::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('pangkat_min', __('Min. Pangkat'));
        $show->field('pangkat_maks', __('Maks. Pangkat'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('deleted_by', __('Deleted by'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new SyaratPangkat);
        $action = explode('.', Route::currentRouteName())[1];

        $min    = Pangkat::selectRaw('id, CONCAT(golongan, "/", ruang, " ", pangkat) as min')->pluck('min', 'id');
        $maks   = Pangkat::selectRaw('id, CONCAT(golongan, "/", ruang, " ", pangkat) as maks')->pluck('maks', 'id');

        if($action == 'create')
        {
            $form->select('pangkat_min', __('Min. Pangkat'))->options($min);
            $form->select('pangkat_maks', __('Maks. Pangkat'))->options($maks);
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id); 
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->select('pangkat_min', __('Min. Pangkat'))->options($min);
            $form->select('pangkat_maks', __('Maks. Pangkat'))->options($maks);
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);
        }

        return $form;
    }
}
