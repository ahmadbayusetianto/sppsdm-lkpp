<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Standar\Transaksi;

#Kamus - Master
use App\Models\Bangprof\Kompetensi\Kamus\Master\Deskripsi;
use App\Models\Bangprof\Kompetensi\Kamus\Master\Perilaku;

#Standar - Master
use App\Models\Bangprof\Kompetensi\Standar\Master\Jabatan;
use App\Models\Bangprof\Kompetensi\Standar\Master\KategoriJabatan;

#Kamus - Transaksi
use App\Models\Bangprof\Kompetensi\Kamus\Transaksi\KompetensiLevel;
use App\Models\Bangprof\Kompetensi\Kamus\Transaksi\KompetensiPerilaku;

#Standar - Transaksi
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\ProfilIkhtisarJabatan;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\ProfilStandarKompetensi;

#System
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;

use Illuminate\Support\Facades\Route;
use App\Admin\Actions\Post\Replicate;
use App\Admin\Actions\Post\BatchReplicate;

class ProfilStandarKompetensiController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Standar > Profil Standar Kompetensi';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ProfilStandarKompetensi);

        $grid->column('id', __('Id'))->hide();
        
        $grid->column('Jabatan')->display(function(){
            
            $lvlId      = ProfilStandarKompetensi::find($this->id)->p_profil_ikhtisar_jabatan_id;
            $jbtId      = ProfilIkhtisarJabatan::select('mj.jabatan', 'mkj.alias', 'mkj.tingkat')
                            ->join('m_jabatan AS mj', 'mj.id', '=', 'p_profil_ikhtisar_jabatan.m_jabatan_id')
                            ->join('m_kategori_jabatan AS mkj', 'mkj.id', '=', 'p_profil_ikhtisar_jabatan.m_kategori_jabatan_id')
                            ->where('p_profil_ikhtisar_jabatan.id', $lvlId)->get();
                            
            foreach ($jbtId as $row) 
            {
                $jabatan    = $row->jabatan;
                $alias      = $row->alias;
                $tingkat    = $row->tingkat;
            }

            return $alias.' '.$jabatan.' <span class="label label-warning">'.$tingkat.'</span>';

        })->style('text-align:center; max-width:200px');

        // ----------

        $grid->column('Kompetensi')->display(function(){
            
            $lvlId      = ProfilStandarKompetensi::find($this->id)->t_kompetensi_level_id;
            $jbtId      = KompetensiLevel::select('mkj.jenis', 'mkjb.kompetensi_jabatan')
                            ->join('t_kompetensi_jenis AS tkj', 'tkj.id', '=', 't_kompetensi_level.t_kompetensi_jenis_id')
                            ->join('m_kompetensi_jenis AS mkj', 'mkj.id', '=', 'tkj.m_kompetensi_jenis_id')
                            ->join('t_kompetensi_nama AS tkn', 'tkn.id', '=', 'tkj.t_kompetensi_nama_id')
                            ->join('m_kompetensi_jabatan AS mkjb', 'mkjb.id', '=', 'tkn.m_kompetensi_jabatan_id')
                            ->where('t_kompetensi_level.id', $lvlId)->get();
                            
            foreach ($jbtId as $row) 
            {
                $komp    = $row->kompetensi_jabatan;
                $jenis   = $row->jenis;
            }

            return $jenis.'<br><span class="label label-warning">'.$komp.'</span>';

        })->style('text-align:center; max-width:200px');

        // ----------

        $grid->column('Deskripsi')->display(function(){
            
            $lvlId  = ProfilStandarKompetensi::find($this->id)->t_kompetensi_level_id;
            $kmpId  = KompetensiLevel::find($lvlId)->m_deskripsi_id;
            $level  = Deskripsi::find($kmpId)->level;
            $desk   = Deskripsi::find($kmpId)->deskripsi;

            return '<div style="text-align:justify">Level '.$level.'<br>'.$desk.'</div>';

        })->style('text-align:center; max-width:300px');

        // ----------

        $grid->column('Indikator Kompetensi')->display(function(){
            
            $lvlId  = ProfilStandarKompetensi::find($this->id)->t_kompetensi_level_id;
            $kmp    = KompetensiLevel::find($lvlId)->id;
            $plk    = KompetensiPerilaku::where('t_kompetensi_level_id', $kmp)
                        ->join('m_perilaku AS mp', 'mp.id', '=', 't_kompetensi_perilaku.m_perilaku_id')
                        ->get();

            $kalakuan = "";

            foreach ($plk as $row) 
            {
                #$kalakuan .= '<div style="text-align:justify">'.$row->perilaku.'</div>';
                $kalakuan .= '<li>'.$row->perilaku.'</li>';
            }
            
            return '<div style="text-align:justify"><ol>'.$kalakuan.'</ol></div>';

        })->style('text-align:center; max-width:500px');

        // ----------

        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();
        $grid->column('deleted_at', __('Deleted at'))->hide();

        $grid->actions(function ($actions) {
            $actions->add(new Replicate);
        });

        $grid->batchActions(function ($batch) {
            $batch->add(new BatchReplicate());
            $batch->disableDelete();
        });

        $grid->filter(function($filter){
            $filter->disableIdFilter();

            $filter->column(1/2, function($filter){
                $filter->where(function ($query) {
                    $query->whereHas('jabatan', function ($query)
                    {
                        $query->join('m_kategori_jabatan AS mkj', 'mkj.id', '=', 'p_profil_ikhtisar_jabatan.m_kategori_jabatan_id')
                            ->where('tingkat', 'like', "%{$this->input}%");
                    });
                }, 'Jabatan');

                $filter->where(function ($query) {
                    $query->whereHas('kompetensi', function ($query)
                    {
                        $query->join('t_kompetensi_jenis AS tkj', 'tkj.id', '=', 't_kompetensi_level.t_kompetensi_jenis_id')
                            ->join('t_kompetensi_nama AS tkn', 'tkn.id', '=', 'tkj.t_kompetensi_nama_id')
                            ->join('m_kompetensi_jenis AS mkjs', 'mkjs.id', '=', 'tkj.m_kompetensi_jenis_id')
                            ->join('m_kompetensi_jabatan AS mkjb', 'mkjb.id', '=', 'tkn.m_kompetensi_jabatan_id')
                            ->where('mkjs.jenis', 'like', "%{$this->input}%")
                            ->orWhere('mkjb.kompetensi_jabatan', 'like', "%{$this->input}%");
                    });
                }, 'Kompetensi');
            });

            $filter->column(1/2, function($filter){
                $filter->where(function ($query) {
                    $query->whereHas('kompetensi', function ($query)
                    {
                        $query->join('m_deskripsi AS md', 'md.id', '=', 't_kompetensi_level.m_deskripsi_id')
                            ->where('md.level', 'like', "%{$this->input}%");
                    });
                }, 'Level');

                $filter->where(function ($query) {
                    $query->whereHas('kompetensi', function ($query)
                    {
                        $query->join('m_deskripsi AS md', 'md.id', '=', 't_kompetensi_level.m_deskripsi_id')
                            ->where('md.deskripsi', 'like', "%{$this->input}%");
                    });
                }, 'Deskripsi');

                $filter->where(function ($query) {
                    $query->whereHas('kompetensi', function ($query)
                    {
                        $query->join('t_kompetensi_jenis AS tkj', 'tkj.id', '=', 't_kompetensi_level.t_kompetensi_jenis_id')
                            ->join('t_kompetensi_perilaku AS tkp', 'tkp.t_kompetensi_level_id', '=', 't_kompetensi_level.id')
                            ->join('m_perilaku AS mp', 'mp.id', '=', 'tkp.m_perilaku_id')
                            ->where('mp.perilaku', 'like', "%{$this->input}%");
                    });
                }, 'Perilaku');
            });
        });


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ProfilStandarKompetensi::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('kode', __('Kode'));
        $show->field('p_profil_ikhtisar_jabatan_id', __('Jabatan'));
        $show->field('p_kompetensi_kamus_id', __('Kamus kompetensi'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('deleted_by', __('Deleted by'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form    = new Form(new ProfilStandarKompetensi);
        $action  = explode('.', Route::currentRouteName())[1];

        $jabatan = ProfilIkhtisarJabatan::selectRaw('p_profil_ikhtisar_jabatan.id, CONCAT(kode, ": ", kj.alias, " ", 
                        mj.jabatan, " ", kj.tingkat) as jbtn')
                        ->join('m_jabatan AS mj', 'mj.id', '=', 'p_profil_ikhtisar_jabatan.m_jabatan_id')
                        ->join('m_kategori_jabatan AS kj', 'kj.id', '=', 'p_profil_ikhtisar_jabatan.m_kategori_jabatan_id')
                        ->pluck('jbtn', 'id');
 
        $level   = KompetensiLevel::selectRaw('t_kompetensi_level.id, CONCAT(mkjb.kompetensi_jabatan, ", Level ", 
                        ds.level, " : ", mkjs.alias) as lvl')
                        ->join('m_deskripsi AS ds', 'ds.id', '=', 't_kompetensi_level.m_deskripsi_id')
                        ->join('t_kompetensi_jenis AS tkj', 'tkj.id', '=', 't_kompetensi_level.t_kompetensi_jenis_id')
                        ->join('t_kompetensi_nama AS tkn', 'tkn.id', '=', 'tkj.t_kompetensi_nama_id')
                        ->join('m_kompetensi_jabatan AS mkjb', 'mkjb.id', '=', 'tkn.m_kompetensi_jabatan_id')
                        ->join('m_kompetensi_jenis AS mkjs', 'mkjs.id', '=', 'tkj.m_kompetensi_jenis_id')
                        ->pluck('lvl', 'id');

        if($action == 'create')
        {
            $form->select('p_profil_ikhtisar_jabatan_id', __('Jabatan'))->options($jabatan);
            $form->select('t_kompetensi_level_id', __('Level'))->options($level);
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id);
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->select('p_profil_ikhtisar_jabatan_id', __('Jabatan'))->options($jabatan);
            $form->select('t_kompetensi_level_id', __('Level'))->options($level);
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);   
        }
        
        return $form;
    }
}
