<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Standar\Transaksi;

use App\Models\Bangprof\Kompetensi\Kamus\Master\RuangLingkup;

use App\Models\Bangprof\Kompetensi\Standar\Master\Jabatan;
use App\Models\Bangprof\Kompetensi\Standar\Master\KategoriJabatan;

use App\Models\Bangprof\Kompetensi\Standar\Transaksi\IkhtisarJabatan;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\ProfilIkhtisarJabatan;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;

use Illuminate\Support\Facades\Route;
use App\Admin\Actions\Post\Replicate;
use App\Admin\Actions\Post\BatchReplicate;

class ProfilIkhtisarJabatanController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Standar > Profil Ikhtisar Jabatan';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ProfilIkhtisarJabatan);

        #$grid->column('id', __('Id'))->hide();
        $grid->column('kode', __('Kode'))->editable();

        $grid->m_jabatan_id('Jabatan')->display(function($jbtnId){
            
            $jbtn = Jabatan::find($jbtnId)->jabatan;
            $tgkt = ProfilIkhtisarJabatan::find($this->id)->m_kategori_jabatan_id;
            $tk   = KategoriJabatan::find($tgkt)->tingkat;
            
            return $jbtn.' '.$tk;
        });

        $grid->m_kategori_jabatan_id('Kelompok Jabatan')->display(function ($ktgr){
            $kategori = KategoriJabatan::find($ktgr)->kategori;
            
            return $kategori;

        })->style('text-align:center');

        $grid->column('t_ikhtisar_jabatan_id', __('Ikhtisar Jabatan'))->display(function ($bi) {

            $bi = array_map(function ($isi) {
                
                $data1 = IkhtisarJabatan::select('kj.alias', 't_ikhtisar_jabatan.kode' , 't_ikhtisar_jabatan.m_ruang_lingkup_id')
                        ->join('m_kompetensi_jenis AS kj', 'kj.id', '=', 't_ikhtisar_jabatan.m_kompetensi_jenis_id')
                        ->find($isi);
                
                $data2 = '';
                
                foreach ($data1['m_ruang_lingkup_id'] as $row) 
                {
                    $data2 .= '<li>'.RuangLingkup::select('ruang_lingkup')->find($row)->ruang_lingkup.'.</li>';
                }

                return '<b>['.$data1['kode'].']</b> '.$data1['alias'].' : <br><ol style="margin-left:-25px">'.$data2.'</ol>';

            }, $bi);
            
            return join('', $bi);

        })->style('max-width:500px;');
        

        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();
        $grid->column('deleted_at', __('Deleted at'))->hide();
        
        $grid->actions(function ($actions) {
            $actions->add(new Replicate);
        });

        $grid->batchActions(function ($batch) {
            $batch->add(new BatchReplicate());
            $batch->disableDelete();
        });

        $grid->filter(function($filter){
            $filter->disableIdFilter();
            
            $filter->where(function ($query) {
                $query->whereHas('jabatan', function ($query)
                {
                    $query->where('tingkat', 'like', "%{$this->input}%");
                });
            }, 'Jabatan');

            $filter->where(function ($query) {
                $query->whereHas('ikhtisar', function ($query)
                {
                    $query->join('m_kompetensi_jenis AS mkj', 'mkj.id', '=', 't_ikhtisar_jabatan.m_kompetensi_jenis_id')
                        ->where('mkj.alias', 'like', "%{$this->input}%");
                });
            }, 'Ikhtisar Jabatan');
        });

        $grid->enableHotKeys();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ProfilIkhtisarJabatan::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('kode', __('Kode'));
        $show->field('m_jabatan_id', __('M jabatan id'));
        $show->field('m_kategori_jabatan_id', __('M kategori jabatan id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('deleted_by', __('Deleted by'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form       = new Form(new ProfilIkhtisarJabatan);
        $action     = explode('.', Route::currentRouteName())[1];

        $jabatan    = Jabatan::selectRaw('id, CONCAT(jabatan, ", Bidang : ", bidang) as jbtn')->pluck('jbtn', 'id');
        $kategori   = KategoriJabatan::selectRaw('id, CONCAT(kategori, " ", tingkat) as ktgr')->pluck('ktgr', 'id');
        $ikhtisar   = IkhtisarJabatan::selectRaw('id, CONCAT(kode) as ij')->pluck('ij', 'id');

        if($action == 'create')
        {
            $form->text('kode', __('Kode'));
            $form->select('m_jabatan_id', __('Jabatan'))->options($jabatan);
            $form->select('m_kategori_jabatan_id', __('Kategori Jabatan'))->options($kategori);
            $form->multipleSelect('t_ikhtisar_jabatan_id', __('Ikhtisar Jabatan'))->options($ikhtisar);
            #$form->ckeditor('ikhtisar_jabatan', __('Ikhtisar Jabatan'));
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id);
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->text('kode', __('Kode'));
            $form->select('m_jabatan_id', __('Jabatan'))->options($jabatan);
            $form->select('m_kategori_jabatan_id', __('Kategori Jabatan'))->options($kategori);
            $form->multipleSelect('t_ikhtisar_jabatan_id', __('Ikhtisar Jabatan'))->options($ikhtisar);
            #$form->ckeditor('ikhtisar_jabatan', __('Ikhtisar Jabatan'));
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);   
        }

        return $form;
    }
}
