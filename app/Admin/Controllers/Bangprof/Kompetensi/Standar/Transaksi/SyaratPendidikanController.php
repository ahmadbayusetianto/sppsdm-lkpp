<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Standar\Transaksi;

use App\Models\Bangprof\Kompetensi\Standar\Master\Jenjang;
use App\Models\Bangprof\Kompetensi\Standar\Master\BidangIlmu;

use App\Models\Bangprof\Kompetensi\Standar\Transaksi\SyaratPendidikan;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;

use Illuminate\Support\Facades\Route;
use App\Admin\Actions\Post\Replicate;
use App\Admin\Actions\Post\BatchReplicate;

class SyaratPendidikanController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Standar > Syarat Pendidikan';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new SyaratPendidikan);

        $grid->column('id', __('Id'))->hide();
        
        $grid->column('m_jenjang_id', __('Jenjang'))->display(function($id){
            return Jenjang::find($id)->jenjang;
        });
        
        /*$grid->column('m_bidang_ilmu_id', __('Bidang Ilmu'))->display(function($id){
            
            $bi = '';

            foreach($id as $line) 
            {
                $ilmu = BidangIlmu::select('bidang_ilmu')->where('id', $line)->get();

                foreach ($ilmu as $row) 
                {
                    $bi .= $row->bidang_ilmu.', ';
                }
            }

            return $bi;
        });*/

        $grid->column('m_bidang_ilmu_id', __('Bidang Ilmu'))->display(function ($bi) {

            $bi = array_map(function ($isi) {
                
                $ilmu = BidangIlmu::select('bidang_ilmu')->find($isi);
                
                return $ilmu['bidang_ilmu'];

            }, $bi);

            return join(', ', $bi).'.';

        })->style('max-width:500px;');
        
        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();
        $grid->column('deleted_at', __('Deleted at'))->hide();

        $grid->actions(function ($actions) {
            $actions->add(new Replicate);
        });

        $grid->batchActions(function ($batch) {
            $batch->add(new BatchReplicate());
            $batch->disableDelete();
        });

        $grid->enableHotKeys();
        
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(SyaratPendidikan::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('m_jenjang_id', __('M jenjang id'));
        $show->field('m_bidang_ilmu_id', __('M bidang ilmu id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('deleted_by', __('Deleted by'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new SyaratPendidikan);
        $action = explode('.', Route::currentRouteName())[1];

        $jenjang    = Jenjang::select('id', 'jenjang')->pluck('jenjang', 'id');
        $ilmu       = BidangIlmu::select('id', 'bidang_ilmu')->pluck('bidang_ilmu', 'id');

        if($action == 'create')
        {
            $form->select('m_jenjang_id', __('Jenjang'))->options($jenjang);
            $form->multipleSelect('m_bidang_ilmu_id', __('Bidang Ilmu'))->options($ilmu);
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id); 
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->select('m_jenjang_id', __('Jenjang'))->options($jenjang);
            $form->multipleSelect('m_bidang_ilmu_id', __('Bidang Ilmu'))->options($ilmu);
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);
        }

        return $form;
    }
}
