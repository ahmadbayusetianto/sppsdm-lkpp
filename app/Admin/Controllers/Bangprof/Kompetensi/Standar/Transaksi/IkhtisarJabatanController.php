<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Standar\Transaksi;

use App\Models\Bangprof\Kompetensi\Kamus\Master\Jenis;
use App\Models\Bangprof\Kompetensi\Kamus\Master\RuangLingkup;

use App\Models\Bangprof\Kompetensi\Standar\Transaksi\IkhtisarJabatan;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;

use Illuminate\Support\Facades\Route;
use App\Admin\Actions\Post\Replicate;
use App\Admin\Actions\Post\BatchReplicate;

class IkhtisarJabatanController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Ikhtisar Jabatan';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new IkhtisarJabatan);

        $grid->column('id', __('Id'))->hide();
        $grid->column('kode', __('Kode'))->editable();
        $grid->column('m_kompetensi_jenis_id', __('Jenis Kompetensi'))->display(function($id){
            return Jenis::find($id)->alias;
        });
        
        $grid->column('m_ruang_lingkup_id', __('Ruang Lingkup'))->display(function ($bi) {

            $bi = array_map(function ($isi) {
                
                $ilmu = RuangLingkup::select('ruang_lingkup')->find($isi);
                
                return $ilmu['ruang_lingkup'];

            }, $bi);
            
            return join(';<br>', $bi).'.';

        })->style('max-width:500px;');

        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();
        $grid->column('deleted_at', __('Deleted at'))->hide();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(IkhtisarJabatan::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('kode', __('Kode'));
        $show->field('m_kompetensi_jenis_id', __('M kompetensi jenis id'));
        $show->field('m_ruang_lingkup_id', __('M ruang lingkup id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('deleted_by', __('Deleted by'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new IkhtisarJabatan);
        $action = explode('.', Route::currentRouteName())[1];

        $jenis          = Jenis::select('id', 'jenis')->pluck('jenis', 'id');
        $ruanglingkup   = RuangLingkup::select('id', 'ruang_lingkup')->pluck('ruang_lingkup', 'id');

        if($action == 'create')
        {
            $form->text('kode', __('Kode'));
            $form->select('m_kompetensi_jenis_id', __('Kompetensi'))->options($jenis);
            $form->multipleSelect('m_ruang_lingkup_id', __('Ruang Lingkup'))->options($ruanglingkup);
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id); 
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->text('kode', __('Kode'));
            $form->select('m_kompetensi_jenis_id', __('Kompetensi'))->options($jenis);
            $form->multipleSelect('m_ruang_lingkup_id', __('Ruang Lingkup'))->options($ruanglingkup);
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);
        }

        return $form;
    }
}
