<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Standar\Transaksi;

use App\Models\Bangprof\Kompetensi\Standar\Master\PengalamanKerja;

use App\Models\Bangprof\Kompetensi\Standar\Transaksi\SyaratPengalamanKerja;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;

use Illuminate\Support\Facades\Route;
use App\Admin\Actions\Post\Replicate;
use App\Admin\Actions\Post\BatchReplicate;

class SyaratPengalamanKerjaController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Standar > Syarat Pengalaman Kerja';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new SyaratPengalamanKerja);

        $grid->column('id', __('Id'))->hide();
        
        $grid->column('m_pengalaman_kerja_id', __('Pengalaman Kerja'))->display(function($id){
            $tahun1 = PengalamanKerja::find($id)->tahun1;
            $tahun2 = PengalamanKerja::find($id)->tahun2;

            $ket1   = PengalamanKerja::find($id)->ket1;
            $ket2   = PengalamanKerja::find($id)->ket2;

            if($tahun2 == null)
            {
                return $tahun1.' Tahun';
            }
            else
            {
                return $tahun1.' '.$ket1.' '.$tahun2.' '.$ket2;
            }
        });

        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();
        $grid->column('deleted_at', __('Deleted at'))->hide();

        $grid->actions(function ($actions) {
            $actions->add(new Replicate);
        });

        $grid->batchActions(function ($batch) {
            $batch->add(new BatchReplicate());
            $batch->disableDelete();
        });
        
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(SyaratPengalamanKerja::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('m_pengalaman_kerja_id', __('Pengalaman Kerja'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('deleted_by', __('Deleted by'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form   = new Form(new SyaratPengalamanKerja);
        $action = explode('.', Route::currentRouteName())[1];

        $pengalaman    = PengalamanKerja::selectRaw('id, IF(tahun2 IS NULL, CONCAT(tahun1, " Tahun"), CONCAT(tahun1, " Tahun 
                            untuk Pengangkatan Pertama atau ", tahun2, " Tahun untuk Inpassing")) as thn')->pluck('thn', 'id');

        if($action == 'create')
        {
            $form->select('m_pengalaman_kerja_id', __('Pengalaman Kerja'))->options($pengalaman);
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id); 
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->select('m_pengalaman_kerja_id', __('Pengalaman Kerja'))->options($pengalaman);
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);
        }

        return $form;
    }
}
