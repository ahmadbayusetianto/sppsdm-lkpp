<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Standar\Transaksi;

use App\Models\Bangprof\KategoriLayanan;
use App\Models\Bangprof\Kompetensi\Standar\Master\Sertifikasi;

use App\Models\Bangprof\Kompetensi\Standar\Transaksi\SyaratSertifikasi;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;

use Illuminate\Support\Facades\Route;
use App\Admin\Actions\Post\Replicate;
use App\Admin\Actions\Post\BatchReplicate;

class SyaratSertifikasiController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Syarat Sertifikasi';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new SyaratSertifikasi);

        $grid->column('id', __('Id'))->hide();

        $grid->column('m_kategori_layanan_id', __('Jenis'))->display(function($id){
            return KategoriLayanan::find($id)->kategori_layanan;
        });

        $grid->column('m_sertifikasi_id', __('Sertifikasi'))->display(function($id){
            return Sertifikasi::find($id)->sertifikasi;
        });

        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();
        $grid->column('deleted_at', __('Deleted at'))->hide();

        $grid->actions(function ($actions) {
            $actions->add(new Replicate);
        });

        $grid->batchActions(function ($batch) {
            $batch->add(new BatchReplicate());
            $batch->disableDelete();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(SyaratSertifikasi::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('m_jenis_sertifikasi_id', __('Jenis'));
        $show->field('m_sertifikasi_id', __('Sertifikasi'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('deleted_by', __('Deleted by'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new SyaratSertifikasi);
        $action = explode('.', Route::currentRouteName())[1];

        $kl             = KategoriLayanan::select('id', 'kategori_layanan')->where('id', 3)->pluck('kategori_layanan', 'id');
        $sertifikasi    = Sertifikasi::select('id', 'sertifikasi')->pluck('sertifikasi', 'id');

        if($action == 'create')
        {
            $form->select('m_kategori_layanan_id', __('Kategori Layanan'))->options($kl);
            $form->select('m_sertifikasi_id', __('Sertifikasi'))->options($sertifikasi);
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id); 
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->select('m_kategori_layanan_id', __('Kategori Layanan'))->options($kl);
            $form->select('m_sertifikasi_id', __('Sertifikasi'))->options($sertifikasi);
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);
        }

        return $form;
    }
}
