<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Standar\Transaksi;

#Standar - Master
use App\Models\Bangprof\Kompetensi\Kamus\Master\RuangLingkup;
use App\Models\Bangprof\Kompetensi\Kamus\Master\Jenis;

use App\Models\Bangprof\Kompetensi\Standar\Master\BidangIlmu;
use App\Models\Bangprof\Kompetensi\Standar\Master\Jabatan;
use App\Models\Bangprof\Kompetensi\Standar\Master\Pelatihan;
use App\Models\Bangprof\Kompetensi\Standar\Master\Sertifikasi;
use App\Models\Bangprof\Kompetensi\Standar\Master\KategoriJabatan;
use App\Models\Bangprof\Kompetensi\Standar\Master\TingkatPenting;

#Standar - Transaksi
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\SyaratPendidikan;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\SyaratPelatihanManajerial;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\SyaratPelatihanTeknis;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\SyaratPelatihanFungsional;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\SyaratSertifikasi;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\SyaratPengalamanKerja;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\SyaratPangkat;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\IkhtisarJabatan;

#Standar - Pivot
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\ProfilIkhtisarJabatan;
use App\Models\Bangprof\Kompetensi\Standar\Transaksi\ProfilPersyaratanJabatan;

#System
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Facades\Admin;

use Illuminate\Support\Facades\Route;
use App\Admin\Actions\Post\Replicate;
use App\Admin\Actions\Post\BatchReplicate;

class ProfilPersyaratanJabatanController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Standar > Profil Persyaratan Jabatan';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */

    protected function grid()
    {
        $grid = new Grid(new ProfilPersyaratanJabatan);

        $grid->column('id', __('Id'))->hide();
        $grid->column('p_profil_ikhtisar_jabatan_id', __('Persyaratan Jabatan'))->display(function($id){
            
            $jbtn       = ProfilIkhtisarJabatan::select('mj.jabatan')
                            ->join('m_jabatan AS mj', 'mj.id', '=', 'p_profil_ikhtisar_jabatan.m_jabatan_id')
                            ->find($id)->jabatan;

            $kat        = ProfilIkhtisarJabatan::select('mkj.tingkat')
                            ->join('m_kategori_jabatan AS mkj', 'mkj.id', '=', 'p_profil_ikhtisar_jabatan.m_kategori_jabatan_id')
                            ->find($id)->tingkat;

            $pgkt_min   = SyaratPangkat::selectRaw('CONCAT(mp.pangkat, " (", mp.golongan, "/", mp.ruang, ")") AS min')
                            ->join('m_pangkat AS mp', 'mp.id', '=', 't_syarat_pangkat.pangkat_min')
                            ->find($this->t_syarat_pangkat_id)->min;

            $pgkt_maks  = SyaratPangkat::selectRaw('CONCAT(mp.pangkat, " (", mp.golongan, "/", mp.ruang, ")") AS maks')
                            ->join('m_pangkat AS mp', 'mp.id', '=', 't_syarat_pangkat.pangkat_maks')
                            ->find($this->t_syarat_pangkat_id)->maks;

            $jenjang    = SyaratPendidikan::selectRaw('mj.jenjang')
                            ->join('m_jenjang AS mj', 'mj.id', '=', 't_syarat_pendidikan.m_jenjang_id')
                            ->find($this->t_syarat_pendidikan_id)->jenjang;

            # Bidang Ilmu
            $bi     = SyaratPendidikan::select('m_bidang_ilmu_id')
                            ->find($this->t_syarat_pendidikan_id)->m_bidang_ilmu_id;

            $bi     = array_map(function ($isi){
                
                $ilmu = BidangIlmu::select('bidang_ilmu')->find($isi);
                
                return $ilmu['bidang_ilmu'];

            }, $bi);
            # end of Bidang Ilmu

            return '<div class="alert" style="margin-right:5px; text-align:left; background: #84a1be">'.$jbtn.' '.$kat.'</div>
                    <b>Pangkat :<br>Min. </b>'.$pgkt_min.'<br><b>Maks. </b>'.$pgkt_maks
                    .'<br><br><b>Pendidikan : </b><br>Min. '.$jenjang.'<br><div class="text-justify" style="margin-right:10px">'.join(', ', $bi).'.</div>';
        })->style('max-width:300px;');;

        $grid->column(__('Pelatihan'))->display(function(){
            
            $kat    = ProfilIkhtisarJabatan::select('mkj.tingkat')
                        ->join('m_kategori_jabatan AS mkj', 'mkj.id', '=', 'p_profil_ikhtisar_jabatan.m_kategori_jabatan_id')
                        ->find($this->p_profil_ikhtisar_jabatan_id)->tingkat;

            $manajerial = SyaratPelatihanManajerial::selectRaw('CONCAT(mp.pelatihan, " Level ", t_syarat_pelatihan_manajerial.level) AS level')
                            ->join('m_pelatihan AS mp', 'mp.id', '=', 't_syarat_pelatihan_manajerial.m_pelatihan_id')
                            ->find($this->t_syarat_pelatihan_manajerial_id)->level;

            $teknis = SyaratPelatihanTeknis::select('mp.pelatihan')
                        ->join('m_pelatihan AS mp', 'mp.id', '=', 't_syarat_pelatihan_teknis.m_pelatihan_id')
                        ->find($this->t_syarat_pelatihan_teknis_id)->pelatihan;

            if(is_null($this->t_syarat_pelatihan_fungsional1))
            {
                $fs1 = "";
            }
            else
            {
                $fungsional    = SyaratPelatihanFungsional::select('mp.id')
                        ->join('m_pelatihan AS mp', 'mp.id', '=', 't_syarat_pelatihan_fungsional.m_pelatihan_id')
                        ->find($this->t_syarat_pelatihan_fungsional1)->id;

                if($fungsional == '2')
                {
                    $fungsional    = SyaratPelatihanFungsional::select('mp.pelatihan')
                        ->join('m_pelatihan AS mp', 'mp.id', '=', 't_syarat_pelatihan_fungsional.m_pelatihan_id')
                        ->find($this->t_syarat_pelatihan_fungsional1)->pelatihan;

                    $fs1 = '<i class="fa fa-caret-right"></i> '.$fungsional.' '.$kat;
                }
                else
                {
                    $fungsional    = SyaratPelatihanFungsional::select('mp.pelatihan')
                        ->join('m_pelatihan AS mp', 'mp.id', '=', 't_syarat_pelatihan_fungsional.m_pelatihan_id')
                        ->find($this->t_syarat_pelatihan_fungsional1)->pelatihan;

                    $fs1 = '<i class="fa fa-caret-right"></i> '.$fungsional;
                }
            }

            if(is_null($this->t_syarat_pelatihan_fungsional2))
            {
                $fs2 = "";
            }
            else
            {
                $fungsional = SyaratPelatihanFungsional::select('mp.pelatihan')
                        ->join('m_pelatihan AS mp', 'mp.id', '=', 't_syarat_pelatihan_fungsional.m_pelatihan_id')
                        ->find($this->t_syarat_pelatihan_fungsional2)->pelatihan;

                $fs2 = '<i class="fa fa-caret-right"></i> '.$fungsional;
            }

            if(is_null($this->t_syarat_pelatihan_fungsional3))
            {
                $fs3 = "";
            }
            else
            {
                $fungsional = SyaratPelatihanFungsional::select('mp.pelatihan')
                        ->join('m_pelatihan AS mp', 'mp.id', '=', 't_syarat_pelatihan_fungsional.m_pelatihan_id')
                        ->find($this->t_syarat_pelatihan_fungsional3)->pelatihan;

                $fs3 = '<i class="fa fa-caret-right"></i> '.$fungsional.' '.$kat;
            }

            $tk1    = TingkatPenting::find($this->tingkat_penting_manajerial)->tingkat_penting;
            $tk2    = TingkatPenting::find($this->tingkat_penting_teknis)->tingkat_penting;

            if(is_null($this->tingkat_penting_fungsional1))
            {
                $tk3 = "";
            }
            else
            {
                $tk3    = TingkatPenting::find($this->tingkat_penting_fungsional1)->tingkat_penting;   
            }

            if(is_null($this->tingkat_penting_fungsional2))
            {
                $tk4 = "";
            }
            else
            {
                $tk4    = TingkatPenting::find($this->tingkat_penting_fungsional2)->tingkat_penting;
            }

            if(is_null($this->tingkat_penting_fungsional3))
            {
                $tk5 = "";
            }
            else
            {
                $tk5    = TingkatPenting::find($this->tingkat_penting_fungsional3)->tingkat_penting;
            }

            return '<div class="row" style="margin-right:5px; text-align:left; background: #ecf2f9">
                        <div class="col-md-12 alert text-center" style="margin:0px; background: #c8d9eb">Pelatihan</div>
                        <div class="col-md-12"><b>Manajerial</b></div>
                        <div class="col-md-9">
                            <i class="fa fa-caret-right"></i> '.$manajerial.'
                        </div>
                        <div class="col-md-3">
                            '.$tk1.'
                        </div>
                        <div class="col-md-12" style="margin:20px 0px 0px 0px;"><b>Teknis</b></div>
                        <div class="col-md-9">
                            <i class="fa fa-caret-right"></i> '.$teknis.'
                        </div>
                        <div class="col-md-3">
                            '.$tk2.'
                        </div>
                        <div class="col-md-12" style="margin:20px 0px 0px 0px;"><b>Fungsional</b></div>
                        <div class="col-md-9">
                            '.$fs1.'
                        </div>
                        <div class="col-md-3">
                            '.$tk3.'
                        </div>
                        <div class="col-md-9">
                            '.$fs2.'
                        </div>
                        <div class="col-md-3">
                            '.$tk4.'
                        </div>
                        <div class="col-md-9">
                            '.$fs3.'
                        </div>
                        <div class="col-md-3">
                            '.$tk5.'
                        </div>
                    </div>';
        })->style('max-width:280px;text-align:center')->help('Tingkat Pentingnya Terhadap Jabatan');
        
        $grid->column('Sertifikasi')->display(function(){
            
            $kat    = ProfilIkhtisarJabatan::select('mkj.tingkat')
                        ->join('m_kategori_jabatan AS mkj', 'mkj.id', '=', 'p_profil_ikhtisar_jabatan.m_kategori_jabatan_id')
                        ->find($this->p_profil_ikhtisar_jabatan_id)->tingkat;

            if(is_null($this->t_syarat_sertifikasi1))
            {
                $fs1 = "";
            }
            else
            {
                $sertifikasi    = SyaratSertifikasi::select('ms.id')
                        ->join('m_sertifikasi AS ms', 'ms.id', '=', 't_syarat_sertifikasi.m_sertifikasi_id')
                        ->find($this->t_syarat_sertifikasi1)->id;

                if($sertifikasi == '1')
                {
                    $sertifikasi    = SyaratSertifikasi::select('ms.sertifikasi')
                        ->join('m_sertifikasi AS ms', 'ms.id', '=', 't_syarat_sertifikasi.m_sertifikasi_id')
                        ->find($this->t_syarat_sertifikasi1)->sertifikasi;

                    $fs1 = '<i class="fa fa-caret-right"></i> '.$sertifikasi;
                }
                else
                {
                    $sertifikasi    = SyaratSertifikasi::select('ms.sertifikasi')
                        ->join('m_sertifikasi AS ms', 'ms.id', '=', 't_syarat_sertifikasi.m_sertifikasi_id')
                        ->find($this->t_syarat_sertifikasi1)->sertifikasi;

                    $fs1 = '<i class="fa fa-caret-right"></i> '.$sertifikasi.' '.$kat;
                }
            }

            if(is_null($this->t_syarat_sertifikasi2))
            {
                $fs2 = "";
            }
            else
            {
                $sertifikasi    = SyaratSertifikasi::select('ms.sertifikasi')
                        ->join('m_sertifikasi AS ms', 'ms.id', '=', 't_syarat_sertifikasi.m_sertifikasi_id')
                        ->find($this->t_syarat_sertifikasi2)->sertifikasi;

                $fs2 = '<i class="fa fa-caret-right"></i> '.$sertifikasi.' '.$kat;
            }

            $tk1    = TingkatPenting::find($this->tingkat_penting_sertifikasi1)->tingkat_penting;

            if(is_null($this->tingkat_penting_sertifikasi2))
            {
                $tk2 = "";
            }
            else
            {
                $tk2    = TingkatPenting::find($this->tingkat_penting_sertifikasi2)->tingkat_penting;
            }            

            return '<div class="row" style="margin-right:5px; text-align:left;">
                        <div class="col-md-12 alert text-center" style="margin:0px; background: #c8d9eb">Sertifikasi</div>
                        <div class="col-md-12"><b>Fungsional</b></div>
                        <div class="col-md-9">
                            '.$fs1.'
                        </div>
                        <div class="col-md-3">
                            '.$tk1.'
                        </div>
                        <div class="col-md-9">
                            '.$fs2.'
                        </div>
                        <div class="col-md-3">
                            '.$tk2.'
                        </div>
                    </div>';

        })->style('max-width:280px;text-align:center')->help('Tingkat Pentingnya Terhadap Jabatan');
        
        $grid->column('Pengalaman Kerja')->display(function(){
            $fs1    = SyaratPengalamanKerja::selectRaw('CONCAT(mpk.tahun1, " ",mpk.ket1) AS tahun1')
                        ->join('m_pengalaman_kerja AS mpk', 'mpk.id', '=', 't_syarat_pengalaman_kerja.m_pengalaman_kerja_id')
                        ->find($this->t_syarat_pengalaman_kerja_id)->tahun1;

            $fs2    = SyaratPengalamanKerja::selectRaw('CONCAT(mpk.tahun2, " ",mpk.ket2) AS tahun2')
                        ->join('m_pengalaman_kerja AS mpk', 'mpk.id', '=', 't_syarat_pengalaman_kerja.m_pengalaman_kerja_id')
                        ->find($this->t_syarat_pengalaman_kerja_id)->tahun2;

            if(is_null($fs1))
            {
                $fs1 = "";
            }
            else
            {
                $pengalaman = SyaratPengalamanKerja::selectRaw('CONCAT(mpk.tahun1, " ",mpk.ket1) AS tahun1')
                        ->join('m_pengalaman_kerja AS mpk', 'mpk.id', '=', 't_syarat_pengalaman_kerja.m_pengalaman_kerja_id')
                        ->find($this->t_syarat_pengalaman_kerja_id)->tahun1;

                $fs1 = '<i class="fa fa-caret-right"></i> '.$pengalaman;
            }

            if(is_null($fs2))
            {
                $fs2 = "";
            }
            else
            {
                $pengalaman = SyaratPengalamanKerja::selectRaw('CONCAT(mpk.tahun2, " ",mpk.ket2) AS tahun2')
                        ->join('m_pengalaman_kerja AS mpk', 'mpk.id', '=', 't_syarat_pengalaman_kerja.m_pengalaman_kerja_id')
                        ->find($this->t_syarat_pengalaman_kerja_id)->tahun2;

                $fs2 = '<i class="fa fa-caret-right"></i> '.$pengalaman;
            }

            $tk1 = TingkatPenting::find($this->tingkat_penting_pengalaman_kerja)->tingkat_penting;
            
            # ikhtisar
            $ikhtisar = ProfilIkhtisarJabatan::select('t_ikhtisar_jabatan_id')
                            ->where('id', $this->p_profil_ikhtisar_jabatan_id)
                            ->get();

            foreach ($ikhtisar as $row) 
            {
                $konten1 = IkhtisarJabatan::find($row->t_ikhtisar_jabatan_id);

                $konten2 = '';
                
                foreach ($konten1 as $baris) 
                {
                    $konten4 = '';    
                    $konten3 = RuangLingkup::find($baris->m_ruang_lingkup_id);
                    
                    foreach($konten3 as $brs)
                    {
                        $konten4 .= '<li>'.$brs->ruang_lingkup.'</li>';
                    }

                    $konten2 .= '<b>'.Jenis::find($baris->m_kompetensi_jenis_id)->alias.'</b><ol style="margin-left:-25px;">'.$konten4.'</ol>';
                }
            }
            # end of ikhtisar

            return '<div class="row" style="text-align:left; background: #ecf2f9">
                        <div class="col-md-12 alert text-center" style="margin:0px; background: #c8d9eb">Pengalaman Kerja</div>
                        <div class="col-md-9">
                            '.$fs1.'<br>
                            '.$fs2.'
                        </div>
                        <div class="col-md-3">
                            '.$tk1.'
                        </div>
                    </div>
                    <div><hr></div>
                    <div class="row" style="text-align:left; background: #ecf2f9">
                        <div class="col-md-12 alert text-center" style="margin:0px; background: #c8d9eb">Indikator Kinerja Jabatan</div>
                        <div class="col-md-12">
                            Dokumen Hasil : <br>'.$konten2.'
                        </div>
                    </div>';

        })->style('max-width:280px;text-align:center')->help('Tingkat Pentingnya Terhadap Jabatan');

        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();
        $grid->column('deleted_at', __('Deleted at'))->hide();

        $grid->paginate(1);

        $grid->batchActions(function ($batch) {
            $batch->add(new BatchReplicate());
            $batch->disableDelete();
        });

        #$grid->enableHotKeys();

        $grid->filter(function($filter){
            $filter->disableIdFilter();
            
            $filter->where(function ($query) {
                $query->whereHas('pangkat', function ($query)
                {
                    $query->join('m_pangkat AS min', 'min.id', '=', 't_syarat_pangkat.pangkat_min')
                        ->join('m_pangkat AS maks', 'maks.id', '=', 't_syarat_pangkat.pangkat_maks')
                        ->where('min.pangkat', 'like', "%{$this->input}%")
                        ->orWhere('min.golongan', 'like', "%{$this->input}%")
                        ->orWhere('maks.pangkat', 'like', "%{$this->input}%")
                        ->orWhere('maks.golongan', 'like', "%{$this->input}%");
                });
            }, 'Pangkat');

            $filter->where(function ($query) {
                $query->whereHas('pendidikan', function ($query)
                {
                    $query->join('m_jenjang AS mj', 'mj.id', '=', 't_syarat_pendidikan.m_jenjang_id')
                        ->where('mj.jenjang', 'like', "%{$this->input}%");
                });
            }, 'Pendidikan');

            $filter->where(function ($query) {
                $query->whereHas('manajerial', function ($query)
                {
                    $query->join('m_pelatihan AS mp', 'mp.id', '=', 't_syarat_pelatihan_manajerial.m_pelatihan_id')
                        ->where('mp.pelatihan', 'like', "%{$this->input}%")
                        ->orWhere('t_syarat_pelatihan_manajerial.level', 'like', "%{$this->input}%");
                });
            }, 'Pelatihan Manajerial');

            $filter->where(function ($query) {
                $query->whereHas('teknis', function ($query)
                {
                    $query->join('m_pelatihan AS mp', 'mp.id', '=', 't_syarat_pelatihan_teknis.m_pelatihan_id')
                        ->where('mp.pelatihan', 'like', "%{$this->input}%")
                        ->orWhere('t_syarat_pelatihan_teknis.level', 'like', "%{$this->input}%");
                });
            }, 'Pelatihan Teknis');

            $filter->where(function ($query) {
                $query->whereHas('fungsional', function ($query)
                {
                    $query->join('m_kategori_jabatan AS mkj', 'mkj.id', '=', 'p_profil_ikhtisar_jabatan.m_kategori_jabatan_id')
                        ->where('mkj.tingkat', 'like', "%{$this->input}%");
                });
            }, 'Pelatihan Fungsional');

            $filter->where(function ($query) {
                $query->whereHas('sertifikasi', function ($query)
                {
                    $query->select('ms1.sertifikasi', 'ms2.sertifikasi', 'mkj.tingkat')
                        ->join('m_kategori_jabatan AS mkj', 'mkj.id', '=', 'p_profil_ikhtisar_jabatan.m_kategori_jabatan_id')
                        ->join('p_profil_persyaratan_jabatan AS pps', 'pps.p_profil_ikhtisar_jabatan_id', '=', 'p_profil_ikhtisar_jabatan.id')
                        ->join('t_syarat_sertifikasi AS tss1', 'tss1.id', '=', 'pps.t_syarat_sertifikasi1')
                        ->join('t_syarat_sertifikasi AS tss2', 'tss2.id', '=', 'pps.t_syarat_sertifikasi2')
                        ->join('m_sertifikasi AS ms1', 'ms1.id', '=', 'tss1.m_sertifikasi_id')
                        ->join('m_sertifikasi AS ms2', 'ms2.id', '=', 'tss2.m_sertifikasi_id')
                        ->where('mkj.tingkat', 'like', "%{$this->input}%")
                        ->orWhere('ms1.sertifikasi', 'like', "%{$this->input}%")
                        ->orWhere('ms2.sertifikasi', 'like', "%{$this->input}%");
                });
            }, 'Sertifikasi');

            $filter->where(function ($query) {
                $query->whereHas('pengalaman', function ($query)
                {
                    $query->join('m_pengalaman_kerja AS p', 'p.id', '=', 't_syarat_pengalaman_kerja.m_pengalaman_kerja_id')
                        ->where('p.tahun1', 'like', "%{$this->input}%")
                        ->orWhere('p.tahun2', 'like', "%{$this->input}%")
                        ->orWhere('p.ket1', 'like', "%{$this->input}%")
                        ->orWhere('p.ket2', 'like', "%{$this->input}%");
                });
            }, 'Pengalaman Kerja');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ProfilPersyaratanJabatan::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('p_profil_ikhtisar_jabatan_id', __('Jabatan'));
        $show->field('t_syarat_pendidikan_id', __('T syarat pendidikan id'));
        $show->field('t_syarat_pelatihan_manajerial_id', __('T syarat pelatihan manajerial id'));
        $show->field('t_syarat_pelatihan_teknis_id', __('T syarat pelatihan teknis id'));
        $show->field('t_syarat_pelatihan_fungsional_id', __('T syarat pelatihan fungsional id'));
        $show->field('t_syarat_sertifikasi_id', __('T syarat sertifikasi id'));
        $show->field('t_syarat_pengalaman_id', __('T syarat pengalaman id'));
        $show->field('t_syarat_pangkat_id', __('T syarat pangkat id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ProfilPersyaratanJabatan);
        $action  = explode('.', Route::currentRouteName())[1];

        $jabatan = ProfilIkhtisarJabatan::selectRaw('p_profil_ikhtisar_jabatan.id, CONCAT(kode, ": ", kj.alias, " ", 
                        mj.jabatan, " ", kj.tingkat) as jbtn')
                        ->join('m_jabatan AS mj', 'mj.id', '=', 'p_profil_ikhtisar_jabatan.m_jabatan_id')
                        ->join('m_kategori_jabatan AS kj', 'kj.id', '=', 'p_profil_ikhtisar_jabatan.m_kategori_jabatan_id')
                        ->pluck('jbtn', 'id');

        $pendidikan = SyaratPendidikan::selectRaw('t_syarat_pendidikan.id, CONCAT(mj.jenjang, " ",
                        mbi.bidang_ilmu) as pdk')
                        ->join('m_jenjang AS mj', 'mj.id', '=', 't_syarat_pendidikan.m_jenjang_id')
                        ->join('m_bidang_ilmu AS mbi', 'mbi.id', '=', 't_syarat_pendidikan.m_bidang_ilmu_id')
                        ->pluck('pdk', 'id');

        $manajerial = SyaratPelatihanManajerial::selectRaw('t_syarat_pelatihan_manajerial.id, CONCAT(mpl.pelatihan, " Level ", 
                        t_syarat_pelatihan_manajerial.level) as mnj')
                        ->join('m_pelatihan AS mpl', 'mpl.id', '=', 't_syarat_pelatihan_manajerial.m_pelatihan_id')
                        ->pluck('mnj', 'id');

        $teknis     = SyaratPelatihanTeknis::selectRaw('t_syarat_pelatihan_teknis.id, CONCAT(mpl.pelatihan) as tkn')
                        ->join('m_pelatihan AS mpl', 'mpl.id', '=', 't_syarat_pelatihan_teknis.m_pelatihan_id')
                        ->pluck('tkn', 'id');

        $fungsional = SyaratPelatihanFungsional::selectRaw('t_syarat_pelatihan_fungsional.id, CONCAT(mpl.pelatihan) as fgs')
                        ->join('m_pelatihan AS mpl', 'mpl.id', '=', 't_syarat_pelatihan_fungsional.m_pelatihan_id')
                        ->pluck('fgs', 'id');

        $sertifikasi = SyaratSertifikasi::selectRaw('t_syarat_sertifikasi.id, CONCAT(mst.sertifikasi) as srt')
                        ->join('m_sertifikasi AS mst', 'mst.id', '=', 't_syarat_sertifikasi.m_sertifikasi_id')
                        ->pluck('srt', 'id');

        $pengalaman = SyaratPengalamanKerja::selectRaw('t_syarat_pengalaman_kerja.id, 
                            IF(mpk.tahun2 IS NULL, CONCAT(mpk.tahun1, " Tahun"), CONCAT(mpk.tahun1, " Tahun 
                            untuk Pengangkatan Pertama atau ", mpk.tahun2, " Tahun untuk Inpassing")) as thn')
                            ->join('m_pengalaman_kerja AS mpk', 'mpk.id', '=', 't_syarat_pengalaman_kerja.m_pengalaman_kerja_id')
                            ->pluck('thn', 'id');

        $pangkat = SyaratPangkat::selectRaw('t_syarat_pangkat.id, CONCAT("Min. ", min_p.pangkat, " (", min_p.golongan, "/", min_p.ruang, ") - Maks. ", maks_p.pangkat, " (", maks_p.golongan, "/", maks_p.ruang, ")") as pgkt')
                        ->join('m_pangkat AS min_p', 'min_p.id', '=', 't_syarat_pangkat.pangkat_min')
                        ->join('m_pangkat AS maks_p', 'maks_p.id', '=', 't_syarat_pangkat.pangkat_maks')
                        ->pluck('pgkt', 'id');       

        $tingkat    = TingkatPenting::select('id', 'tingkat_penting')->pluck('tingkat_penting', 'id');

        if($action == 'create')
        {
            $form->select('p_profil_ikhtisar_jabatan_id', __('Jabatan'))->options($jabatan);
            $form->select('t_syarat_pendidikan_id', __('Pendidikan'))->options($pendidikan);
            $form->select('t_syarat_pelatihan_manajerial_id', __('Pelatihan Manajerial'))->options($manajerial);
            $form->select('tingkat_penting_manajerial', __('Tingkat Penting'))->options($tingkat);
            $form->select('t_syarat_pelatihan_teknis_id', __('Pelatihan Teknis'))->options($teknis);
            $form->select('tingkat_penting_teknis', __('Tingkat Penting'))->options($tingkat);
            $form->select('t_syarat_pelatihan_fungsional1', __('Pelatihan Fungsional #1'))->options($fungsional);
            $form->select('tingkat_penting_fungsional1', __('Tingkat Penting'))->options($tingkat);
            $form->select('t_syarat_pelatihan_fungsional2', __('Pelatihan Fungsional #2'))->options($fungsional);
            $form->select('tingkat_penting_fungsional2', __('Tingkat Penting'))->options($tingkat);
            $form->select('t_syarat_pelatihan_fungsional3', __('Pelatihan Fungsional #3'))->options($fungsional);
            $form->select('tingkat_penting_fungsional3', __('Tingkat Penting'))->options($tingkat);
            
            $form->select('t_syarat_sertifikasi1', __('Sertifikasi #1'))->options($sertifikasi);
            $form->select('tingkat_penting_sertifikasi1', __('Tingkat Penting'))->options($tingkat);
            $form->select('t_syarat_sertifikasi2', __('Sertifikasi #2'))->options($sertifikasi);
            $form->select('tingkat_penting_sertifikasi2', __('Tingkat Penting'))->options($tingkat);

            $form->select('t_syarat_pengalaman_kerja_id', __('Syarat Pengalaman Kerja'))->options($pengalaman);
            $form->select('tingkat_penting_pengalaman_kerja', __('Tingkat Penting'))->options($tingkat);

            $form->select('t_syarat_pangkat_id', __('Syarat Pangkat'))->options($pangkat);

            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id);
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->select('p_profil_ikhtisar_jabatan_id', __('Jabatan'))->options($jabatan);
            $form->select('t_syarat_pendidikan_id', __('Pendidikan'))->options($pendidikan);
            $form->select('t_syarat_pelatihan_manajerial_id', __('Pelatihan Manajerial'))->options($manajerial);
            $form->select('tingkat_penting_manajerial', __('Tingkat Penting'))->options($tingkat);
            $form->select('t_syarat_pelatihan_teknis_id', __('Pelatihan Teknis'))->options($teknis);
            $form->select('tingkat_penting_teknis', __('Tingkat Penting'))->options($tingkat);
            
            $form->select('t_syarat_pelatihan_fungsional1', __('Pelatihan Fungsional #1'))->options($fungsional);
            $form->select('tingkat_penting_fungsional1', __('Tingkat Penting'))->options($tingkat);
            $form->select('t_syarat_pelatihan_fungsional2', __('Pelatihan Fungsional #2'))->options($fungsional);
            $form->select('tingkat_penting_fungsional2', __('Tingkat Penting'))->options($tingkat);
            $form->select('t_syarat_pelatihan_fungsional3', __('Pelatihan Fungsional #3'))->options($fungsional);
            $form->select('tingkat_penting_fungsional3', __('Tingkat Penting'))->options($tingkat);
            
            $form->select('t_syarat_sertifikasi1', __('Sertifikasi #1'))->options($sertifikasi);
            $form->select('tingkat_penting_sertifikasi1', __('Tingkat Penting'))->options($tingkat);
            $form->select('t_syarat_sertifikasi2', __('Sertifikasi #2'))->options($sertifikasi);
            $form->select('tingkat_penting_sertifikasi2', __('Tingkat Penting'))->options($tingkat);

            $form->select('t_syarat_pengalaman_kerja_id', __('Syarat Pengalaman Kerja'))->options($pengalaman);
            $form->select('tingkat_penting_pengalaman_kerja', __('Tingkat Penting'))->options($tingkat);

            $form->select('t_syarat_pangkat_id', __('Syarat Pangkat'))->options($pangkat);

            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);   
        }
        
        
        /*$form->number('m_jabatan_id', __('M jabatan id'));
        $form->number('t_syarat_pendidikan_id', __('T syarat pendidikan id'));
        $form->number('t_syarat_pelatihan_manajerial_id', __('T syarat pelatihan manajerial id'));
        $form->number('t_syarat_pelatihan_teknis_id', __('T syarat pelatihan teknis id'));
        $form->number('t_syarat_pelatihan_fungsional_id', __('T syarat pelatihan fungsional id'));
        $form->number('t_syarat_sertifikasi_id', __('T syarat sertifikasi id'));
        $form->number('t_syarat_pengalaman_id', __('T syarat pengalaman id'));
        $form->number('t_syarat_pangkat_id', __('T syarat pangkat id'));
        $form->number('created_by', __('Created by'));
        $form->number('updated_by', __('Updated by'));
        $form->number('deleted_by', __('Deleted by'));*/

        return $form;
    }
}
