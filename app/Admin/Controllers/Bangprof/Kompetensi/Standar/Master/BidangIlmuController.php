<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Standar\Master;

use App\Models\Bangprof\Kompetensi\Standar\Master\BidangIlmu;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;

use Illuminate\Support\Facades\Route;
use App\Admin\Actions\Post\Replicate;
use App\Admin\Actions\Post\BatchReplicate;

class BidangIlmuController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Standar > Bidang Ilmu';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new BidangIlmu);

        $grid->column('id', __('Id'))->hide();
        $grid->column('kode', __('Kode'));
        $grid->column('bidang_ilmu', __('Bidang Ilmu'));
        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();
        $grid->column('deleted_at', __('Deleted at'))->hide();

        $grid->actions(function ($actions) {
            $actions->add(new Replicate);
        });

        $grid->batchActions(function ($batch) {
            $batch->add(new BatchReplicate());
            $batch->disableDelete();
        });

        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('Kode', 'kode');
            $filter->like('Bidang Ilmu', 'bidang_ilmu');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(BidangIlmu::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('kode', __('Kode'));
        $show->field('bidang_ilmu', __('Bidang Ilmu'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('deleted_by', __('Deleted by'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form   = new Form(new BidangIlmu);
        $action = explode('.', Route::currentRouteName())[1];

        if($action == 'create')
        {
            $form->text('kode', __('Kode'));
            $form->text('bidang_ilmu', __('Bidang Ilmu'));
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id); 
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->text('kode', __('Kode'));
            $form->text('bidang_ilmu', __('Bidang Ilmu'));
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);
        }

        return $form;
    }
}
