<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Standar\Master;

use App\Models\Bangprof\Kompetensi\Standar\Master\PengalamanKerja;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;

use Illuminate\Support\Facades\Route;
use App\Admin\Actions\Post\Replicate;
use App\Admin\Actions\Post\BatchReplicate;

class PengalamanKerjaController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Standar > Pengalaman Kerja';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new PengalamanKerja);

        $grid->column('id', __('Id'))->hide();
        $grid->column('kode', __('Kode'));
        $grid->column(__('Tahun #1'))->display(function($id){
            return $this->tahun1.' '.$this->ket1;
        });
        $grid->column(__('Tahun #2'))->display(function($id){
            return $this->tahun2.' '.$this->ket2;
        });
        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();
        $grid->column('deleted_at', __('Deleted at'))->hide();

        $grid->actions(function ($actions) {
            $actions->add(new Replicate);
        });

        $grid->batchActions(function ($batch) {
            $batch->add(new BatchReplicate());
            $batch->disableDelete();
        });

        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('Kode', 'kode');
            $filter->like('Tahun #1', 'tahun1');
            $filter->like('Tahun #2', 'tahun2');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(PengalamanKerja::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('kode', __('Kode'));
        $show->field('tahun1', __('Tahun #1'));
        $show->field('ket1', __('Keterangan #1'));
        $show->field('tahun2', __('Tahun #2'));
        $show->field('ket2', __('Keterangan #2'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('deleted_by', __('Deleted by'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form   = new Form(new PengalamanKerja);
        $action = explode('.', Route::currentRouteName())[1];

        if($action == 'create')
        {
            $form->text('kode', __('Kode'));
            $form->text('tahun1', __('Tahun #1'));
            $form->text('ket1', __('Keterangan #1'));
            $form->text('tahun2', __('Tahun #2'));
            $form->text('ket2', __('Keterangan #2'));
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id); 
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->text('kode', __('Kode'));
            $form->text('tahun1', __('Tahun #1'));
            $form->text('ket1', __('Keterangan #1'));
            $form->text('tahun2', __('Tahun #2'));
            $form->text('ket2', __('Keterangan #2'));
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);
        }

        return $form;
    }
}