<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Standar\Master;

use App\Models\Bangprof\Kompetensi\Standar\Master\Pelatihan;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;

use Illuminate\Support\Facades\Route;
use App\Admin\Actions\Post\Replicate;
use App\Admin\Actions\Post\BatchReplicate;

class PelatihanController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Master > Pelatihan';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Pelatihan);

        $grid->column('id', __('Id'))->hide();
        $grid->column('kode', __('Kode'))->editable();
        $grid->column('pelatihan', __('Pelatihan'))->editable();
        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();
        $grid->column('deleted_at', __('Deleted at'))->hide();

        $grid->actions(function ($actions) {
            $actions->add(new Replicate);
        });

        $grid->batchActions(function ($batch) {
            $batch->add(new BatchReplicate());
            $batch->disableDelete();
        });

        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('Kode', 'kode');
            $filter->like('Pelatihan', 'pelatihan');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Pelatihan::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('kode', __('Kode'));
        $show->field('pelatihan', __('Pelatihan'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('deleted_by', __('Deleted by'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form   = new Form(new Pelatihan);
        $action = explode('.', Route::currentRouteName())[1];

        if($action == 'create')
        {
            $form->text('kode', __('Kode'));
            $form->text('pelatihan', __('Pelatihan'));
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id); 
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->text('kode', __('Kode'));
            $form->text('pelatihan', __('Pelatihan'));
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);
        }

        return $form;
    }
}
