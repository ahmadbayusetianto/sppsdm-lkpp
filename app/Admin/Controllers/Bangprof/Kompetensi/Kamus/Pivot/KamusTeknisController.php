<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Kamus\Pivot;

//master
use App\Models\Bangprof\Kompetensi\Kamus\Master\Jenis;

//transaksi
use App\Models\Bangprof\Kompetensi\Kamus\Transaksi\KompetensiNama;
use App\Models\Bangprof\Kompetensi\Kamus\Transaksi\KompetensiJenis;
use App\Models\Bangprof\Kompetensi\Kamus\Transaksi\KompetensiLevel;
use App\Models\Bangprof\Kompetensi\Kamus\Transaksi\KompetensiPerilaku;

//template
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Widgets\Tab;
use Encore\Admin\Widgets\Table;

class KamusTeknisController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Kamus Teknis';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $tab        = new Tab();
        $headers    = ['Level', 'Deskripsi', 'Indikator Perilaku'];
        $konten     = KompetensiNama::select('t_kompetensi_nama.id', 'kjb.kode')
                        ->join('m_kompetensi_jabatan AS kjb', 'kjb.id', '=', 't_kompetensi_nama.m_kompetensi_jabatan_id')
                        ->where('kjb.id', '3')
                        ->get();

        foreach ($konten as $row1)
        {
            $jenis = KompetensiJenis::select('t_kompetensi_jenis.id', 'kjs.alias', 'kjs.jenis', 'kjs.kode', 'kjs.definisi')
                        ->join('m_kompetensi_jenis AS kjs', 'kjs.id', '=', 't_kompetensi_jenis.m_kompetensi_jenis_id')
                        ->where('t_kompetensi_jenis.t_kompetensi_nama_id', $row1->id)
                        ->get();

            foreach ($jenis as $row2) 
            {                
                unset($rows);

                $level = KompetensiLevel::select('desk.id',  'desk.kode', 'desk.level', 'desk.deskripsi')
                            ->join('m_deskripsi AS desk', 'desk.id', '=', 't_kompetensi_level.m_deskripsi_id')
                            ->where('t_kompetensi_level.t_kompetensi_jenis_id', $row2->id)
                            ->get();

                foreach ($level as $row3) 
                {
                    unset($brs);

                    $perilaku = KompetensiPerilaku::select('lk.kode', 'lk.perilaku')
                                    ->join('m_perilaku AS lk', 'lk.id', '=', 't_kompetensi_perilaku.m_perilaku_id')
                                    ->where('t_kompetensi_perilaku.t_kompetensi_level_id', $row3->id)
                                    ->get();

                    foreach ($perilaku as $row4) 
                    {
                        $brs[] = '<div class="row" style="margin:0px 0px -20px 0px; text-align:center;">
                                    <div class="col-sm-2">'
                                        .$row4->kode.
                                    '</div>
                                    <div class="col-sm-10" style="padding-left:0px; text-align:justify;">'
                                        .$row4->perilaku.
                                    '</div>
                                </div>';
                    }

                    $rows[] = ['<div><span class="label label-warning">Level '.$row3->level.'</span><br>'.$row3->kode.'</div>', 
                                '<div style="text-align:justify; max-width:500px">'.$row3->deskripsi.'</div>', 
                                '<div style="text-align:justify; max-width:800px">'.implode('<br>', $brs).'</div>'];
                }

                $tab->add($row2->alias, 
                    '<div class="row" style="margin:0px;">
                        <div class="col-sm-2">Nama Kompetensi</div>
                        <div class="col-sm-4">'.$row2->jenis.'</div>
                    </div>
                    <div class="row" style="margin:0px;">
                        <div class="col-sm-2">Kode Kompetensi</div>
                        <div class="col-sm-4">'.$row2->kode.'</div>
                    </div>
                    <div class="row" style="margin:0px;">
                        <div class="col-sm-2">Definisi</div>
                        <div class="col-sm-5" style="text-align:justify">'.$row2->definisi.'</div>
                    </div>
                    <div class="row" style="margin:5px; text-align:center">
                        <div class="alert alert-success" style="margin:0px; padding:0px;"><b>KECAKAPAN</b></div>
                    </div>
                    <div class="row" style="margin:0px">
                        <div class="col-md-12">'.
                          new Table($headers, $rows)
                        .'</div>
                    </div>');
            }
        }

        return $tab->render();
    }
}