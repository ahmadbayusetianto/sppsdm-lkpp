<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Kamus\Transaksi;

# Master
use App\Models\Bangprof\Kompetensi\Kamus\Master\Deskripsi;
use App\Models\Bangprof\Kompetensi\Kamus\Master\Jenis;

# Transaksi
use App\Models\Bangprof\Kompetensi\Kamus\Transaksi\KompetensiLevel;
use App\Models\Bangprof\Kompetensi\Kamus\Transaksi\KompetensiJenis;

# System
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;
use Illuminate\Support\Facades\Route;

class KompetensiLevelController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Transaksi > Kompetensi Level';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new KompetensiLevel);

        $grid->column('id', __('Id'))->hide();

        $grid->t_kompetensi_jenis_id('Kompetensi')->display(function($rId){
            
            $jenis      = KompetensiJenis::find($rId)->m_kompetensi_jenis_id;
            $kompetensi = Jenis::find($jenis)->jenis;

            return $kompetensi;

        })->style('text-align:center');

        $grid->m_deskripsi_id('Level')->display(function($rId){
            
            $level  = Deskripsi::find($rId)->level;
            $desk   = Deskripsi::find($rId)->deskripsi;

            return '<div class="row>">
                        <div class="col-sm-1"><span class="label label-warning">Level '.$level.'</span></div>
                        <div class="col-sm-11" style="text-align:justify">'.$desk.'</div>
                    </div>';

        })->style('max-width:500px; text-align:center');

        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();

        $grid->filter(function($filter){
            $filter->disableIdFilter();

            $filter->where(function ($query) {
                $query->whereHas('kompetensi', function ($query)
                {
                    $query->join('m_kompetensi_jenis AS mkj', 'mkj.id', '=', 't_kompetensi_jenis.m_kompetensi_jenis_id')
                            ->where('mkj.jenis', 'like', "%{$this->input}%");                            
                });
            }, 'Kompetensi');

            $filter->where(function ($query) {
                $query->whereHas('level', function ($query)
                {
                    $query->where('level', 'like', "%{$this->input}%");
                });
            }, 'Level');
        });

        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });

        $grid->paginate(5);

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(KompetensiLevel::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('t_kompetensi_jenis_id', __('T kompetensi jenis id'));
        $show->field('m_level_id', __('M level id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('deleted_by', __('Deleted by'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form       = new Form(new KompetensiLevel);
        $action     = explode('.', Route::currentRouteName())[1];

        $level      = Deskripsi::selectRaw('id, CONCAT("[",kode, "][Level ", level,"] : ", deskripsi) as lvl')->pluck('lvl', 'id');
        $kompetensi = KompetensiJenis::selectRaw('t_kompetensi_jenis.id, CONCAT("[", kode, "] ", kj.jenis) as kj')
                        ->join('m_kompetensi_jenis AS kj', 'kj.id', '=', 't_kompetensi_jenis.m_kompetensi_jenis_id')
                        ->pluck('kj', 'id');

        if($action == 'create')
        {
            $form->select('t_kompetensi_jenis_id', __('Kompetensi'))->options($kompetensi);
            $form->select('m_deskripsi_id', __('Level'))->options($level);
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id);
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->select('t_kompetensi_jenis_id', __('Kompetensi'))->options($kompetensi);
            $form->select('m_deskripsi_id', __('Level'))->options($level);
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);   
        }

        return $form;
    }
}
