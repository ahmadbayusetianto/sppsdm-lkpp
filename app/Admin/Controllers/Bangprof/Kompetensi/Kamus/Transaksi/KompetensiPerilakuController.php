<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Kamus\Transaksi;

# Master
use App\Models\Bangprof\Kompetensi\Kamus\Master\Perilaku;
use App\Models\Bangprof\Kompetensi\Kamus\Master\Deskripsi;

# Transaksi
use App\Models\Bangprof\Kompetensi\Kamus\Transaksi\KompetensiLevel;
use App\Models\Bangprof\Kompetensi\Kamus\Transaksi\KompetensiPerilaku;

# System
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;
use Illuminate\Support\Facades\Route;

class KompetensiPerilakuController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Transaksi > Kompetensi Level';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new KompetensiPerilaku);

        $grid->column('id', __('Id'))->hide();

        $grid->t_kompetensi_level_id('Level')->display(function($rId){
            
            $lvl    = KompetensiLevel::find($rId)->m_deskripsi_id;
            $kode   = Deskripsi::find($lvl)->kode;
            $level  = Deskripsi::find($lvl)->level;

            return '<span class="label label-warning">['.$kode.']</span> Level '.$level;

        })->style('text-align:center');

        $grid->m_perilaku_id('Perilaku')->display(function($rId){
            
            $kode       = Perilaku::find($rId)->kode;
            $perilaku   = Perilaku::find($rId)->perilaku;

            return '<div class="row">
                        <div class="col-sm-1">
                            <span class="label label-warning">['.$kode.']</span>
                        </div>
                        <div class="col-sm-11">'.$perilaku.'</div>
                    </div>';

        })->style('max-width:500px; text-align:justify');
        
        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();
        $grid->column('deleted_at', __('Deleted at'))->hide();

        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(KompetensiPerilaku::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('t_kompetensi_level_id', __('T kompetensi level id'));
        $show->field('m_perilaku_id', __('M perilaku id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('deleted_by', __('Deleted by'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new KompetensiPerilaku);
        $action     = explode('.', Route::currentRouteName())[1];

        $level      = KompetensiLevel::selectRaw('t_kompetensi_level.id, CONCAT("[", dsk.kode, "][", dsk.level ,"]", 
                        dsk.deskripsi) as lvl')
                        ->join('m_deskripsi AS dsk', 'dsk.id', '=', 't_kompetensi_level.m_deskripsi_id')
                        ->pluck('lvl', 'id');

        $perilaku   = Perilaku::selectRaw('id, CONCAT("[",kode, "] ", perilaku) as lk')->pluck('lk', 'id');

        if($action == 'create')
        {
            $form->select('t_kompetensi_level_id', __('Level'))->options($level);
            $form->select('m_perilaku_id', __('Perilaku'))->options($perilaku);
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id);
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->select('t_kompetensi_level_id', __('Level'))->options($level);
            $form->select('m_perilaku_id', __('Perilaku'))->options($perilaku);
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);   
        }

        return $form;
    }
}
