<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Kamus\Transaksi;

# Master
use App\Models\Bangprof\Kompetensi\Kamus\Master\Jenis;
use App\Models\Bangprof\Kompetensi\Kamus\Master\KompetensiJabatan;

# Transaksi
use App\Models\Bangprof\Kompetensi\Kamus\Transaksi\KompetensiNama;
use App\Models\Bangprof\Kompetensi\Kamus\Transaksi\KompetensiJenis;

# System
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;
use Illuminate\Support\Facades\Route;
use App\Admin\Actions\Post\Replicate;
use App\Admin\Actions\Post\BatchReplicate;

class KompetensiJenisController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Transaksi > Kompetensi Jenis';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new KompetensiJenis);

        $grid->column('id', __('Id'))->hide();
        $grid->m_kompetensi_jenis_id('Jenis')->display(function($rId){
            
            $jenis  = Jenis::find($rId)->jenis;
            
            return $jenis;
        })->style('text-align:center');

        $grid->t_kompetensi_nama_id('Kompetensi')->display(function($rId){
            
            $kompetensi = KompetensiNama::selectRaw('CONCAT(kj.kompetensi_jabatan, " [", kj.kode, "]") as kj')
                            ->where('t_kompetensi_nama.id', $rId)
                            ->join('m_kompetensi_jabatan AS kj', 'kj.id', '=', 't_kompetensi_nama.m_kompetensi_jabatan_id')
                            ->pluck('kj.kj')->first();
            
            return $kompetensi;
        })->style('text-align:center');

        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();

        $grid->filter(function($filter){
            $filter->disableIdFilter();

            $filter->where(function ($query) {
                $query->whereHas('jenis', function ($query)
                {
                    $query->where('kode', 'like', "%{$this->input}%")
                        ->orWhere('alias', 'like', "%{$this->input}%")
                        ->orWhere('jenis', 'like', "%{$this->input}%");
                });
            }, 'Jenis');

            /*$filter->where(function ($query) {
                $query->whereHas('kompetensi', function ($query)
                {
                    $query->where('kompetensi_jabatan', 'like', "%{$this->input}%")
                        ->orWhere('kode', 'like', "%{$this->input}%");
                });
            }, 'Kompetensi');*/
        });

        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(KompetensiJenis::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('m_kompetensi_jenis_id', __('M kompetensi jenis id'));
        $show->field('t_kompetensi_nama_id', __('T kompetensi nama id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('deleted_by', __('Deleted by'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form       = new Form(new KompetensiJenis);
        $action     = explode('.', Route::currentRouteName())[1];

        $jenis      = Jenis::selectRaw('id, CONCAT("[", kode, "] ", jenis) as jns')->pluck('jns', 'id');
        $kompetensi = KompetensiNama::selectRaw('t_kompetensi_nama.id, CONCAT(kj.kompetensi_jabatan, " [", kj.kode, "]") as kj')
                        ->join('m_kompetensi_jabatan AS kj', 'kj.id', '=', 't_kompetensi_nama.m_kompetensi_jabatan_id')
                        ->pluck('kj', 'id');

        if($action == 'create')
        {
            $form->select('m_kompetensi_jenis_id', __('Jenis'))->options($jenis);
            $form->select('t_kompetensi_nama_id', __('Kompetensi'))->options($kompetensi);
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id);
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->select('m_kompetensi_jenis_id', __('Jenis'))->options($jenis);
            $form->select('t_kompetensi_nama_id', __('Kompetensi'))->options($kompetensi);
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);   
        }

        return $form;
    }
}
