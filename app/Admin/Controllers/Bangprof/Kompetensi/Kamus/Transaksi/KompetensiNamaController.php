<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Kamus\Transaksi;

# Master
use App\Models\Bangprof\Kompetensi\Kamus\Master\Regulasi;
use App\Models\Bangprof\Kompetensi\Kamus\Master\KompetensiJabatan;

# Transaksi
use App\Models\Bangprof\Kompetensi\Kamus\Transaksi\KompetensiNama;

# System
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;
use Illuminate\Support\Facades\Route;

class KompetensiNamaController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Transaksi > Kompetensi Nama';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new KompetensiNama);

        $grid->column('id', __('Id'))->hide();

        $grid->m_regulasi_id('Regulasi')->display(function($rId){
            
            $nomor  = Regulasi::find($rId)->nomor;
            $tahun  = Regulasi::find($rId)->tahun;
            
            return $nomor.' Tahun '.$tahun;
        })->style('text-align:center');

        $grid->m_kompetensi_jabatan_id('Kompetensi')->display(function($rId){
            
            $kompetensi = KompetensiJabatan::find($rId)->kompetensi_jabatan;
            $kode       = KompetensiJabatan::find($rId)->kode;
            
            return $kompetensi.' ['.$kode.']';
        })->style('text-align:center');

        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();

        $grid->filter(function($filter){
            $filter->disableIdFilter();

            $filter->where(function ($query) {
                $query->whereHas('regulasi', function ($query)
                {
                    $query->where('nomor', 'like', "%{$this->input}%")
                        ->orWhere('tahun', 'like', "%{$this->input}%");
                });
            }, 'Regulasi');

            $filter->where(function ($query) {
                $query->whereHas('kompetensi', function ($query)
                {
                    $query->where('kompetensi_jabatan', 'like', "%{$this->input}%")
                        ->orWhere('kode', 'like', "%{$this->input}%");
                });
            }, 'Kompetensi');
        });

        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(KompetensiNama::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('m_regulasi_id', __('M regulasi id'));
        $show->field('m_kompetensi_jabatan_id', __('M kompetensi jabatan id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('deleted_by', __('Deleted by'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form       = new Form(new KompetensiNama);
        $action     = explode('.', Route::currentRouteName())[1];

        $regulasi   = Regulasi::selectRaw('id, CONCAT(nomor, " Tahun ", tahun) as rgls')->pluck('rgls', 'id');
        $kompetensi = KompetensiJabatan::selectRaw('id, CONCAT(kode, " : ", kompetensi_jabatan) as kj')->pluck('kj', 'id');

        if($action == 'create')
        {
            $form->select('m_regulasi_id', __('Regulasi'))->options($regulasi);
            $form->select('m_kompetensi_jabatan_id', __('Kompetensi'))->options($kompetensi);
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id);
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->select('m_regulasi_id', __('Regulasi'))->options($regulasi);
            $form->select('m_kompetensi_jabatan_id', __('Kompetensi'))->options($kompetensi);
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);   
        }

        return $form;
    }
}
