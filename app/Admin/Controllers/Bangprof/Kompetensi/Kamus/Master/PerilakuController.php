<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Kamus\Master;

use App\Models\Bangprof\Kompetensi\Kamus\Master\Perilaku;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Facades\Admin;

use Illuminate\Support\Facades\Route;
use App\Admin\Actions\Post\Replicate;
use App\Admin\Actions\Post\BatchReplicate;

class PerilakuController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Kamus > Master > Perilaku';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Perilaku);

        $grid->column('id', __('ID'))->hide();
        $grid->column('kode', __('Kode'))->style('text-align:center')->editable();
        $grid->column('perilaku', __('Perilaku'))->style('max-width:500px; text-align:justify')->editable();
        $grid->column('created_by', __('Created by'))->hide();
        $grid->column('updated_by', __('Updated by'))->hide();
        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();

        $grid->actions(function ($actions) {
            $actions->add(new Replicate);
        });

        $grid->batchActions(function ($batch) {
            $batch->add(new BatchReplicate());
            $batch->disableDelete();
        });

        $grid->filter(function($filter){

            $filter->disableIdFilter();
            
            $filter->like('Kode', 'kode');
            $filter->like('Perilaku', 'perilaku');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Perilaku::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('kode', __('Kode'));
        $show->field('perilaku', __('Perilaku'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('deleted_by', __('Deleted by'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $action = explode('.', Route::currentRouteName())[1];
        $form   = new Form(new Perilaku);

        if($action == 'create')
        {
            $form->text('kode', __('Kode'));
            $form->text('perilaku', __('Perilaku'));
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id); 
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->text('kode', __('Kode'));
            $form->text('perilaku', __('Perilaku'));
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);
        }

        #$currentPath = Route::getCurrentRoute()->getActionName();
        #$currentPath = Route::currentRouteAction();
        #$currentPath = = Route::getFacadeRoot()->current()->uri();

        /*$form->saved(function (Form $form) {

            $form->model()->id;

        });

        $form->deleted(function (Form $form) {
            var_dump("expression"); die;
        });
    
        */        

        return $form;
    }
}
