<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Kamus\Master;

use App\Models\Bangprof\Kompetensi\Kamus\Master\Jenis;
use App\Models\Bangprof\Kompetensi\Kamus\Master\Regulasi;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;

use Illuminate\Support\Facades\Route;
use App\Admin\Actions\Post\Replicate;
use App\Admin\Actions\Post\BatchReplicate;
#use App\Admin\Actions\Post\ImportPost;

class JenisController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Kamus > Master > Jenis';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Jenis);

        $grid->column('kode', __('Kode'))->editable()->style('text-align:center;');
        $grid->column('alias', __('Alias'))->editable();
        $grid->column('jenis', __('Jenis'))->editable();
        $grid->column('definisi', __('Definisi'))->editable('textarea')->style('max-width:500px; text-align:justify');
        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();
        $grid->column('deleted_at', __('Deleted at'))->hide();

        $grid->actions(function ($actions) {
            $actions->add(new Replicate);
            #var_dump($actions->getKey());
        });

        $grid->batchActions(function ($batch) {
            $batch->add(new BatchReplicate());
            $batch->disableDelete();
        });

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();
            #$filter->scope('kode', 'Kode')->where('kode', 'L1-1.1');
            // Add a column filter
            $filter->like('Kode', 'kode');
            $filter->like('Jenis', 'jenis');
            $filter->like('Definisi', 'definisi');

            #$filter->is('department_manager_id', 'Manager')->select(Employee::all()->pluck('emp_name', 'emp_id'));
        });

        /*$grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });

        $grid->tools(function (Grid\Tools $tools) {
            #$tools->append(new ImportPost());
        });*/

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Jenis::findOrFail($id));

        $show->field('kode', __('Kode'));
        $show->field('alias', __('Alias'));
        $show->field('jenis', __('Jenis'));
        $show->field('definisi', __('Definisi'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Jenis);
        $action = explode('.', Route::currentRouteName())[1];

        if($action == 'create')
        {
            $form->text('kode', __('Kode'));
            $form->text('alias', __('Alias'));
            $form->text('jenis', __('Jenis'));
            $form->textarea('definisi', __('Definisi'))->rows(5);
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id); 
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->text('kode', __('Kode'));
            $form->text('alias', __('Alias'));
            $form->text('jenis', __('Jenis'));
            $form->textarea('definisi', __('Definisi'))->rows(5);
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);
        }
        
        return $form;
    }
}
