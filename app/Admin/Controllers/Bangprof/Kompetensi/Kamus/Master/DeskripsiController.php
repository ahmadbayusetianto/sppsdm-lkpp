<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Kamus\Master;

use App\Models\Bangprof\Kompetensi\Kamus\Master\Deskripsi;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Facades\Admin;

use Illuminate\Support\Facades\Route;
use App\Admin\Actions\Post\Replicate;
use App\Admin\Actions\Post\BatchReplicate;

class DeskripsiController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Kamus > Master > Deskripsi';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Deskripsi);

        $grid->column('id', __('ID'))->hide();
        $grid->column('kode', __('Kode'))->editable()->style('text-align:center');
        $grid->column('level', __('Level'))->editable()->style('text-align:center');
        $grid->column('deskripsi', __('Deskripsi'))->style('max-width:500px; text-align:justify')->editable();
        $grid->column('created_by', __('Created by'))->hide();
        $grid->column('updated_by', __('Updated by'))->hide();
        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();

        $grid->actions(function ($actions) {
            $actions->add(new Replicate);
        });

        $grid->batchActions(function ($batch) {
            $batch->add(new BatchReplicate());
            $batch->disableDelete();
        });

        $grid->filter(function($filter){

            $filter->disableIdFilter();
            
            $filter->like('Kode', 'kode');
            $filter->like('Level', 'level');
            $filter->like('Deskripsi', 'deskripsi');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Deskripsi::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('kode', __('Kode'));
        $show->field('level', __('Level'));
        $show->field('deskripsi', __('Deskripsi'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('deleted_by', __('Deleted by'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Deskripsi);
        $action = explode('.', Route::currentRouteName())[1];

        if($action == 'create')
        {
            $form->text('kode', __('Kode'));
            $form->text('level', __('Level'));
            $form->textarea('deskripsi', __('Deskripsi'))->rows(10);
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id); 
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->text('kode', __('Kode'));
            $form->text('level', __('Level'));
            $form->textarea('deskripsi', __('Deskripsi'))->rows(10);
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);
        }

        return $form;
    }
}
