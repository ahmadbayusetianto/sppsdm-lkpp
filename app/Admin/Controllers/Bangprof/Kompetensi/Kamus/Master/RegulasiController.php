<?php

namespace App\Admin\Controllers\Bangprof\Kompetensi\Kamus\Master;

use App\Models\Bangprof\Kompetensi\Kamus\Master\Regulasi;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Facades\Admin;

use Illuminate\Support\Facades\Route;
use App\Admin\Actions\Post\Replicate;
use App\Admin\Actions\Post\BatchReplicate;

class RegulasiController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Kamus > Master > Regulasi';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Regulasi);

        $grid->column('id', __('ID'))->hide();
        $grid->column('nomor', __('Nomor'))->editable()->style('text-align:center');
        $grid->column('tahun', __('Tahun'))->editable()->style('text-align:center');
        $grid->column('tentang', __('Tentang'))->style('max-width:500px; text-align:justify')->editable();
        $grid->column('dokumen', __('Dokumen'))->downloadable();
        $grid->column('created_by', __('Created by'))->hide();
        $grid->column('updated_by', __('Updated by'))->hide();
        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();

        $grid->actions(function ($actions) {
            $actions->add(new Replicate);
        });

        $grid->batchActions(function ($batch) {
            $batch->add(new BatchReplicate());
            $batch->disableDelete();
        });

        $grid->filter(function($filter){

            $filter->disableIdFilter();
            
            $filter->like('Nomor', 'nomor');
            $filter->like('Tahun', 'tahun');
            $filter->like('Tentang', 'tentang');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Regulasi::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('nomor', __('Nomor'));
        $show->field('tahun', __('Tahun'));
        $show->field('tentang', __('Tentang'));
        $show->field('dokumen', __('Dokumen'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('deleted_by', __('Deleted by'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $action = explode('.', Route::currentRouteName())[1];
        $form   = new Form(new Regulasi);

        if($action == 'create')
        {
            $form->text('nomor', __('Nomor'));
            $form->text('tahun', __('Tahun'));
            $form->text('tentang', __('Tentang'));
            $form->file('dokumen', __('Dokumen'));
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id); 
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->text('nomor', __('Nomor'));
            $form->text('tahun', __('Tahun'));
            $form->text('tentang', __('Tentang'));
            $form->file('dokumen', __('Dokumen'));
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);
        }

        return $form;
    }
}
