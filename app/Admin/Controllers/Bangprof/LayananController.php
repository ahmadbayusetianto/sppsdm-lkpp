<?php

namespace App\Admin\Controllers\Bangprof;

use App\Models\Bangprof\Layanan;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;

use Illuminate\Support\Facades\Route;
use App\Admin\Actions\Post\Replicate;
use App\Admin\Actions\Post\BatchReplicate;

class LayananController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Layanan';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Layanan);

        $grid->column('kode', __('Kode'))->editable()->style('text-align:center;');
        $grid->column('layanan', __('Layanan'))->editable();
        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();
        $grid->column('deleted_at', __('Deleted at'))->hide();

        $grid->actions(function ($actions) {
            $actions->add(new Replicate);
        });

        $grid->batchActions(function ($batch) {
            $batch->add(new BatchReplicate());
            $batch->disableDelete();
        });

        $grid->filter(function($filter){

            $filter->disableIdFilter();

            $filter->like('Kode', 'kode');
            $filter->like('Layanan', 'layanan');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Layanan::findOrFail($id));

        $show->field('kode', __('Kode'));
        $show->field('layanan', __('Layanan'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Layanan);
        $action = explode('.', Route::currentRouteName())[1];

        if($action == 'create')
        {
            $form->text('kode', __('Kode'));
            $form->text('layanan', __('Layanan'));
            $form->hidden('created_by', __('Created by'))->default(Admin::user()->id); 
            $form->hidden('updated_by', __('Updated by'));
        }
        else
        {
            $form->text('kode', __('Kode'));
            $form->text('layanan', __('Layanan'));
            $form->hidden('created_by', __('Created by'));
            $form->hidden('updated_by', __('Updated by'))->default(Admin::user()->id);
        }
        
        return $form;
    }
}
