<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');
    
    # GENERAL
    $router->resource('regulasi', Bangprof\Kompetensi\Kamus\Master\RegulasiController::class);
    $router->resource('unit-kerja', Bangprof\UnitKerjaController::class);
    $router->resource('layanan', Bangprof\LayananController::class);
    $router->resource('kategori-layanan', Bangprof\KategoriLayananController::class);

    # BANGPROF #
    //kamus-master
    $router->resource('kompetensi-jabatan', Bangprof\Kompetensi\Kamus\Master\KompetensiJabatanController::class);
    $router->resource('jenis', Bangprof\Kompetensi\Kamus\Master\JenisController::class);
    $router->resource('perilaku', Bangprof\Kompetensi\Kamus\Master\PerilakuController::class);
    $router->resource('deskripsi', Bangprof\Kompetensi\Kamus\Master\DeskripsiController::class);
    $router->resource('ruang-lingkup', Bangprof\Kompetensi\Kamus\Master\RuangLingkupController::class);
    $router->resource('tingkat-penting', Bangprof\Kompetensi\Standar\Master\TingkatPentingController::class);

    //kamus-transaksi
    $router->resource('kompetensi-nama', Bangprof\Kompetensi\Kamus\Transaksi\KompetensiNamaController::class);
    $router->resource('kompetensi-jenis', Bangprof\Kompetensi\Kamus\Transaksi\KompetensiJenisController::class);
    $router->resource('kompetensi-level', Bangprof\Kompetensi\Kamus\Transaksi\KompetensiLevelController::class);
    $router->resource('kompetensi-perilaku', Bangprof\Kompetensi\Kamus\Transaksi\KompetensiPerilakuController::class);
    
    //kamus
    $router->resource('kamus-manajerial', Bangprof\Kompetensi\Kamus\Pivot\KamusManajerialController::class);
    $router->resource('kamus-sosial-kultural', Bangprof\Kompetensi\Kamus\Pivot\KamusSosialKulturalController::class);
    $router->resource('kamus-teknis', Bangprof\Kompetensi\Kamus\Pivot\KamusTeknisController::class);

    //standar-master
    $router->resource('kategori-jabatan', Bangprof\Kompetensi\Standar\Master\KategoriJabatanController::class);
    $router->resource('pangkat', Bangprof\Kompetensi\Standar\Master\PangkatController::class);
    $router->resource('jabatan', Bangprof\Kompetensi\Standar\Master\JabatanController::class);
    $router->resource('jenjang', Bangprof\Kompetensi\Standar\Master\JenjangController::class);
    $router->resource('bidang-ilmu', Bangprof\Kompetensi\Standar\Master\BidangIlmuController::class);
    $router->resource('pengalaman-kerja', Bangprof\Kompetensi\Standar\Master\PengalamanKerjaController::class);

    $router->resource('program', Bangprof\Kompetensi\Standar\Master\ProgramController::class);
    $router->resource('pelatihan', Bangprof\Kompetensi\Standar\Master\PelatihanController::class);
    $router->resource('sertifikasi', Bangprof\Kompetensi\Standar\Master\SertifikasiController::class);
    #$router->resource('jenis-pelatihan', Bangprof\Kompetensi\Standar\Master\JenisPelatihanController::class);

    //standar-transaksi
    $router->resource('ikhtisar-jabatan', Bangprof\Kompetensi\Standar\Transaksi\IkhtisarJabatanController::class);
    $router->resource('syarat-pendidikan', Bangprof\Kompetensi\Standar\Transaksi\SyaratPendidikanController::class);
    $router->resource('syarat-pelatihan-manajerial', Bangprof\Kompetensi\Standar\Transaksi\SyaratPelatihanManajerialController::class);
    $router->resource('syarat-pelatihan-teknis', Bangprof\Kompetensi\Standar\Transaksi\SyaratPelatihanTeknisController::class);
    $router->resource('syarat-pelatihan-fungsional', Bangprof\Kompetensi\Standar\Transaksi\SyaratPelatihanFungsionalController::class);
    $router->resource('syarat-sertifikasi', Bangprof\Kompetensi\Standar\Transaksi\SyaratSertifikasiController::class);
    $router->resource('syarat-pangkat', Bangprof\Kompetensi\Standar\Transaksi\SyaratPangkatController::class);
    $router->resource('syarat-pengalaman-kerja', Bangprof\Kompetensi\Standar\Transaksi\SyaratPengalamanKerjaController::class);

    $router->resource('profil-ikhtisar-jabatan', Bangprof\Kompetensi\Standar\Transaksi\ProfilIkhtisarJabatanController::class);
    $router->resource('profil-standar-kompetensi', Bangprof\Kompetensi\Standar\Transaksi\ProfilStandarKompetensiController::class);
    $router->resource('profil-persyaratan-jabatan', Bangprof\Kompetensi\Standar\Transaksi\ProfilPersyaratanJabatanController::class);
    
    //standar-pivot
    $router->resource('standar-kompetensi-jabfung-ppbj', Bangprof\Kompetensi\Standar\Pivot\JabfungPpbjController::class);

    # PUSDIKLAT #
    
});