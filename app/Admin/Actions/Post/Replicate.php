<?php

namespace App\Admin\Actions\Post;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;

class Replicate extends RowAction
{
    public $name = 'Copy';

    public function handle(Model $model)
    {
        // $model ...
    	$model->replicate()->save();
    	
        return $this->response()->success('Success message.')->refresh();
    }

}