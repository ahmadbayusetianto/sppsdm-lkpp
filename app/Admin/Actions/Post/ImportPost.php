<?php

namespace App\Admin\Actions\Post;

use Encore\Admin\Actions\Action;
use Illuminate\Http\Request;

class ImportPost extends Action
{
    public $name = 'Import Data';

    protected $selector = '.import-post';

    public function handle(Request $request)
    {
        // $request ...
        $request->file('file');

        return $this->response()->success('Success message...')->refresh();
    }

    public function form()
    {
        $this->file('file', 'Please select file');
    }

    public function html()
    {
        return <<<HTML
        <a class="btn btn-sm btn-default import-post">Import Data</a>
        HTML;
    }
}