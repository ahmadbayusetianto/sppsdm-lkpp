<?php

namespace App\Models\Bangprof;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KategoriLayanan extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 'm_kategori_layanan';
    protected $fillable 	= ['kode', 'kategori_layanan', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];
}
