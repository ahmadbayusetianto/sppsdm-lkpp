<?php

namespace App\Models\Bangprof\Kompetensi\Kamus\Transaksi;

use App\Models\Bangprof\Kompetensi\Kamus\Master\Jenis;
use App\Models\Bangprof\Kompetensi\Kamus\Transaksi\KompetensiNama;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KompetensiJenis extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 't_kompetensi_jenis';
    protected $fillable 	= ['t_kompetensi_nama_id', 'm_kompetensi_jenis_id', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];

    public function jenis()
    {
    	return $this->belongsTo(Jenis::class, 'm_kompetensi_jenis_id');
    }

    public function nama()
    {
    	return $this->belongsTo(KompetensiNama::class, 't_kompetensi_nama_id');
    }
}
