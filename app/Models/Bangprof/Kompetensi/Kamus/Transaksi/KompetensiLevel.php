<?php

namespace App\Models\Bangprof\Kompetensi\Kamus\Transaksi;

use App\Models\Bangprof\Kompetensi\Kamus\Master\Deskripsi;
use App\Models\Bangprof\Kompetensi\Kamus\Transaksi\KompetensiJenis;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KompetensiLevel extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 't_kompetensi_level';
    protected $fillable 	= ['t_kompetensi_jenis_id', 'm_deskripsi_id', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];

    public function kompetensi()
    {
    	return $this->belongsTo(KompetensiJenis::class, 't_kompetensi_jenis_id');
    }

    public function level()
    {
    	return $this->belongsTo(Deskripsi::class, 'm_deskripsi_id');
    }
}
