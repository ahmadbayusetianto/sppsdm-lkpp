<?php

namespace App\Models\Bangprof\Kompetensi\Kamus\Transaksi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KompetensiPerilaku extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 't_kompetensi_perilaku';
    protected $fillable 	= ['t_kompetensi_level_id', 'm_perilaku_id', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];
}
