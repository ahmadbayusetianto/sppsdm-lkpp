<?php

namespace App\Models\Bangprof\Kompetensi\Kamus\Transaksi;

use App\Models\Bangprof\Kompetensi\Kamus\Master\Regulasi;
use App\Models\Bangprof\Kompetensi\Kamus\Master\KompetensiJabatan;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KompetensiNama extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 't_kompetensi_nama';
    protected $fillable 	= ['m_regulasi_id', 'm_kompetensi_jabatan_id', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];

    public function regulasi()
    {
    	return $this->belongsTo(Regulasi::class, 'm_regulasi_id');
    }

    public function kompetensi()
    {
    	return $this->belongsTo(KompetensiJabatan::class, 'm_kompetensi_jabatan_id');
    }
}
