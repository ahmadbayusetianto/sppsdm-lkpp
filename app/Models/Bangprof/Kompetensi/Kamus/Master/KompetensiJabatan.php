<?php

namespace App\Models\Bangprof\Kompetensi\Kamus\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KompetensiJabatan extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 'm_kompetensi_jabatan';
    protected $fillable 	= ['kode', 'kompetensi_jabatan', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];

    public function jenis()
    {
    	return $this->belongsToMany(Jenis::class, 'p_kompetensi_nama', 'm_kompetensi_jabatan_id', 'm_kompetensi_jenis_id')->groupBy(['id']);
    }

    public function regulasi()
    {
    	return $this->belongsToMany(Regulasi::class, 'p_kompetensi_nama', 'm_kompetensi_jabatan_id', 'm_regulasi_id')->groupBy(['id']);
    }
}
