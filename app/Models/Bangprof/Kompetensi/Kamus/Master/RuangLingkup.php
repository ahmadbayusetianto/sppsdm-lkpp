<?php

namespace App\Models\Bangprof\Kompetensi\Kamus\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RuangLingkup extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 'm_ruang_lingkup';
    protected $fillable 	= ['kode', 'ruang_lingkup', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];
}
