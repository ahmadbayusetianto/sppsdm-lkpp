<?php

namespace App\Models\Bangprof\Kompetensi\Kamus\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Perilaku extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 'm_perilaku';
    protected $fillable 	= ['kode', 'perilaku', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];

    public function cakap()
    {
    	return $this->belongsToMany(Kecakapan::class, 'p_kompetensi_kecakapan', 'm_perilaku_id', 'm_deskripsi_id')->withPivot(['id']);
    }
}
