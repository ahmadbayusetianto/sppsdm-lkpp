<?php

namespace App\Models\Bangprof\Kompetensi\Kamus\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deskripsi extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 'm_deskripsi';
    protected $fillable 	= ['kode', 'level', 'deskripsi', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];

    public function laku()
    {
    	return $this->belongsToMany(Perilaku::class, 'p_kompetensi_kecakapan', 'm_deskripsi_id', 'm_perilaku_id')->withPivot(['id']);
    }
}
