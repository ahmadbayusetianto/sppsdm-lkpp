<?php

namespace App\Models\Bangprof\Kompetensi\Kamus\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Jenis extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 'm_kompetensi_jenis';
    protected $fillable 	= ['kode', 'jenis', 'definisi', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];

    public function aturan()
    {
    	return $this->belongsToMany(Regulasi::class, 'p_kompetensi_nama', 'm_kompetensi_jenis_id', 'm_regulasi_id');
    }

    public function kompetensi()
    {
    	return $this->belongsToMany(KompetensiJabatan::class, 'p_kompetensi_nama', 'm_kompetensi_jenis_id', 'm_kompetensi_jabatan_id');
    }
}
