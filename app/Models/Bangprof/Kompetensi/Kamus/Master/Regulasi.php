<?php

namespace App\Models\Bangprof\Kompetensi\Kamus\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Regulasi extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 'm_regulasi';
    protected $fillable 	= ['nomor', 'tahun', 'tentang', 'dokumen', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];

    public function jenis()
    {
    	return $this->belongsToMany(Jenis::class, 'p_kompetensi_nama', 'm_regulasi_id', 'm_kompetensi_jenis_id')->withPivot(['id']);
    }

    public function jabatan()
    {
    	return $this->belongsToMany(Kompetensi_jabatan::class, 'p_kompetensi_nama', 'm_regulasi_id', 'm_kompetensi_jabatan_id')->groupBy(['id']);
    }
}
