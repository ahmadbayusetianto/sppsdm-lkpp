<?php

namespace App\Models\Bangprof\Kompetensi\Standar\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TingkatPenting extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 'm_tingkat_penting';
    protected $fillable 	= ['kode', 'tingkat_penting', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];
}
