<?php

namespace App\Models\Bangprof\Kompetensi\Standar\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Jenjang extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 'm_jenjang';
    protected $fillable 	= ['jenjang', 'level', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];
}
