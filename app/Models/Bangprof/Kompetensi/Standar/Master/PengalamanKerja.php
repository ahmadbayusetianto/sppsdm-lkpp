<?php

namespace App\Models\Bangprof\Kompetensi\Standar\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PengalamanKerja extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 'm_pengalaman_kerja';
    protected $fillable 	= ['kode', 'tahun1', 'tahun2', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];
}
