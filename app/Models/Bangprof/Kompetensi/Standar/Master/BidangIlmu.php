<?php

namespace App\Models\Bangprof\Kompetensi\Standar\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BidangIlmu extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 'm_bidang_ilmu';
    protected $fillable 	= ['kode', 'bidang_ilmu', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];
}
