<?php

namespace App\Models\Bangprof\Kompetensi\Standar\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pelatihan extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 'm_pelatihan';
    protected $fillable 	= ['kode', 'pelatihan', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];
}
