<?php

namespace App\Models\Bangprof\Kompetensi\Standar\Transaksi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfilPersyaratanJabatan extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 'p_profil_persyaratan_jabatan';
    protected $fillable 	= ['p_profil_ikhtisar_jabatan_id', 
    							't_syarat_pendidikan_id', 
    							't_syarat_pelatihan_manajerial_id', 
    							'tingkat_penting_manajerial', 
    							't_syarat_pelatihan_teknis_id', 
    							'tingkat_penting_teknis', 
    							't_syarat_pelatihan_fungsional1', 
    							'tingkat_penting_fungsional1', 
    							't_syarat_pelatihan_fungsional2', 
    							'tingkat_penting_fungsional2', 
    							't_syarat_pelatihan_fungsional3', 
    							'tingkat_penting_fungsional3', 
    							't_syarat_sertifikasi1',  
    							'tingkat_penting_sertifikasi1', 
    							't_syarat_sertifikasi2', 
    							'tingkat_penting_sertifikasi2', 
    							't_syarat_pengalaman_kerja_id',  
    							'tingkat_penting_pengalaman_kerja', 
    							't_syarat_pangkat_id', 
    							'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];

    public function pangkat()
    {
        return $this->belongsTo(SyaratPangkat::class, 't_syarat_pangkat_id');
    }

    public function pendidikan()
    {
        return $this->belongsTo(SyaratPendidikan::class, 't_syarat_pendidikan_id');
    }

    public function manajerial()
    {
        return $this->belongsTo(SyaratPelatihanManajerial::class, 't_syarat_pelatihan_manajerial_id');
    }

    public function teknis()
    {
        return $this->belongsTo(SyaratPelatihanTeknis::class, 't_syarat_pelatihan_teknis_id');
    }

    public function fungsional()
    {
        /*return $this->belongsTo(SyaratPelatihanFungsional::class, 't_syarat_pelatihan_fungsional1');
        return $this->belongsTo(SyaratPelatihanFungsional::class, 't_syarat_pelatihan_fungsional2');
        return $this->belongsTo(SyaratPelatihanFungsional::class, 't_syarat_pelatihan_fungsional3');*/

        return $this->belongsTo(ProfilIkhtisarJabatan::class, 'p_profil_ikhtisar_jabatan_id');
    }

    public function sertifikasi()
    {
        return $this->belongsTo(ProfilIkhtisarJabatan::class, 'p_profil_ikhtisar_jabatan_id');
    }

    public function pengalaman()
    {
        return $this->belongsTo(SyaratPengalamanKerja::class, 't_syarat_pengalaman_kerja_id');
    }
}
