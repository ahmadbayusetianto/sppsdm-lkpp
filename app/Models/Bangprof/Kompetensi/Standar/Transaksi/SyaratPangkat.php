<?php

namespace App\Models\Bangprof\Kompetensi\Standar\Transaksi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SyaratPangkat extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 't_syarat_pangkat';
    protected $fillable 	= ['pangkat_min', 'pangkat_maks', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];
}
