<?php

namespace App\Models\Bangprof\Kompetensi\Standar\Transaksi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SyaratPelatihanManajerial extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 't_syarat_pelatihan_manajerial';
    protected $fillable 	= ['m_jenis_pelatihan_id', 'm_pelatihan_id', 'level', 'm_tingkat_penting_id', 'created_by', 
    							'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];
}
