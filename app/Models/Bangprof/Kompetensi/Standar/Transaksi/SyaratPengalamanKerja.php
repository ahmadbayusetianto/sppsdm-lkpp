<?php

namespace App\Models\Bangprof\Kompetensi\Standar\Transaksi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SyaratPengalamanKerja extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 't_syarat_pengalaman_kerja';
    protected $fillable 	= ['m_pengalaman_kerja_id', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];
}
