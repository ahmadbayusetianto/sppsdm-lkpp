<?php

namespace App\Models\Bangprof\Kompetensi\Standar\Transaksi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SyaratSertifikasi extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 't_syarat_sertifikasi';
    protected $fillable 	= ['m_kategori_pelayanan_id', 'm_sertifikasi_id', 'm_kategori_jabatan_id', 'm_tingkat_penting_id', 
    							'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];
}
