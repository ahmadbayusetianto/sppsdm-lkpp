<?php

namespace App\Models\Bangprof\Kompetensi\Standar\Transaksi;

use App\Models\Bangprof\Kompetensi\Standar\Transaksi\ProfilIkhtisarJabatan;
use App\Models\Bangprof\Kompetensi\Kamus\Transaksi\KompetensiLevel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfilStandarKompetensi extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 'p_profil_standar_kompetensi';
    protected $fillable 	= ['p_profil_ikhtisar_jabatan_id', 't_kompetensi_level_id', 'created_by', 'updated_by', 
                                'deleted_by'];
    protected $date     	= ['deleted_at'];

    public function jabatan()
    {
    	return $this->belongsTo(ProfilIkhtisarJabatan::class, 'p_profil_ikhtisar_jabatan_id');
    }

    public function kompetensi()
    {
    	return $this->belongsTo(KompetensiLevel::class, 't_kompetensi_level_id');
    }
}
