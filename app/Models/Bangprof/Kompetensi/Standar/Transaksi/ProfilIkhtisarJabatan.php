<?php

namespace App\Models\Bangprof\Kompetensi\Standar\Transaksi;

use App\Models\Bangprof\Kompetensi\Standar\Master\KategoriJabatan;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfilIkhtisarJabatan extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 'p_profil_ikhtisar_jabatan';
    protected $fillable 	= ['kode', 'm_jabatan_id', 'm_kategori_jabatan_id', 't_ikhtisar_jabatan_id', 'created_by', 'updated_by', 
    							'deleted_by'];
    protected $date     	= ['deleted_at'];

    public function getTIkhtisarJabatanIdAttribute($value)
    {
        return explode(',', $value);
    }

    public function setTIkhtisarJabatanIdAttribute($value)
    {
        $this->attributes['t_ikhtisar_jabatan_id'] = implode(',', $value);
    }

    public function jabatan()
    {
        return $this->belongsTo(KategoriJabatan::class, 'm_kategori_jabatan_id');
    }

    public function ikhtisar()
    {
        return $this->belongsTo(IkhtisarJabatan::class, 't_ikhtisar_jabatan_id');
    }
}
