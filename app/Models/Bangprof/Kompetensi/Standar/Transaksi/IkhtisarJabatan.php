<?php

namespace App\Models\Bangprof\Kompetensi\Standar\Transaksi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IkhtisarJabatan extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 't_ikhtisar_jabatan';
    protected $fillable 	= ['kode', 'm_kompetensi_jenis', 'm_ruang_lingkup_id', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];

    public function getMRuangLingkupIdAttribute($value)
    {
        return explode(',', $value);
    }

    public function setMRuangLingkupIdAttribute($value)
    {
        $this->attributes['m_ruang_lingkup_id'] = implode(',', $value);
    }
}
