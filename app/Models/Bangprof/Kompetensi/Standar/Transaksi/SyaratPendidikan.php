<?php

namespace App\Models\Bangprof\Kompetensi\Standar\Transaksi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SyaratPendidikan extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 't_syarat_pendidikan';
    protected $fillable 	= ['m_jenjang_id', 'm_bidang_ilmu_id', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];

	public function getMBidangIlmuIdAttribute($value)
    {
        return explode(',', $value);
    }

    public function setMBidangIlmuIdAttribute($value)
    {
        $this->attributes['m_bidang_ilmu_id'] = implode(',', $value);
    }
}
