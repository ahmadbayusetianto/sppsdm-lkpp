<?php

namespace App\Models\Bangprof;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Layanan extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 'm_layanan';
    protected $fillable 	= ['kode', 'layanan', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];
}
