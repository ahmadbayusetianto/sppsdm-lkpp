<?php

namespace App\Models\Bangprof;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitKerja extends Model
{
    use SoftDeletes;

    protected $connection 	= 'bangprof';
    protected $table    	= 'm_unit_kerja';
    protected $fillable 	= ['kode', 'alias', 'unit_kerja', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];
}
