<?php

namespace App\Models\Pusdiklat\Pelatihan\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Program extends Model
{
    use SoftDeletes;

    protected $connection 	= 'pusdiklat';
    protected $table    	= 'm_program';
    protected $fillable 	= ['kode', 'program', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];
}
