<?php

namespace App\Models\Pusdiklat\Pelatihan\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pelatihan extends Model
{
    use SoftDeletes;

    protected $connection 	= 'pusdiklat';
    protected $table    	= 'm_pelatihan';
    protected $fillable 	= ['kode', 'pelatihan', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];
}
