<?php

namespace App\Models\Pusdiklat\Pelatihan\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JenisPelatihan extends Model
{
    use SoftDeletes;

    protected $connection 	= 'pusdiklat';
    protected $table    	= 'm_jenis_pelatihan';
    protected $fillable 	= ['kode', 'jenis_pelatihan', 'created_by', 'updated_by', 'deleted_by'];
    protected $date     	= ['deleted_at'];
}
