/*
Navicat MySQL Data Transfer

Source Server         : LOCAL1
Source Server Version : 50532
Source Host           : localhost:3306
Source Database       : ppsdm_bangprof

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2019-09-17 10:45:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2019_08_16_140541_create_perilakus_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_000000_create_users_table', '2');
INSERT INTO `migrations` VALUES ('3', '2014_10_12_100000_create_password_resets_table', '2');
INSERT INTO `migrations` VALUES ('4', '2016_01_04_173148_create_admin_tables', '2');
INSERT INTO `migrations` VALUES ('5', '2017_07_17_040159_create_config_table', '2');
INSERT INTO `migrations` VALUES ('6', '2017_07_17_040159_create_exceptions_table', '2');
INSERT INTO `migrations` VALUES ('7', '2019_08_15_030921_create_kompetensi_jenis_table', '2');

-- ----------------------------
-- Table structure for m_bidang_ilmu
-- ----------------------------
DROP TABLE IF EXISTS `m_bidang_ilmu`;
CREATE TABLE `m_bidang_ilmu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bidang_ilmu` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_bidang_ilmu
-- ----------------------------
INSERT INTO `m_bidang_ilmu` VALUES ('1', 'A', 'Matematika dan Ilmu Pengetahuan Alam', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_bidang_ilmu` VALUES ('2', 'B', 'Ilmu Tanaman', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_bidang_ilmu` VALUES ('3', 'C', 'Ilmu Hewani', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_bidang_ilmu` VALUES ('4', 'D', 'Ilmu Kedokteran', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_bidang_ilmu` VALUES ('5', 'E', 'Ilmu Kesehatan', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_bidang_ilmu` VALUES ('6', 'F', 'Ilmu Teknik', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_bidang_ilmu` VALUES ('7', 'G', 'Ilmu Bahasa', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_bidang_ilmu` VALUES ('8', 'H', 'Ilmu Ekonomi', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_bidang_ilmu` VALUES ('9', 'I', 'Ilmu Sosial Humaniora', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_bidang_ilmu` VALUES ('10', 'J', 'Ilmu Agama dan FiIsafat', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_bidang_ilmu` VALUES ('11', 'K', 'Ilmu Seni, Desain, dan Media', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_bidang_ilmu` VALUES ('12', 'L', 'Ilmu Pendidikan', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_bidang_ilmu` VALUES ('13', 'M', 'Lainnya', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);

-- ----------------------------
-- Table structure for m_deskripsi
-- ----------------------------
DROP TABLE IF EXISTS `m_deskripsi`;
CREATE TABLE `m_deskripsi` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_deskripsi
-- ----------------------------
INSERT INTO `m_deskripsi` VALUES ('1', 'M.01-A1', '1', 'Mampu bertindak sesuai nilai, norma, etika organisasi dalam kapasitas pribadi.', '2019-08-22 00:00:00', '2019-08-24 14:45:39', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('2', 'M.01-A2', '2', 'Mampu mengingatkan, mengajak rekan kerja untuk bertindak sesuai nilai, norma, dan etika organisasi.', '2019-08-22 00:00:00', '2019-08-24 14:45:46', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('3', 'M.01-A3', '3', 'Mampu memastikan, menanamkan keyakinan bersama agar anggota yang dipimpin bertindak sesuai nilai, norma, dan etika organisasi, dalam lingkup formal.', '2019-08-22 00:00:00', '2019-08-24 14:45:49', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('4', 'M.01-A4', '4', 'Mampu menciptakan situasi kerja yang mendorong kepatuhan pada nilai, norma, dan etika organisasi.', '2019-08-22 00:00:00', '2019-08-24 14:45:53', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('5', 'M.01-A5', '5', 'Mampu menjadi role model dalam penerapan standar keadilan dan etika di tingkat nasional.', '2019-08-22 00:00:00', '2019-08-24 14:45:56', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('6', 'M.02-B1', '1', 'Berpartisipasi dalam kelompok kerja.', '2019-08-22 00:00:00', '2019-08-22 22:31:30', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('7', 'M.02-B2', '2', 'Menumbuhkan tim kerja yang partisipatif dan efektif.', '2019-08-22 00:00:00', '2019-08-22 22:31:32', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('8', 'M.02-B3', '3', 'Efektif membangun tim kerja untuk peningkatan kinerja organisasi.', '2019-08-22 00:00:00', '2019-08-22 22:31:34', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('9', 'M.02-B4', '4', 'Membangun komitmen tim, sinergi.', '2019-08-22 00:00:00', '2019-08-22 22:31:37', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('10', 'M.02-B5', '5', 'Menciptakan situasi kerja sama secara konsisten, baik di dalam maupun di luar instansi.', '2019-08-22 00:00:00', '2019-08-22 22:31:39', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('11', 'M.03-C1', '1', 'Menyampaikan informasi dengan jelas, lengkap, pemahaman yang sama.', '2019-08-22 00:00:00', '2019-08-22 22:31:48', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('12', 'M.03-C2', '2', 'Aktif menjalankan komunikasi secara formal dan informal; Bersedia mendengarkan orang lain, menginterpretasikan pesan dengan respon yang sesuai, mampu menyusun materi presentasi, pidato, naskah, laporan, dll.', '2019-08-22 00:00:00', '2019-08-22 22:31:50', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('13', 'M.03-C3', '3', 'Berkomunikasi secara asertif, terampil berkomunikasi lisan/tertulis untuk menyampaikan informasi yang sensitif/rumit/kompleks.', '2019-08-22 00:00:00', '2019-08-22 22:31:52', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('14', 'M.03-C4', '4', 'Mampu mengemukakan pemikiran multidimensi secara lisan dan tertulis untuk mendorong kesepakatan dengan tujuan meningkatkan kinerja secara keseluruhan.', '2019-08-22 00:00:00', '2019-08-22 22:31:56', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('15', 'M.03-C5', '5', 'Menggagas sistem komunikasi yang terbuka secara strategis untuk mencari solusi dengan tujuan meningkatkan kinerja.', '2019-08-22 00:00:00', '2019-08-22 22:31:58', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('16', 'M.04-D1', '1', 'Bertanggung jawab untuk memenuhi standar kerja.', '2019-08-22 00:00:00', '2019-08-22 22:32:01', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('17', 'M.04-D2', '2', 'Berupaya meningkatkan hasil kerja pribadi yang lebih tinggi dari standar yang ditetapkan, mencari, mencoba metode alternatif untuk peningkatan kinerja.', '2019-08-22 00:00:00', '2019-08-22 22:32:03', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('18', 'M.04-D3', '3', 'Menetapkan target kerja yang menantang bagi unit kerja, memberi apresiasi dan teguran untuk mendorong kinerja.', '2019-08-22 00:00:00', '2019-08-22 22:32:06', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('19', 'M.04-D4', '4', 'Mendorong unit kerja mencapai target yang ditetapkan atau melebihi hasil kerja sebelumnya.', '2019-08-22 00:00:00', '2019-08-22 22:32:08', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('20', 'M.04-D5', '5', 'Meningkatkan mutu pencapaian kerja organisasi.', '2019-08-22 00:00:00', '2019-08-22 22:32:13', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('21', 'M.05-E1', '1', 'Menjalankan tugas mengikuti standar pelayanan.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('22', 'M.05-E2', '2', 'Mampu mensupervisi/mengawasi/menyelia dan menjelaskan proses pelaksanaan tugas-tugas pemerintahan/pelayanan publik secara transparan.', '2019-08-22 00:00:00', '2019-08-22 22:32:18', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('23', 'M.05-E3', '3', 'Mampu memanfaatkan kekuatan kelompok serta memperbaiki standar pelayanan publik di lingkup unit kerja.', '2019-08-22 00:00:00', '2019-08-22 22:32:20', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('24', 'M.05-E4', '4', 'Mampu memonitor, mengevaluasi, memperhitungkan dan mengantisipasi dampak dari isu-isu jangka panjang, kesempatan, atau kekuatan politik dalam hal pelayanan kebutuhan pemangku kepentingan yang transparan, objektif, dan profesional.', '2019-08-22 00:00:00', '2019-08-22 22:32:23', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('25', 'M.05-E5', '5', 'Mampu memastikan kebijakan kebijakan pelayanan publik yang menjamin terselenggaranya pelayanan publik yang objektif, netral, tidak memihak, tidak diskriminatif, serta tidak terpengaruh kepentingan pribadi/kelompok/p artai politik.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('26', 'M.06-F1', '1', 'Pengembangan diri.', '2019-08-22 00:00:00', '2019-08-22 22:32:26', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('27', 'M.06-F2', '2', 'Meningkatkan kemampuan bawahan dengan memberikan contoh dan penjelasan cara melaksanakan suatu pekerjaan.', '2019-08-22 00:00:00', '2019-08-22 22:32:27', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('28', 'M.06-F3', '3', 'Memberikan umpan balik, membimbing.', '2019-08-22 00:00:00', '2019-08-22 22:32:30', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('29', 'M.06-F4', '4', 'Menyusun program pengembangan jangka panjang dalam rangka mendorong manajemen pembelajaran.', '2019-08-22 00:00:00', '2019-08-22 22:32:33', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('30', 'M.06-F5', '5', 'Menciptakan situasi yang mendorong organisasi untuk mengembangkan kemampuan belajar secara berkelanjutan dalam rangka mendukung pencapaian hasil.', '2019-08-22 00:00:00', '2019-08-22 22:32:38', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('31', 'M.07-G1', '1', 'Mengikuti perubahan dengan arahan.', '2019-08-22 00:00:00', '2019-08-22 22:32:40', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('32', 'M.07-G2', '2', 'Proaktif beradaptasi mengikuti perubahan.', '2019-08-22 00:00:00', '2019-08-22 22:32:42', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('33', 'M.07-G3', '3', 'Membantu orang lain mengikuti perubahan, mengantisipasi perubahan secara tepat.', '2019-08-22 00:00:00', '2019-08-22 22:32:44', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('34', 'M.07-G4', '4', 'Memimpin perubahan pada unit kerja.', '2019-08-22 00:00:00', '2019-08-22 22:32:46', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('35', 'M.07-G5', '5', 'Memimpin, menggalang dan menggerakkan dukungan pemangku kepentingan untuk menjalankan perubahan secara berkelanjutan pada tingkat instansi/nasional.', '2019-08-22 00:00:00', '2019-08-22 22:32:48', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('36', 'M.08-H1', '1', 'Mengumpulkan informasi untuk bertindak sesuai kewenangan.', '2019-08-22 00:00:00', '2019-08-22 22:32:50', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('37', 'M.08-H2', '2', 'Menganalisis masalah secara mendalam.', '2019-08-22 00:00:00', '2019-08-22 22:32:53', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('38', 'M.08-H3', '3', 'Membandingkan berbagai alternatif, menyeimbangkan risiko keberhasilan dalam implementasi.', '2019-08-22 00:00:00', '2019-08-22 22:32:56', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('39', 'M.08-H4', '4', 'Menyelesaikan masalah yang mengandung risiko tinggi, mengantisipasi dampak keputusan, membuat tindakan pengamanan; mitigasi risiko.', '2019-08-22 00:00:00', '2019-08-22 22:32:57', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('40', 'M.08-H5', '5', 'Menghasilkan solusi dan mengambil keputusan untuk mengatasi permasalahan jangka panjang/strategis, berdampak nasional.', '2019-08-22 00:00:00', '2019-08-22 22:32:59', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('41', 'S.01-A1', '1', 'Peka memahami dan menerima kemajemukan.', '2019-08-22 00:00:00', '2019-08-22 22:33:04', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('42', 'S.01-A2', '2', 'Aktif mengembangkan sikap saling menghargai, menekankan persamaan dan persatuan.', '2019-08-22 00:00:00', '2019-08-22 22:33:06', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('43', 'S.01-A3', '3', 'Mempromosikan, mengembangkan sikap toleransi dan persatuan.', '2019-08-22 00:00:00', '2019-08-22 22:33:08', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('44', 'S.01-A4', '4', 'Mendayagunakan perbedaan secara konstruktif dan kreatif untuk meningkatkan efektifitas organisasi.', '2019-08-22 00:00:00', '2019-08-22 22:33:10', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('45', 'S.01-A5', '5', 'Wakil pemerintah untuk membangun hubungan sosial psikologis.', '2019-08-22 00:00:00', '2019-08-22 22:33:12', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('46', 'A1', '1', 'Mampu memahami tentang Identifikasi/Reviu Kebutuhan dan Penetapan Barang/Jasa, Spesifikasi Teknis/Kerangka Acuan Kerja, Perkiraan Harga, Strategi Pengadaan, Organisasi Pengadaan Barang/Jasa Pemerintah, dan melakukan pengumpulan bahan/data/informasi terkait.', '2019-08-22 00:00:00', '2019-08-22 22:33:14', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('47', 'A2', '2', 'Mampu melakukan Identifikasi/Reviu Kebutuhan dan Penetapan Barang/Jasa,  penyusunan Spesifikasi Teknis Barang/Jasa sederhana, Kerangka Acuan Kerja (KAK) Jasa Konsultansi perorangan, dan perkiraan harga berbasis harga pasar, standar harga, dan harga paket pekerjaan sejenis.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('48', 'A3', '3', 'Mampu melakukan penyusunan Spesifikasi Teknis, Kerangka Acuan Kerja (KAK), dan Perkiraan Harga untuk pekerjaan tidak kompleks, serta merumuskan Pemaketan dan Cara Pengadaan.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('49', 'A4', '4', 'Mampu melakukan penyusunan Spesifikasi Teknis, Kerangka Acuan Kerja (KAK), dan Perkiraan Harga untuk pekerjaan kompleks, serta merumuskan Strategi Pengadaan dan Organisasi Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('50', 'A5', '5', 'Mampu mengembangkan konsep, teori, dan/atau kebijakan tentang Identifikasi/Reviu Kebutuhan, Spesifikasi Teknis/Kerangka Acuan Kerja, Perkiraan Harga, dan Strategi Pengadaan, dan Organisasi Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('51', 'B1', '1', 'Mampu memahami tentang Reviu terhadap Dokumen Persiapan Pengadaan Barang/Jasa Pemerintah, Tahapan Pemilihan Penyedia Barang/Jasa, Penyusunan Daftar Penyedia, Negosiasi dalam Pengadaan Barang/Jasa Pemerintah, dan melakukan pengumpulan bahan/data/informasi terkait.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('52', 'B2', '2', 'Mampu melakukan Pemilihan Penyedia Barang/Jasa untuk pekerjaan dengan proses Pengadaan Barang/Jasa yang sederhana.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('53', 'B3', '3', 'Mampu melakukan Reviu  terhadap Dokumen Persiapan Pengadaan Barang/Jasa Pemerintah dan melakukan Pemilihan Penyedia Barang/Jasa untuk pekerjaan dengan proses Pengadaan Barang/Jasa yang tidak sederhana.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('54', 'B4', '4', 'Mampu melakukan Pemilihan Penyedia Barang/Jasa untuk pekerjaan dengan proses Pengadaan Barang/Jasa yang memiliki persyaratan khusus dan/atau spesifik.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('55', 'B5', '5', 'Mampu mengembangkan konsep, teori, dan kebijakan tentang  Reviu  terhadap Dokumen Persiapan Pengadaan Barang/Jasa Pemerintah, Pemilihan Penyedia Barang/Jasa, Penyusunan Daftar Penyedia, dan Negosiasi dalam Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('56', 'C1', '1', 'Mampu memahami tentang persiapan dan pelaksanaan pengendalian kontrak Pengadaan Barang/Jasa Pemerintah, Evaluasi Kinerja Penyedia Pengadaan Barang/Jasa Pemerintah, dan melakukan Perumusan Kontrak Pengadaan Barang/Jasa Pemerintah yang sederhana, serta melakukan pengumpulan bahan/data/informasi terkait.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('57', 'C2', '2', 'Mampu merumuskan kontrak Pengadaan Barang/Jasa Pemerintah dalam bentuk yang sederhana dan melakukan pengendalian kontrak untuk pekerjaan Pengadaan Barang/Jasa Pemerintah yang sederhana, serta melaksanakan Evaluasi Kinerja Penyedia Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('58', 'C3', '3', 'Mampu merumuskan kontak Pengadaan Barang/Jasa Pemerintah dalam bentuk yang tidak sederhana dan melakukan pengendalian kontrak untuk pekerjaan Pengadaan Barang/Jasa Pemerintah yang tidak kompleks.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('59', 'C4', '4', 'Mampu merumuskan kontak Pengadaan Barang/Jasa Pemerintah untuk pekerjaan yang memiliki kriteria/persyaratan khusus dan/atau spesifik, dan melakukan pengendalian kontrak untuk pekerjaan Pengadaan Barang/Jasa Pemerintah yang kompleks, sertu menyusun instrumen Evaluasi Kinerja Penyedia Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('60', 'C5', '5', 'Mampu mengembangkan konsep, teori, dan kebijakan tentang persiapan dan pelaksanaan pengendalian kontak Pengadaan Barang/Jasa Pemerintah, serta Evaluasi Kinerja Penyedia Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('61', 'D1', '1', 'Mampu memahami tentang perencanaan, pelaksanaan, pengawasan Pengadaan Barang/Jasa Pemerintah secara Swakelola dan melakukan pengumpulan bahan/data/informasi terkait.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('62', 'D2', '2', 'Mampu melaksanakan tahapan perencanaan, pelaksanaan, dan pengawasan Pengadaan Barang/Jasa Pemerintah secara Swakelola.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('63', 'D3', '3', 'Mampu melakukan analisis dan memecahkan masalah teknis operasional  pada tahapan perencanaan, pelaksanaan, dan pengawasan Pengadaan Barang/Jasa Pemerintah secara Swakelola.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('64', 'D4', '4', 'Mampu melakukan evaluasi terhadap efektifitas pelaksanaan Pengadaan Barang/Jasa Pemerintah secara swakelola.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_deskripsi` VALUES ('65', 'D5', '5', 'Mampu mengembangkan konsep, teori, dan kebijakan pelaksanaan Pengadaan Barang/Jasa Pemerintah secara Swakelola.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);

-- ----------------------------
-- Table structure for m_jabatan
-- ----------------------------
DROP TABLE IF EXISTS `m_jabatan`;
CREATE TABLE `m_jabatan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bidang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_jabatan
-- ----------------------------
INSERT INTO `m_jabatan` VALUES ('1', 'Pengelola Pengadaan Barang/Jasa', 'Pengadaan Barang/Jasa Pemerintah', '2019-08-23 15:46:06', '2019-08-23 15:46:06', null, '1', null, null);

-- ----------------------------
-- Table structure for m_jenjang
-- ----------------------------
DROP TABLE IF EXISTS `m_jenjang`;
CREATE TABLE `m_jenjang` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `jenjang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_jenjang
-- ----------------------------
INSERT INTO `m_jenjang` VALUES ('1', 'SD/Sederajat', '1', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_jenjang` VALUES ('2', 'SMP/Sederajat', '2', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_jenjang` VALUES ('3', 'SMA/Sederajat', '3', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_jenjang` VALUES ('4', 'D.I', '4', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_jenjang` VALUES ('5', 'D.II', '5', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_jenjang` VALUES ('6', 'D.III', '6', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_jenjang` VALUES ('7', 'D.IV', '7', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_jenjang` VALUES ('8', 'S.1', '7', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_jenjang` VALUES ('9', 'S.2', '8', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_jenjang` VALUES ('10', 'S.3', '9', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);

-- ----------------------------
-- Table structure for m_kategori_jabatan
-- ----------------------------
DROP TABLE IF EXISTS `m_kategori_jabatan`;
CREATE TABLE `m_kategori_jabatan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kategori` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tingkat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_kategori_jabatan
-- ----------------------------
INSERT INTO `m_kategori_jabatan` VALUES ('1', 'JPT', 'Jabatan Pimpinan Tinggi', 'Utama', '2019-08-23 08:26:07', '2019-08-23 08:26:07', null, '0', '0', '0');
INSERT INTO `m_kategori_jabatan` VALUES ('2', 'JPT', 'Jabatan Pimpinan Tinggi', 'Madya', '2019-08-23 08:46:39', '2019-08-23 08:46:39', null, '1', null, null);
INSERT INTO `m_kategori_jabatan` VALUES ('3', 'JPT', 'Jabatan Pimpinan Tinggi', 'Pratama', '2019-08-23 08:46:55', '2019-08-23 08:46:55', null, '1', null, null);
INSERT INTO `m_kategori_jabatan` VALUES ('4', 'JA', 'Jabatan Administrasi', 'Administrator', '2019-08-23 08:47:15', '2019-08-23 08:48:40', null, '1', '1', null);
INSERT INTO `m_kategori_jabatan` VALUES ('5', 'JA', 'Jabatan Administrasi', 'Pengawas', '2019-08-23 08:51:01', '2019-08-23 08:51:43', null, '1', '1', null);
INSERT INTO `m_kategori_jabatan` VALUES ('6', 'JA', 'Jabatan Administrasi', 'Pelaksana', '2019-08-23 08:51:47', '2019-08-23 08:51:52', null, '1', '1', null);
INSERT INTO `m_kategori_jabatan` VALUES ('7', 'JF', 'Jabatan Fungsional', 'Ahli Utama', '2019-08-23 08:51:55', '2019-08-23 08:52:25', null, '1', '1', null);
INSERT INTO `m_kategori_jabatan` VALUES ('8', 'JF', 'Jabatan Fungsional', 'Ahli Madya', '2019-08-23 08:52:27', '2019-08-23 08:52:31', null, '1', '1', null);
INSERT INTO `m_kategori_jabatan` VALUES ('9', 'JF', 'Jabatan Fungsional', 'Ahli Muda', '2019-08-23 08:52:34', '2019-08-23 08:52:39', null, '1', '1', null);
INSERT INTO `m_kategori_jabatan` VALUES ('10', 'JF', 'Jabatan Fungsional', 'Ahli Pertama', '2019-08-23 08:52:42', '2019-08-23 08:52:51', null, '1', '1', null);

-- ----------------------------
-- Table structure for m_kategori_layanan
-- ----------------------------
DROP TABLE IF EXISTS `m_kategori_layanan`;
CREATE TABLE `m_kategori_layanan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kategori_layanan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_kategori_layanan
-- ----------------------------
INSERT INTO `m_kategori_layanan` VALUES ('1', '001', 'Manajerial', '2019-08-27 08:07:49', '2019-08-27 08:07:49', null, '1', null, null);
INSERT INTO `m_kategori_layanan` VALUES ('2', '002', 'Teknis', '2019-08-27 08:07:58', '2019-08-27 08:07:58', null, '1', null, null);
INSERT INTO `m_kategori_layanan` VALUES ('3', '003', 'Fungsional', '2019-08-27 08:08:06', '2019-08-27 08:08:06', null, '1', null, null);

-- ----------------------------
-- Table structure for m_kompetensi_jabatan
-- ----------------------------
DROP TABLE IF EXISTS `m_kompetensi_jabatan`;
CREATE TABLE `m_kompetensi_jabatan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kompetensi_jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_kompetensi_jabatan
-- ----------------------------
INSERT INTO `m_kompetensi_jabatan` VALUES ('1', 'KM', 'Kompetensi Manajerial', '2019-08-21 18:06:47', '2019-08-21 19:09:44', null, '1', null, null);
INSERT INTO `m_kompetensi_jabatan` VALUES ('2', 'KS', 'Kompetensi Sosial Kultural', '2019-08-21 18:06:59', '2019-08-21 19:09:47', null, '1', null, null);
INSERT INTO `m_kompetensi_jabatan` VALUES ('3', 'KT', 'Kompetensi Teknis', '2019-08-21 18:07:12', '2019-08-21 19:10:46', null, '1', null, null);

-- ----------------------------
-- Table structure for m_kompetensi_jenis
-- ----------------------------
DROP TABLE IF EXISTS `m_kompetensi_jenis`;
CREATE TABLE `m_kompetensi_jenis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `jenis` varchar(255) DEFAULT NULL,
  `definisi` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of m_kompetensi_jenis
-- ----------------------------
INSERT INTO `m_kompetensi_jenis` VALUES ('1', 'M.01', 'Integritas', 'Integritas', 'Konsisten berperilaku selaras dengan nilai, norma dan/atau etika organisasi, dan jujur dalam hubungan dengan manajemen, rekan kerja, bawahan langsung, dan pemangku kepentingan, menciptakan budaya etika tinggi, bertanggung jawab atas tindakan atau keputusan beserta risiko yang menyertainya.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_kompetensi_jenis` VALUES ('2', 'M.02', 'Kerjasama', 'Kerjasama', 'Kemampuan menjalin, membina, mempertahankan hubungan kerja yang efektif, memiliki komitmen saling membantu dalam penyelesaian tugas, dan mengoptimalkan segala sumberdaya untuk mencapai tujuan strategis organisasi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_kompetensi_jenis` VALUES ('3', 'M.03', 'Komunikasi', 'Komunikasi', 'Kemampuan untuk menerangkan pandangan dan gagasan secara jelas,  sistematis disertai argumentasi yang logis dengan cara-cara yang sesuai baik secara lisan maupun tertulis; memastikan pemahaman; mendengarkan secara aktif dan efektif; mempersuasi, meyakinkan dan membujuk orang lain dalam rangka mencapai tujuan organisasi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_kompetensi_jenis` VALUES ('4', 'M.04', 'Orientasi pada Hasil', 'Orientasi pada Hasil', 'Kemampuan mempertahankan komitmen pribadi yang tinggi untuk menyelesaikan tugas, dapat diandalkan, bertanggungjawab, mampu secara sistimatis mengidentifikasi risiko dan peluang dengan memperhatikan keterhubungan antara perencanaan dan hasil, untuk keberhasilan organisasi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_kompetensi_jenis` VALUES ('5', 'M.05', 'Pelayanan Publik', 'Pelayanan Publik', 'Kemampuan dalam melaksanakan tugas-tugas pemerintahan, pembangunan dan kegiatan pemenuhan kebutuhan pelayanan public secara profesional, transparan, mengikuti standar pelayanan yang objektif, netral, tidak memihak, tidak diskriminatif, serta tidak terpengaruh kepentingan pribadi/kelompok/golongan/partai politik.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_kompetensi_jenis` VALUES ('6', 'M.06', 'Pengembangan Diri dan Orang Lain', 'Pengembangan Diri dan Orang Lain', 'Kemampuan untuk meningkatkan pengetahuan dan menyempurnakan keterampilan diri; menginspirasi orang lain untuk mengembangkan  dan menyempurnakan pengetahuan dan keterampilan yang relevan dengan pekerjaan dan pengembangan karir jangka panjang, mendorong kemauan belajar sepanjang hidup, memberikan saran/bantuan, umpan balik, bimbingan untuk membantu orang lain untuk mengembangkan potensi dirinya.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_kompetensi_jenis` VALUES ('7', 'M.07', 'Mengelola Perubahan', 'Mengelola Perubahan', 'Kemampuan dalam menyesuaikan diri dengan situasi yang baru atau berubah dan tidak bergantung secara berlebihan pada metode dan proses lama, mengambil tindakan untuk mendukung dan melaksanakan insiatif perubahan, memimpin usaha perubahan, mengambil tanggungjawab pribadi untuk memastikan perubahan berhasil diimplementasikan secara efektif.', '2019-08-22 00:00:00', '2019-08-24 13:09:36', null, '1', null, null);
INSERT INTO `m_kompetensi_jenis` VALUES ('8', 'M.08', 'Pengambilan Keputusan', 'Pengambilan Keputusan', 'Kemampuan membuat keputusan yang baik secara tepat waktu dan dengan keyakinan diri setelah mempertimbangkan prinsip kehati-hatian, dirumuskan secara sistematis dan seksama berdasarkan berbagai informasi, alternatif pemecahan masalah dan konsekuensinya, serta bertanggungjawab atas keputusan yang diambil.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_kompetensi_jenis` VALUES ('9', 'SK.01', 'Perekat Bangsa', 'Perekat Bangsa', 'Kemampuan dalam mempromosikan sikap toleransi, keterbukaan, peka terhadap perbedaan individu/kelompok masyarakat; mampu menjadi perpanjangan tangan pemerintah dalam mempersatukan masyarakat dan membangun hubungan social psikologis dengan masyarakat ditengah kemajemukan Indonesia sehingga menciptakan kelekatan yang kuat antara ASN dan para pemangku kepentingan serta diantara para pemangku kepentingan itu sendiri; menjaga, mengembangkan, dan mewujudkan rasa persatuan dan kesatuan dalam kehidupan bermasyarakat, berbangsa dan bernegara Indonesia.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_kompetensi_jenis` VALUES ('10', '001', 'Perencanaan', 'Melakukan Perencanaan Pengadaan Barang/Jasa Pemerintah', 'Kompetensi ini mencakupi kemampuan (sinergitas pengetahuan, keterampilan, dan sikap) untuk melakukan perencanaan Pengadaan Barang/Jasa pemerintah (beserta pengelolaan risikonya), yang meliputi : Identifikasi/Reviu Kebutuhan dan Penetapan Barang/Jasa; penyusunan Spesifikasi Teknis dan Kerangka Acuan Kerja (KAK); penyusunan perkiraan harga; perumusan strategi Pengadaan, pemaketan dan cara Pengadaan; dan perumusan organisasi pada Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_kompetensi_jenis` VALUES ('11', '002', 'Pemilihan Penyedia', 'Melakukan Pemilihan Penyedia Barang/Jasa Pemerintah', 'Kompetensi ini mencakupi kemampuan (sinergitas pengetahuan, keterampilan dan sikap) untuk melakukan pemilihan penyedia Barang/Jasa (beserta pengelolaan risikonya), yang meliputi : reviu  terhadap dokumen persiapan Pengadaan Barang/Jasa Pemerintah, penyusunan dan penjelasan dokumen pemilihan, penilaian kualifikasi, evaluasi penawaran, pengelolaan sanggah, penyusunan daftar penyedia, dan negosiasi dalam Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_kompetensi_jenis` VALUES ('12', '003', 'Kontrak', 'Mengelola Kontrak Pengadaan  Barang/Jasa Pemerintah', 'Kompetensi ini mencakupi kemampuan (sinergitas pengetahuan, keterampilan dan sikap) untuk melakukan pekerjaan pengelolaan kontrak Pengadaan Barang/Jasa Pemerintah (beserta pengelolaan risikonya)), yang meliputi perumusan kontrak, pembentukan tim pengelola kontrak, pengendalian pelaksanaan kontrak, serah terima hasil, dan evaluasi kinerja penyedia pada Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_kompetensi_jenis` VALUES ('13', '004', 'Swakelola', 'Mengelola Pengadaan Barang/Jasa Pemerintah Secara Swakelola', 'Kompetensi ini mencakupi kemampuan (sinergitas pengetahuan, keterampilan dan sikap) untuk melakukan pengelolaan Pengadaan Barang/Jasa secara swakelola (beserta pengelolaan risikonya), yang meliputi : penyusunan rencana dan persiapan; pelaksanaan; dan pengawasan, pengendalian, dan pelaporan; untuk Pengadaan Barang/Jasa Pemerintah yang dilakukan secara Swakelola tipe I, II, III, dan IV.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);

-- ----------------------------
-- Table structure for m_layanan
-- ----------------------------
DROP TABLE IF EXISTS `m_layanan`;
CREATE TABLE `m_layanan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `layanan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_layanan
-- ----------------------------
INSERT INTO `m_layanan` VALUES ('1', '001', 'Akreditasi LPPBJ', '2019-08-26 10:13:31', '2019-08-26 10:13:31', null, '1', null, null);
INSERT INTO `m_layanan` VALUES ('2', '002', 'Fasilitasi Pelatihan PBJ', '2019-08-26 10:13:45', '2019-08-26 10:13:45', null, '1', null, null);
INSERT INTO `m_layanan` VALUES ('3', '003', 'Layanan Sertifikasi Tingkat Dasar', '2019-08-26 10:14:04', '2019-08-26 10:14:04', null, '1', null, null);
INSERT INTO `m_layanan` VALUES ('4', '004', 'Layanan Sertifikasi Kompetensi Bagi Pengelola Pengadaan Barang/Jasa', '2019-08-26 10:14:36', '2019-08-26 10:14:36', null, '1', null, null);
INSERT INTO `m_layanan` VALUES ('5', '005', 'Layanan Sertifikasi Kompetensi Okupasi Bagi Pejabat Pembuat Komitmen (PPK), Kelompok Kerja (Pokja) Pemilihan dan Pejabat Pengadaan (PP)', '2019-08-26 10:15:04', '2019-08-26 10:15:43', null, '1', null, null);
INSERT INTO `m_layanan` VALUES ('6', '006', 'Layanan Sosialisasi dan Workshop Sertifikasi Kompetensi Pengadaan Barang/Jasa Pemerintah', '2019-08-26 10:16:19', '2019-08-26 10:16:19', null, '1', null, null);

-- ----------------------------
-- Table structure for m_pangkat
-- ----------------------------
DROP TABLE IF EXISTS `m_pangkat`;
CREATE TABLE `m_pangkat` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pangkat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `golongan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ruang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_pangkat
-- ----------------------------
INSERT INTO `m_pangkat` VALUES ('1', 'Pembina Utama', 'IV', 'e', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_pangkat` VALUES ('2', 'Pembina Utama Madya', 'IV', 'd', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_pangkat` VALUES ('3', 'Pembina Utama Muda', 'IV', 'c', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_pangkat` VALUES ('4', 'Pembina Tingkat I', 'IV', 'b', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_pangkat` VALUES ('5', 'Pembina', 'IV', 'a', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_pangkat` VALUES ('6', 'Penata Tingkat I', 'III', 'd', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_pangkat` VALUES ('7', 'Penata', 'III', 'c', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_pangkat` VALUES ('8', 'Penata Muda Tingkat I', 'III', 'b', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_pangkat` VALUES ('9', 'Penata Muda', 'III', 'a', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_pangkat` VALUES ('10', 'Pengatur Tingkat I', 'II', 'd', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_pangkat` VALUES ('11', 'Pengatur', 'II', 'c', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_pangkat` VALUES ('12', 'Pengatur Muda Tingkat I', 'II', 'b', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_pangkat` VALUES ('13', 'Pengatur Muda', 'II', 'a', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_pangkat` VALUES ('14', 'Juru Tingkat I', 'I', 'd', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_pangkat` VALUES ('15', 'Juru', 'I', 'c', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_pangkat` VALUES ('16', 'Juru Muda Tingkat I', 'I', 'b', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_pangkat` VALUES ('17', 'Juru Muda', 'I', 'a', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);

-- ----------------------------
-- Table structure for m_pelatihan
-- ----------------------------
DROP TABLE IF EXISTS `m_pelatihan`;
CREATE TABLE `m_pelatihan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pelatihan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_pelatihan
-- ----------------------------
INSERT INTO `m_pelatihan` VALUES ('1', '001', 'Pelatihan Pembentukan Jabatan Fungsional Pengelola PBJ', '2019-08-25 16:58:13', '2019-08-25 16:58:13', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('2', '002', 'Pelatihan Penjenjangan Jabatan Fungsional Pengelola PBJ', '2019-08-25 16:58:24', '2019-08-25 16:58:42', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('3', '003', 'Pelatihan dalam Jabatan Organisasi (Okupasi) PBJ', '2019-08-25 16:58:46', '2019-08-25 16:59:22', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('4', '004', 'Pelatihan berdasarkan Standar Kompetensi Jabatan', '2019-08-25 17:00:16', '2019-08-25 17:00:42', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('5', '005', 'Pelatihan Keahlian PBJ Tingkat Dasar', '2019-08-25 17:00:54', '2019-08-25 17:01:19', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('6', '006', 'Pelatihan bagi Calon Pelatih (Training of Trainers - TOT) PBJ', '2019-08-25 17:01:22', '2019-08-25 17:01:59', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('7', '007', 'Pelatihan bagi Pengelola Kelas (Training Officer Course)', '2019-08-25 17:02:01', '2019-08-25 17:03:32', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('8', '008', 'Pelatihan bagi Pengelola Pelatihan (Management of Training)', '2019-08-25 17:14:18', '2019-08-25 17:15:06', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('9', '009', 'Pelatihan PBJ di Desa', '2019-08-25 17:15:13', '2019-08-25 17:15:28', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('10', '010', 'Pelatihan Pengadaan Kerja Sama Pemerintah dan Badan Usaha (KPBU)', '2019-08-25 17:15:31', '2019-08-25 17:16:36', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('11', '011', 'Pelatihan Manajerial', '2019-08-28 07:48:59', '2019-08-28 07:48:59', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('12', '012', 'Pelatihan Teknis Pendukung', '2019-08-28 12:00:13', '2019-08-28 12:00:13', null, '1', null, null);

-- ----------------------------
-- Table structure for m_pengalaman_kerja
-- ----------------------------
DROP TABLE IF EXISTS `m_pengalaman_kerja`;
CREATE TABLE `m_pengalaman_kerja` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ket1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ket2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_pengalaman_kerja
-- ----------------------------
INSERT INTO `m_pengalaman_kerja` VALUES ('1', '001', '1', 'Tahun untuk Pengangkatan Pertama', '2', 'Tahun untuk Inpassing', '2019-08-26 15:53:55', '2019-09-02 10:28:29', null, '1', '1', null);
INSERT INTO `m_pengalaman_kerja` VALUES ('2', '002', '4', 'Tahun', null, null, '2019-08-26 15:55:01', '2019-08-27 07:28:23', null, '1', '1', null);
INSERT INTO `m_pengalaman_kerja` VALUES ('3', '003', '8', 'Tahun', null, null, '2019-08-26 15:56:52', '2019-08-27 07:28:30', null, '1', '1', null);

-- ----------------------------
-- Table structure for m_perilaku
-- ----------------------------
DROP TABLE IF EXISTS `m_perilaku`;
CREATE TABLE `m_perilaku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) DEFAULT NULL,
  `perilaku` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=227 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of m_perilaku
-- ----------------------------
INSERT INTO `m_perilaku` VALUES ('1', 'M.A1-P.1', 'Bertingkah laku sesuai dengan perkataan; berkata sesuai dengan fakta.', '2019-08-22 00:00:00', '2019-08-22 16:36:37', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('2', 'M.A1-P.2', 'Melaksanakan peraturan, kode etik organisasi dalam lingkungan kerja sehari- hari, pada tataran individu/pribadi.', '2019-08-22 00:00:00', '2019-08-22 16:36:41', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('3', 'M.A1-P.3', 'Tidak menjanjikan/memberikan sesuatu yang bertentangan dengan aturan organisasi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('4', 'M.A2-P.1', 'Mengingatkan rekan kerja untuk bertindak sesuai dengan nilai, norma, dan etika organisasi dalam segala situasi dan kondisi; Mengajak orang lain untuk bertindak sesuai etika dan kode etik.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('5', 'M.A2-P.2', 'Menerapkan norma-norma secara konsisten dalam setiap situasi, pada unit kerja terkecil/kelompok kerjanya.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('6', 'M.A2-P.3', 'Memberikan informasi yang dapat dipercaya sesuai dengan etika organisasi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('7', 'M.A3-P.1', 'Memastikan anggota yang dipimpin bertindak sesuai dengan nilai, norma,  dan etika organisasi dalam segala situasi dan kondisi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('8', 'M.A3-P.2', 'Mampu untuk memberi apresiasi dan teguran bagi anggota yang dipimpin agar bertindak selaras dengan nilai, norma, dan etika organisasi dalam segala situasi dan kondisi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('9', 'M.A3-P.3', 'Melakukan monitoring dan evaluasi terhadap penerapan sikap integritas di dalam unit kerja yang dipimpin.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('10', 'M.A4-P.1', 'Menciptakan situasi kerja yang mendorong seluruh pemangku kepentingan mematuhi nilai, norma, dan etika organisasi dalam segala situasi dan kondisi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('11', 'M.A4-P.2', 'Mendukung dan menerapkan prinsip moral dan standar etika yang tinggi, serta berani menanggung konsekuensinya.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('12', 'M.A4-P.3', 'Berani melakukan koreksi atau mengambil tindakan atas penyimpangan kode etik/nilai-nilai yang dilakukan oleh orang lain, pada tataran lingkup kerja setingkat instansi meskipun ada resiko.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('13', 'M.A5-P.1', 'Mempertahankan tingkat standar keadilan dan etika yang tinggi dalam perkataan dan tindakan sehari-hari yang dipatuhi oleh seluruh pemangku kepentingan pada lingkup instansi yang dipimpinnya.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('14', 'M.A5-P.2', 'Menjadi “role model” /keteladanan dalam penerapan standar keadilan dan etika yang tinggi di tingkat nasional.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('15', 'M.A5-P.3', 'Membuat konsep kebijakan dan strategi penerapan sikap integritas dalam pelaksanaan tugas dan norma-norma yang sejalan dengan nilai strategis organisasi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('16', 'M.B1-P.1', 'Berpartisipasi sebagai anggota tim yang baik, melakukan tugas/bagiannya, dan mendukung keputusan tim.', '2019-08-22 00:00:00', '2019-08-22 22:33:44', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('17', 'M.B1-P.2', 'Mendengarkan dan menghargai masukan dari orang lain dan memberikan usulan-usulan bagi kepentingan tim.', '2019-08-22 00:00:00', '2019-08-22 22:33:46', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('18', 'M.B1-P.3', 'Mampu menjalin interaksi sosial untuk penyelesaian tugas.', '2019-08-22 00:00:00', '2019-08-22 22:33:51', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('19', 'M.B2-P.1', 'Membantu orang lain dalam menyelesaikan tugas-tugas mereka untuk mendukung sasaran tim.', '2019-08-22 00:00:00', '2019-08-22 22:33:49', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('20', 'M.B2-P.2', 'Berbagi informasi yang relevan atau bermanfaat pada anggota tim; mempertimbangkan masukan dan keahlian anggota dalam tim/kelompok kerja serta bersedia untuk belajar dari orang lain.', '2019-08-22 00:00:00', '2019-08-22 22:33:53', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('21', 'M.B2-P.3', 'Membangun komitmen yang tinggi untuk menyelesaikan tugas tim.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('22', 'M.B3-P.1', 'Melihat kekuatan/kelemahan anggota tim, membentuk tim yang tepat, mengantisipasi kemungkinan hambatan, dan mencari solusi yang optimal.', '2019-08-22 00:00:00', '2019-08-22 22:34:02', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('23', 'M.B3-P.2', 'Mengupayakan dan mengutamakan pengambilan keputusan berdasarkan usulan-usulan anggota tim/kelompok, bernegosiasi secara efektif untuk upaya penyelesaikan pekerjaan yang menjadi target kinerja kelompok dan/atau unit kerja.', '2019-08-22 00:00:00', '2019-08-22 22:34:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('24', 'M.B3-P.3', 'Membangun aliansi dengan para pemangku kepentingan dalam rangka mendukung penyelesaian target kerja kelompok.', '2019-08-22 00:00:00', '2019-08-22 22:34:07', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('25', 'M.B4-P.1', 'Membangun sinergi antar unit kerja di lingkup instansi yang dipimpin.', '2019-08-22 00:00:00', '2019-08-22 22:34:09', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('26', 'M.B4-P.2', 'Memfasilitasi kepentingan yang berbeda dari unit kerja lain sehingga tercipta sinergi dalam rangka pencapaian target kerja organisasi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('27', 'M.B4-P.3', 'Mengembangkan sistem yang menghargai kerja sama antar unit, memberikan dukungan / semangat untuk memastikan tercapainya sinergi dalam rangka pencapaian target kerja organisasi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('28', 'M.B5-P.1', 'Menciptakan hubungan kerja yang konstruktif dengan  menerapkan norma / etos / nilai-nilai kerja yang baik di dalam dan di luar organisasi; meningkatkan produktivitas dan menjadi panutan dalam organisasi.', '2019-08-22 00:00:00', '2019-08-22 22:34:14', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('29', 'M.B5-P.2', 'Secara konsisten menjaga sinergi agar pemangku kepentingan dapat bekerja sama dengan orang di dalam maupun di luar organisasi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('30', 'M.B5-P.3', 'Membangun konsensus untuk menggabungkan sumberdaya dari berbagai pemangku kepentingan untuk tujuan bangsa dan negara.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('31', 'M.C1-P.1', 'Menyampaikan informasi (data), pikiran atau pendapat dengan jelas, singkat dan tepat dengan menggunakan cara/media yang sesuai dan mengikuti alur yang logis;', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('32', 'M.C1-P.2', 'Memastikan pemahaman yang sama atas instruksi yang diterima/diberikan.', '2019-08-22 00:00:00', '2019-08-22 22:34:25', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('33', 'M.C1-P.3', 'Mampu melaksanakan kegiatan surat menyurat sesuai tata naskah organisasi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('34', 'M.C2-P.1', 'Menggunakan gaya komunikasi informal untuk meningkatkan hubungan profesional.', '2019-08-22 00:00:00', '2019-08-22 22:34:29', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('35', 'M.C2-P.2', 'Mendengarkan pihak lain secara aktif; menangkap dan menginterpretasikan pesan-pesan dari orang lain, serta memberikan respon yang sesuai.', '2019-08-22 00:00:00', '2019-08-22 22:34:32', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('36', 'M.C2-P.3', 'Membuat materi presentasi, pidato, draft naskah, laporan dll sesuai arahan pimpinan.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('37', 'M.C3-P.1', 'Menyampaikan suatu informasi yang sensitif/rumit dengan cara penyampaian dan kondisi yang tepat, sehingga dapat dipahami dan diterima oleh pihak lain.', '2019-08-22 00:00:00', '2019-08-22 22:34:36', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('38', 'M.C3-P.2', 'Menyederhanakan topik yang rumit dan sensitif sehingga lebih mudah dipahami dan diterima orang lain.', '2019-08-22 00:00:00', '2019-08-22 22:34:38', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('39', 'M.C3-P.3', 'Membuat laporan tahunan/periodik/naskah/dokumen/proposal yang kompleks; Membuat surat resmi yang sistematis dan tidak menimbulkan pemahaman yang berbeda; membuat proposal yang rinci dan lengkap.', '2019-08-22 00:00:00', '2019-08-22 22:34:41', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('40', 'M.C4-P.1', 'Mengintegrasikan informasi-informasi penting dari berbagai sumber dengan pihak lain untuk mendapatkan pemahaman yang sama.', '2019-08-22 00:00:00', '2019-08-22 22:34:44', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('41', 'M.C4-P.2', 'Menuangkan pemikiran/konsep dari berbagai sudut pandang/multidimensi dalam bentuk tulisan formal.', '2019-08-22 00:00:00', '2019-08-22 22:34:49', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('42', 'M.C4-P.3', 'Menyampaikan informasi secara persuasif untuk mendorong pemangku kepentingan sepakat pada langkah-langkah bersama dengan tujuan meningkatkan kinerja secara keseluruhan.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('43', 'M.C5-P.1', 'Menghilangkan hambatan komunikasi, mampu berkomunikasi dalam isu-isu nasional yang memiliki resiko tinggi, menggalang hubungan dalam skala strategis di tingkat nasional.', '2019-08-22 00:00:00', '2019-08-22 22:34:52', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('44', 'M.C5-P.2', 'Menggunakan saluran komunikasi formal dan non formal guna mencapai kesepakatan dengan tujuan meningkatkan kinerja di tingkat instansi/nasional.', '2019-08-22 00:00:00', '2019-08-22 22:34:54', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('45', 'M.C5-P.3', 'Menggagas sistem komunikasi dengan melibatkan pemangku kepentingan sejak dini untuk mencari solusi dengan tujuan meningkatkan kinerja di tingkat instansi/nasional.', '2019-08-22 00:00:00', '2019-08-22 22:34:56', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('46', 'M.D1-P.1', 'Menyelesaikan tugas dengan tuntas; dapat diandalkan.', '2019-08-22 00:00:00', '2019-08-22 22:34:59', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('47', 'M.D1-P.2', 'Bekerja dengan teliti dan hati-hati guna meminimalkan kesalahan dengan mengacu pada standar kualitas (SOP).', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('48', 'M.D1-P.3', 'Bersedia menerima masukan, mengikuti contoh cara bekerja yang lebih efektif, efisien di lingkungan kerjanya.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('49', 'M.D2-P.1', 'Menetapkan dan berupaya mencapai standar kerja pribadi yang lebih tinggi dari standar kerja yang ditetapkan organisasi.', '2019-08-22 00:00:00', '2019-08-22 22:35:02', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('50', 'M.D2-P.2', 'Mencari, mencoba metode kerja alternatif untuk meningkatkan hasil kerjanya.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('51', 'M.D2-P.3', 'Memberi contoh kepada orang-orang di unit kerjanya untuk mencoba menerapkan metode kerja yang lebih efektif yang sudah dilakukannya.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('52', 'M.D3-P.1', 'Menetapkan target kinerja unit yang lebih tinggi dari target yang ditetapkan organisasi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('53', 'M.D3-P.2', 'Memberikan apresiasi dan teguran untuk mendorong pencapaian hasil unit kerjanya.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('54', 'M.D3-P.3', 'Mengembangkan metode kerja yang lebih efektif dan efisien untuk mencapai target kerja unitnya.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('55', 'M.D4-P.1', 'Mendorong unit kerja di tingkat instansi untuk mencapai kinerja yang melebihi target yang ditetapkan.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('56', 'M.D4-P.2', 'Memantau dan mengevaluasi hasil kerja unitnya agar selaras dengan sasaran strategis instansi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('57', 'M.D4-P.3', 'Mendorong pemanfaatan sumber daya bersama antar unit kerja dalam rangka meningkatkan efektifitas dan efisiensi pencaian target organisasi.', '2019-08-22 00:00:00', '2019-08-22 22:35:09', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('58', 'M.D5-P.1', 'Memastikan kualitas sesuai standar dan keberlanjutan hasil kerja organisasi yang memberi kontribusi pada pencapaian target prioritas nasional.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('59', 'M.D5-P.2', 'Memastikan tersedianya sumber daya organisasi untuk menjamin tercapainya target prioritas instansi/nasional.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('60', 'M.D5-P.3', 'Membuat kebijakan untuk menerapkan metode kerja yang lebih efektif-efisien dalam mencapai tujuan prioritas nasional.', '2019-08-22 00:00:00', '2019-08-22 22:35:11', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('61', 'M.E1-P.1', 'Mampu mengerjakan tugas-tugas dengan mengikuti standar pelayanan yang objektif, netral, tidak memihak, tidak diskriminatif, transparan dan tidak terpengaruh kepentingan pribadi/kelompok/partai politik.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('62', 'M.E1-P.2', 'Melayani kebutuhan, permintaan dan keluhan pemangku kepentingan.', '2019-08-22 00:00:00', '2019-08-22 22:35:17', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('63', 'M.E1-P.3', 'Menyelesaikan masalah dengan tepat tanpa bersikap membela diri dalam kapasitas sebagai pelaksana pelayanan publik.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('64', 'M.E2-P.1', 'Menunjukan sikap yakin dalam mengerjakan tugas-tugas pemerintahan/pelayanan publik, mampu menyelia dan menjelaskan secara obyektif bila ada yang mempertanyakan kebijakan yang diambil.', '2019-08-22 00:00:00', '2019-08-22 22:35:21', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('65', 'M.E2-P.2', 'Secara aktif mencari informasi untuk mengenali kebutuhan pemangku kepentingan agar dapat menjalankan pelaksanaan tugas pemerintahan, pembangunan dan pelayanan publik secara cepat dan tanggap;', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('66', 'M.E2-P.3', 'Mampu mengenali dan memanfaatkan kebiasaan, tatacara, situasi tertentu sehingga apa yang disampaikan menjadi perhatian pemangku kepentingan dalam hal penyelesaian tugas-tugas pemerintahan, pembangunan dan pelayanan publik.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('67', 'M.E3-P.1', 'Memahami, mendeskripsikan pengaruh dan hubungan/kekuatan kelompok yang sedang berjalan di organisasi (aliansi atau persaingan), dan dampaknya terhadap unit kerja untuk menjalankan tugas pemerintahan secara profesional dan netral, tidak memihak.', '2019-08-22 00:00:00', '2019-08-22 22:35:25', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('68', 'M.E3-P.2', 'Menggunakan keterampilan dan pemahaman lintas organisasi untuk secara efektif memfasilitasi kebutuhan kelompok yang lebih besar dengan cara-cara yang mengikuti standar objektif, transparan, profesional, sehingga tidak merugikan para pihak di lingkup pelayanan publik unit kerjanya.', '2019-08-22 00:00:00', '2019-08-22 22:35:28', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('69', 'M.E3-P.3', 'Mengimplementasikan cara-cara yang efektif untuk memantau dan mengevaluasi masalah yang dihadapi pemangku kepentingan/masyarakat serta mengantisipasi kebutuhan mereka saat menjalankan tugas pelayanan publik di unit kerjanya.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('70', 'M.E4-P.1', 'Memahami dan memberi perhatian kepada isu-isu jangka panjang, kesempatan atau kekuatan politik yang mempengaruhi organisasi dalam hubungannya dengan dunia luar, memperhitungkan dan mengantisipasi dampak terhadap pelaksanaan tugas- tugas pelayanan publik secara objektif, transparan, dan professional dalam lingkup organisasi.', '2019-08-22 00:00:00', '2019-08-22 22:35:35', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('71', 'M.E4-P.2', 'Menjaga agar kebijakan pelayanan publik yang diselenggarakan oleh instansinya telah selaras dengan standar pelayanan yang objektif, netral, tidak memihak, tidak diskriminatif, serta tidak terpengaruh kepentingan pribadi/kelompok/partai politik.', '2019-08-22 00:00:00', '2019-08-22 22:35:38', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('72', 'M.E4-P.3', 'Menerapkan strategi jangka panjang yang berfokus pada pemenuhan kebutuhan pemangku kepentingan dalam menyusun kebijakan dengan mengikuti standar objektif,  netral, tidak memihak, tidak diskriminatif, transparan, tidak terpengaruh kepentingan pribadi/kelompok.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('73', 'M.E5-P.1', 'Mampu menciptakan kebijakan kebijakan pelayanan publik yang menjamin terselenggaranya pelayanan publik yang objektif, netral, tidak memihak, tidak diskriminatif, serta tidak terpengaruh kepentingan pribadi/kelompok/partai politik.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('74', 'M.E5-P.2', 'Menginternalisasikan nilai dan semangat pelayanan publik yang mengikuti standar objektif,  netral, tidak memihak, tidak diskriminatif, transparan, tidak terpengaruh kepentingan pribadi/kelompok kepada setiap individu di lingkungan instansi/nasional.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('75', 'M.E5-P.3', 'Menjamin terselenggaranya pelayanan publik yang objektif, netral, tidak memihak, tidak diskriminatif, serta tidak terpengaruh kepentingan pribadi/kelompok/partai politik.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('76', 'M.F1-P.1', 'Mengidentifikasi kebutuhan pengembangan diri dan menyeleksi sumber serta metodologi pembelajaran yang diperlukan.', '2019-08-22 00:00:00', '2019-08-22 22:35:46', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('77', 'M.F1-P.2', 'Menunjukkan usaha mandiri untuk mempelajari keterampilan atau kemampuan baru dari berbagai media pembelajaran.', '2019-08-22 00:00:00', '2019-08-22 22:35:48', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('78', 'M.F1-P.3', 'Berupaya meningkatkan diri dengan belajar dari orang-orang lain yang berwawasan luas di dalam organisasi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('79', 'M.F2-P.1', 'Meningkatkan kemampuan bawahan dengan memberikan contoh, instruksi, penjelasan dan petunjuk praktis yang jelas kepada bawahan dalam menyelesaikan suatu pekerjaan.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('80', 'M.F2-P.2', 'Membantu bawahan untuk mempelajari proses, program atau sistem baru.', '2019-08-22 00:00:00', '2019-08-22 22:35:51', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('81', 'M.F2-P.3', 'Menggunakan metode lain untuk meyakinkan bahwa orang lain telah memahami penjelasan atau pengarahan.', '2019-08-22 00:00:00', '2019-08-22 22:35:57', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('82', 'M.F3-P.1', 'Memberikan tugas-tugas yang menantang pada bawahan sebagai media belajar untuk mengembangkan kemampuannya.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('83', 'M.F3-P.2', 'Mengamati bawahan dalam mengerjakan tugasnya dan memberikan umpan balik yang objektif dan jujur; melakukan diskusi dengan bawahan untuk memberikan bimbingan dan umpan balik yang berguna bagi bawahan.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('84', 'M.F3-P.3', 'Mendorong kepercayaan diri bawahan; memberikan kepercayaan penuh pada bawahan untuk mengerjakan tugas dengan caranya sendiri; memberi kesempatan dan membantu bawahan menemukan peluang untuk berkembang.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('85', 'M.F4-P.1', 'Menyusun program pengembangan jangka panjang bersama-sama dengan bawahan, termasuk didalamnya penetapan tujuan, bimbingan, penugasan dan pengalaman lainnya, serta mengalokasikan waktu untuk mengikuti pelatihan/pendidikan/ pengembangan kompetensi dan karir.', '2019-08-22 00:00:00', '2019-08-22 22:36:02', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('86', 'M.F4-P.2', 'Melaksanakan manajemen pembelajaran termasuk evaluasi dan umpan balik pada tataran organisasi.', '2019-08-22 00:00:00', '2019-08-22 22:36:04', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('87', 'M.F4-P.3', 'Mengembangkan orang-orang disekitarnya secara konsisten, melakukan kaderisasi untuk posisi-posisi di unit kerjanya.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('88', 'M.F5-P.1', 'Menciptakan situasi yang mendorong individu, kelompok, unit kerja untuk mengembangkan kemampuan belajar secara berkelanjutan di tingkat instansi.', '2019-08-22 00:00:00', '2019-08-22 22:36:07', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('89', 'M.F5-P.2', 'Merekomendasikan/memberikan penghargaan bagi upaya pengembangan yang berhasil, memastikan dukungan bagi orang lain dalam mengembangkan kemampuan dalam unit kerja di tingkat instansi.', '2019-08-22 00:00:00', '2019-08-22 22:36:09', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('90', 'M.F5-P.3', 'Memberikan inspirasi kepada individu atau kelompok untuk belajar secara berkelanjutan dalam penerapan di tingkat instansi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('91', 'M.G1-P.1', 'Sadar mengenai perubahan  yang  terjadi di organisasi dan berusaha menyesuaikan diri dengan perubahan tersebut.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('92', 'M.G1-P.2', 'Mengikuti perubahan secara terbuka sesuai petunjuk/pedoman.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('93', 'M.G1-P.3', 'Menyesuaikan cara kerja lama dengan menerapkan metode/proses baru dengan bimbingan orang lain.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('94', 'M.G2-P.1', 'Menyesuaikan cara kerja lama dengan menerapkan metode/proses baru selaras dengan ketentuan yang berlaku tanpa arahan orang lain.', '2019-08-22 00:00:00', '2019-08-22 22:36:15', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('95', 'M.G2-P.2', 'Mengembangkan kemampuan diri untuk menghadapi perubahan.', '2019-08-22 00:00:00', '2019-08-22 22:36:18', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('96', 'M.G2-P.3', 'Cepat dan tanggap dalam menerima perubahan.', '2019-08-22 00:00:00', '2019-08-22 22:36:20', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('97', 'M.G3-P.1', 'Membantu orang lain dalam melakukan perubahan.', '2019-08-22 00:00:00', '2019-08-22 22:36:22', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('98', 'M.G3-P.2', 'Menyesuaikan prioritas kerja secara berulang-ulang jika diperlukan.', '2019-08-22 00:00:00', '2019-08-22 22:36:24', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('99', 'M.G3-P.3', 'Mengantisipasi perubahan yang dibutuhkan oleh unit kerjanya secara tepat. Memberikan solusi efektif terhadap masalah yang ditimbulkan oleh adanya perubahan.', '2019-08-22 00:00:00', '2019-08-22 22:36:26', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('100', 'M.G4-P.1', 'Mengarahkan unit kerja untuk lebih siap dalam menghadapi perubahan termasuk memitigasi risiko yang mungkin terjadi.', '2019-08-22 00:00:00', '2019-08-22 22:36:29', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('101', 'M.G4-P.2', 'Memastikan perubahan sudah diterapkan secara aktif di lingkup unit kerjanya secara berkala.', '2019-08-22 00:00:00', '2019-08-22 22:36:35', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('102', 'M.G4-P.3', 'Memimpin dan memastikan penerapan program-program perubahan selaras antar unit kerja.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('103', 'M.G5-P.1', 'Membuat kebijakan-kebijakan yang mendorong perubahan  yang berdampak pada pencapaian sasaran prioritas nasional.', '2019-08-22 00:00:00', '2019-08-22 22:36:39', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('104', 'M.G5-P.2', 'Menggalang dan menggerakkan dukungan para pemangku kepentingan untuk mengimplementasikan perubahan yang telah ditetapkan.', '2019-08-22 00:00:00', '2019-08-22 22:36:42', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('105', 'M.G5-P.3', 'Secara berkelanjutan, mencari cara- cara baru untuk memberi nilai tambah bagi perubahan yang tengah dijalankan agar memberi manfaat yang lebih besar bagi para pemangku kepentingan.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('106', 'M.H1-P.1', 'Mengumpulkan dan mempertimbangkan informasi yang dibutuhkan dalam mencari solusi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('107', 'M.H1-P.2', 'Mengenali situasi/pilihan yang tepat untuk bertindak sesuai kewenangan.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('108', 'M.H1-P.3', 'Mempertimbangkan kemungkinan solusi yang dapat diterapkan dalam pekerjaan rutin berdasarkan kebijakan dan prosedur yang telah ditentukan.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('109', 'M.H2-P.1', 'Melakukan analisis secara mendalam terhadap informasi yang tersedia dalam upaya mencari solusi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('110', 'M.H2-P.2', 'Mempertimbangkan berbagai alternatif yang ada sebelum membuat kesimpulan.', '2019-08-22 00:00:00', '2019-08-22 22:36:51', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('111', 'M.H2-P.3', 'Membuat keputusan operasional berdasarkan kesimpulan dari berbagai sumber informasi sesuai dengan pedoman yang ada.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('112', 'M.H3-P.1', 'Membandingkan berbagai alternatif tindakan dan implikasinya.', '2019-08-22 00:00:00', '2019-08-22 22:36:55', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('113', 'M.H3-P.2', 'Memilih alternatif solusi yang terbaik, membuat keputusan operasional mengacu pada alternatif solusi terbaik yang didasarkan pada analisis data yang sistematis, seksama, mengikuti prinsip kehati-hatian.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('114', 'M.H3-P.3', 'Menyeimbangkan antara kemungkinan risiko dan keberhasilan dalam implementasinya.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('115', 'M.H4-P.1', 'Menyusun dan/atau memutuskan konsep penyelesaian masalah yang melibatkan beberapa/seluruh fungsi dalam organisasi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('116', 'M.H4-P.2', 'Menghasilkan solusi dari berbagai masalah yang kompleks, terkait dengan bidang kerjanya yang berdampak pada pihak lain.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('117', 'M.H4-P.3', 'Membuat keputusan dan mengantisipasi dampak keputusannya serta menyiapkan tindakan penanganannya (mitigasi risiko).', '2019-08-22 00:00:00', '2019-08-22 22:36:59', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('118', 'M.H5-P.1', 'Menghasilkan solusi yang dapat mengatasi permasalahan jangka panjang.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('119', 'M.H5-P.2', 'Menghasilkan solusi strategis yang berdampak pada tataran instansi/nasional.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('120', 'M.H5-P.3', 'Membuat keputusan atau kebijakan yang berdampak nasional dengan memitigasi risiko yang mungkin timbul.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('121', 'S.A1-P.1', 'Mampu memahami, menerima, peka terhadap perbedaan individu/kelompok masyarakat.', '2019-08-22 00:00:00', '2019-08-22 22:37:08', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('122', 'S.A1-P.2', 'Terbuka, ingin belajar tentang perbedaan/kemajemukan masyarakat.', '2019-08-22 00:00:00', '2019-08-22 22:37:11', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('123', 'S.A1-P.3', 'Mampu bekerja bersama dengan individu yang berbeda latar belakang dengan-nya.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('124', 'S.A2-P.1', 'Menampilkan sikap dan perilaku yang peduli akan nilai-nilai keberagaman dan menghargai perbedaan.', '2019-08-22 00:00:00', '2019-08-22 22:37:14', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('125', 'S.A2-P.2', 'Membangun hubungan baik antar individu dalam organisasi, mitra kerja, pemangku kepentingan.', '2019-08-22 00:00:00', '2019-08-22 22:37:36', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('126', 'S.A2-P.3', 'Bersikap tenang, mampu mengendalikan emosi, kemarahan dan frustasi dalam menghadapi pertentangan yang ditimbulkan oleh perbedaan latar belakang, agama/kepercayaan, suku, jender, sosial ekonomi, preferensi politik di lingkungan unit kerjanya.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('127', 'S.A3-P.1', 'Mempromosikan sikap menghargai perbedaan di antara orang-orang yang mendorong toleransi dan keterbukaan.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('128', 'S.A3-P.2', 'Melakukan pemetaan sosial di masyarakat sehingga dapat memberikan respon yang sesuai dengan budaya yang berlaku. Mengidentifikasi potensi kesalah- pahaman yang diakibatkan adanya keragaman budaya yang ada.', '2019-08-22 00:00:00', '2019-08-22 22:37:39', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('129', 'S.A3-P.3', 'Menjadi mediator untuk menyelesaikan konflik atau mengurangi dampak negatif dari konflik atau potensi konflik.', '2019-08-22 00:00:00', '2019-08-22 22:37:46', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('130', 'S.A4-P.1', 'Menginisiasi dan merepresentasikan pemerintah di lingkungan kerja dan masyarakat untuk senantiasa menjaga persatuan dan kesatuan dalam keberagaman dan menerima segala bentuk perbedaan dalam kehidupan bermasyarakat.', '2019-08-22 00:00:00', '2019-08-22 22:37:50', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('131', 'S.A4-P.2', 'Mampu mendayagunakan perbedaan latar belakang, agama/kepercayaan, suku, jender, sosial ekonomi, preferensi politik untuk mencapai kelancaran pencapaian tujuan organisasi.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('132', 'S.A4-P.3', 'Mampu membuat program yang mengakomodasi perbedaan latar belakang, agama/kepercayaan, suku, jender, sosial ekonomi, preferensi politik.', '2019-08-22 00:00:00', '2019-08-22 22:37:55', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('133', 'S.A5-P.1', 'Menjadi wakil pemerintah yang mampu membangun hubungan sosial psikologis dengan masyarakat sehingga menciptakan kelekatan yang kuat antara ASN dan para pemangku kepentingan serta     diantara para pemangku kepentingan itu sendiri.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('134', 'S.A5-P.2', 'Mampu mengkomunikasikan dampak risiko yang teridentifikasi dan merekomendasikan tindakan korektif berdasarkan pertimbangan perbedaan latar belakang, agama/kepercayaan, suku, jender, sosial ekonomi, preferensi politik untuk membangun hubungan jangka panjang.', '2019-08-22 00:00:00', '2019-08-22 22:38:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('135', 'S.A5-P.3', 'Mampu membuat kebijakan yang mengakomodasi perbedaan latar belakang, agama/kepercayaan, suku, jender, sosial ekonomi, preferensi politik yang berdampak positif secara nasional.', '2019-08-22 00:00:00', '2019-08-22 22:38:03', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('136', 'A1-P.1', 'Memahami tentang Identifikasi/Reviu Kebutuhan dan Penetapan Barang/Jasa pada Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('137', 'A1-P.2', 'Memahami tentang Penyusunan Spesifikasi Teknis dan Kerangka Acuan Kerja (KAK) pada Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('138', 'A1-P.3', 'Memahami tentang Penyusunan Perkiraan Harga untuk setiap tahapan Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('139', 'A1-P.4', 'Memahami tentang Perumusan Strategi Pengadaan, Pemaketan, dan Cara Pengadaan pada Pengadaan Barang/Jasa Pemerintah, serta masing-masing faktor yang mempengaruhinya.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('140', 'A1-P.5', 'Memahami tentang Perumusan Organisasi Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('141', 'A1-P.6', 'Memahami resiko perencanaan Pengadaan Barang/Jasa Pemerintah. ', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('142', 'A1-P.7', 'Mampu mengidentifikasi dan mengumpulkan bahan dan/atau data dan/atau informasi yang dibutuhkan untuk melakukan perencanaan Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('143', 'A2-P.1', 'Mampu melakukan pekerjaan penyusunan Spesifikasi Teknis berbasis keluaran (output) untuk Barang/Jasa sederhana yang banyak tersedia di pasar dan Kerangka Acuan Kerja (KAK) Jasa Konsultansi perorangan.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('144', 'A2-P.2', 'Mampu melakukan pekerjaan penyusunan perkiraan harga berbasis harga pasar, standar harga, dan harga paket pekerjaan sejenis yang pernah dilaksanakan.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('145', 'A2-P.3', 'Mampu melakukan pekerjaan Identifikasi/Reviu Kebutuhan dan Penetapan Barang/Jasa.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('146', 'A3-P.1', 'Mampu melakukan pekerjaan penyusunan Spesifikasi Teknis dan Kerangka Acuan Kerja (KAK) berbasis masukan (input), proses, dan keluaran (ouput) sesuai dengan analisis proses produksi/pelaksanaan pekerjaan, untuk pekerjaan yang tidak kompleks dan/atau tidak membutuhkan kompetensi teknis yang spesifik.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('147', 'A3-P.2', 'Mampu melakukan pekerjaan penyusunan Perkiraan Harga sesuai dengan analisis proses produksi/pelaksanaan, untuk pekerjaan yang tidak kompleks dan/atau tidak membutuhkan kompetensi teknis yang spesifik.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('148', 'A3-P.3', 'Mampu melakukan perumusan Pemaketan dan Cara Pengadaan sesuai Strategi Pengadaan.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('149', 'A4-P.1', 'Mampu melakukan pekerjaan penyusunan Spesifikasi Teknis dan Kerangka Acuan Kerja (KAK) berbasis masukan (input), proses, dan keluaran (ouput) sesuai dengan analisis proses produksi/pelaksanaan pekerjaan, untuk pekerjaan yang kompleks dan/atau membutuhkan kompetensi teknis yang spesifik.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('150', 'A4-P.2', 'Mampu melakukan pekerjaan penyusunan Perkiraan Harga sesuai dengan analisis proses produksi/pelaksanaan, untuk pekerjaan yang kompleks dan/atau membutuhkan kompetensi teknis yang spesifik.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('151', 'A4-P.3', 'Mampu melakukan perumusan Strategi Pengadaan yang sesuai tujuan organisasi dan/atau tujuan Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('152', 'A4-P.4', 'Mampu melakukan perumusan Organisasi Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('153', 'A5-P.1', 'Mampu melakukan pengembangan konsep, teori, dan/atau kebijakan tentang Identifikasi/Reviu Kebutuhan dan Penetapan Barang/Jasa pada Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('154', 'A5-P.2', 'Mampu melakukan pengembangan konsep, teori, dan/atau kebijakan tentang Penyusunan Spesifikasi Teknis dan Kerangka Acuan Kerja (KAK).', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('155', 'A5-P.3', 'Mampu melakukan pengembangan konsep, teori, dan/atau kebijakan tentang Penyusunan Perkiraan Harga untuk setiap tahapan Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('156', 'A5-P.4', 'Mampu melakukan pengembangan konsep, teori, dan/atau kebijakan tentang Perumusan Strategi Pengadaan, Pemaketan, dan Cara Pengadaan, serta masing-masing faktor yang mempengaruhinya.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('157', 'A5-P.5', 'Mampu melakukan pengembangan konsep, teori, dan/atau kebijakan tentang Perumusan Organisasi Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('158', 'B1-P.1', 'Mampu Memahami tentang Reviu  terhadap Dokumen Persiapan Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('159', 'B1-P.2', 'Mampu Memahami tentang Penyusunan dan Penjelasan Dokumen Pemilihan, Penilaian Kualifikasi, Evaluasi Penawaran, dan Pengelolaan Sanggah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('160', 'B1-P.3', 'Mampu Memahami tentang Penyusunan Daftar Penyedia Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('161', 'B1-P.4', 'Mampu Memahami tentang Negosiasi dalam Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('162', 'B1-P.5', 'Mampu memahami Pengadaan Barang/Jasa Pemerintah dengan persyaratan khusus dan/atau spesifik.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('163', 'B1-P.6', 'Memahami resiko pemilihan penyedia Pengadaan Barang/Jasa Pemerintah. ', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('164', 'B1-P.7', 'Mampu mengidentifikasi dan mengumpulkan bahan dan/atau data dan/atau informasi yang dibutuhkan untuk melakukan Reviu  terhadap Dokumen Persiapan Pengadaan Barang/Jasa Pemerintah, Penyusunan dan Penjelasan Dokumen Pemilihan, Penilaian Kualifikasi, Evaluasi Penawaran, Pengelolaan Sanggah, Penyusunan Daftar Penyedia, atau Negosiasi dalam Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('165', 'B2-P.1', 'Mampu melakukan pekerjaan Reviu  terhadap Dokumen Persiapan Pengadaan Barang/Jasa Pemerintah untuk pekerjaan dengan proses Pengadaan Barang/Jasa yang sederhana.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('166', 'B2-P.2', 'Mampu melakukan pekerjaan Penyusunan dan Penjelasan Dokumen Pemilihan yang dilakukan melalui metode pemilihan Pengadaan Langsung dan Tender Cepat.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('167', 'B2-P.3', 'Mampu melakukan pekerjaan Evaluasi Penawaran dengan metode evaluasi harga terendah sistem gugur.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('168', 'B2-P.4', 'Mampu melakukan pekerjaan Penilaian Kualifikasi pada Pengadaan Langsung.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('169', 'B2-P.5', 'Mampu melakukan pekerjaan Negosiasi dalam Pengadaan Barang/Jasa Pemerintah dengan mengacu pada Harga Perkiraan Sendiri (HPS) dan standar harga/biaya. ', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('170', 'B2-P.6', 'Mampu melakukan Pengadaan Barang/Jasa secara E-Purchasing dan Pembelian melalui Toko Daring.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('171', 'B3-P.1', 'Mampu melakukan pekerjaan Reviu terhadap Dokumen Persiapan Pengadaan Barang/Jasa Pemerintah untuk pekerjaan dengan proses Pengadaan Barang/Jasa yang tidak sederhana.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('172', 'B3-P.2', 'Mampu melakukan pekerjaan Penyusunan dan Penjelasan Dokumen Pemilihan yang dilakukan melalui metode pemilihan Tender/Seleksi dan Penunjukan Langsung.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('173', 'B3-P.3', 'Mampu melakukan pekerjaan Evaluasi Penawaran dengan metode evaluasi harga terendah ambang batas, sistem nilai, penilaian biaya selama umur ekonomis, kualitas, kualitas dan biaya, pagu anggaran, dan biaya terendah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('174', 'B3-P.4', 'Mampu melakukan pekerjaan Penilaian Kualifikasi pada Tender/Seleksi dan Penunjukan Langsung.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('175', 'B3-P.5', 'Mampu melakukan pekerjaan Pengelolaan Sanggah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('176', 'B3-P.6', 'Mampu melakukan pekerjaan Negosiasi Teknis dan Harga dalam Pengadaan Barang/Jasa Pemerintah dengan mengacu pada analisis proses produksi, metode pelaksanaan dan/atau harga pokok penjualan (HPP). ', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('177', 'B3-P.7', 'Mampu melakukan pekerjaan Penyusunan Daftar Penyedia Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('178', 'B4-P.1', 'Mampu melakukan pekerjaan Penyusunan dan Penjelasan Dokumen Pemilihan seperti pada Pengadaan Barang/Jasa pekerjaan terintegrasi, Tender/Seleksi Internasional, dan/atau pada Pengadaan Badan Usaha Pelaksana Kerjasama Pemerintah dengan Badan Usaha.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('179', 'B4-P.2', 'Mampu melakukan pekerjaan Evaluasi Penawaran seperti pada Pengadaan pekerjaan terintegrasi, Tender/Seleksi Internasional, dan/atau pada Pengadaan Badan Usaha Pelaksana Kerjasama Pemerintah dengan Badan Usaha.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('180', 'B4-P.3', 'Mampu melakukan pekerjaan Penilaian Kualifikasi seperti pada Pengadaan pekerjaan terintegrasi, Tender/Seleksi Internasional, dan/atau pada Pengadaan Badan Usaha Pelaksana Kerjasama Pemerintah dengan Badan Usaha.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('181', 'B5-P.1', 'Mampu melakukan pengembangan konsep, teori, dan kebijakan tentang Reviu  terhadap Dokumen Persiapan Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('182', 'B5-P.2', 'Mampu melakukan pengembangan konsep, teori, dan kebijakan tentang Penyusunan dan Penjelasan Dokumen Pemilihan, Penilaian Kualifikasi, Evaluasi Penawaran, Pengelolaan Sanggah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('183', 'B5-P.3', 'Mampu melakukan pengembangan konsep, teori dan kebijakan tentang Penyusunan Daftar Penyedia.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('184', 'B5-P.4', 'Mampu melakukan pengembangan konsep, teori dan kebijakan tentang Negosiasi dalam Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('185', 'C1-P.1', 'Memahami tentang  Perumusan Kontrak Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('186', 'C1-P.2', 'Memahami tentang  Pembentukan Tim Pengelola Kontrak.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('187', 'C1-P.3', 'Memahami tentang  Pengendalian Pelaksanaan Kontrak Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('188', 'C1-P.4', 'Memahami tentang  Serah Terima Hasil Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('189', 'C1-P.5', 'Memahami tentang  Evaluasi Kinerja Penyedia Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('190', 'C1-P.6', 'Mampu melakukan pekerjaan Perumusan Kontrak Pengadaan Barang/Jasa Pemerintah dalam bentuk kuitansi, bukti pembayaran/pembelian, dan surat pesanan.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('191', 'C1-P.7', 'Memahami resiko pengelolaan kontrak Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('192', 'C1-P.8', 'Mampu mengidentifikasi dan mengumpulkan bahan dan/atau data dan/atau informasi yang dibutuhkan untuk melakukan persiapan dan pelaksanaan pengendalian kontrak Pengadaan Barang/Jasa Pemerintah, serta Evaluasi Kinerja Penyedia Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('193', 'C2-P.1', 'Mampu melakukan pekerjaan Perumusan Kontrak Pengadaan Barang/Jasa Pemerintah dalam bentuk Surat Perintah Kerja (SPK).', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('194', 'C2-P.2', 'Mampu melakukan pekerjaan Pengendalian Pelaksanaan Kontrak Pengadaan Barang/Jasa Pemerintah terhadap keluaran (output)  hasil pekerjaan berupa Barang/Jasa yang sederhana dan banyak tersedia di pasar.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('195', 'C2-P.3', 'Mampu melakukan pekerjaan Serah Terima Hasil Pengadaan Barang/Jasa Pemerintah hasil pekerjaan atau keluaran (output)  berupa Barang/Jasa yang sederhana dan banyak tersedia di pasar.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('196', 'C2-P.4', 'Mampu melakukan pekerjaan Evaluasi Kinerja Penyedia Pengadaan Barang/Jasa Pemerintah sesuai pedoman/panduan dan ketentuan.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('197', 'C3-P.1', 'Mampu melakukan pekerjaan Perumusan Kontrak Pengadaan Barang/Jasa Pemerintah dalam bentuk Surat Perjanjian.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('198', 'C3-P.2', 'Mampu melakukan pekerjaan Pengendalian Pelaksanaan Kontrak Pengadaan Barang/Jasa Pemerintah terhadap masukan (input), proses produksi/pelaksanaan, dan keluaran (output) hasil pekerjaan  berupa Barang/Jasa yang tidak kompleks.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('199', 'C3-P.3', 'Mampu melakukan pekerjaan Serah Terima Hasil Pengadaan Barang/Jasa Pemerintah berupa Barang/Jasa yang tidak kompleks setelah melalui proses penjaminan mutu (quality assurance).', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('200', 'C4-P.1', 'Mampu melakukan pekerjaan Perumusan Kontrak Pengadaan Barang/Jasa Pemerintah dalam bentuk Surat Perjanjian untuk Kontrak Pekerjaan Terintegrasi, Kontrak Payung, Kontrak Pengadaan Barang/Jasa Internasional, dan kontrak yang pemilihan penyedia Barang/Jasanya dilakukan itemized.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('201', 'C4-P.2', 'Mampu melakukan pekerjaan Pengendalian Pelaksanaan Kontrak Pengadaan Barang/Jasa Pemerintah terhadap masukan (input), proses produksi/pelaksanaan, dan keluaran (output) hasil pekerjaan  berupa Barang/Jasa yang kompleks.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('202', 'C4-P.3', 'Mampu melakukan pekerjaan Pembentukan Tim Pengelola Kontrak Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('203', 'C4-P.4', 'Mampu melakukan pekerjaan Serah Terima Hasil Pengadaan Barang/Jasa Pemerintah berupa Barang/Jasa yang kompleks setelah melalui proses penjaminan mutu (quality assurance).', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('204', 'C4-P.5', 'Mampu melakukan pekerjaan penyusunan instrumen Evaluasi Kinerja Penyedia Pengadaan Barang/Jasa Pemerintah. ', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('205', 'C5-P.1', 'Mampu melakukan pengembangan konsep, teori dan kebijakan Perumusan Kontrak Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('206', 'C5-P.2', 'Mampu melakukan pengembangan konsep, teori dan kebijakan Pembentukan Tim Pengelola Kontrak Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('207', 'C5-P.3', 'Mampu melakukan pengembangan konsep, teori dan kebijakan Pengendalian Pelaksanaan Kontrak Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('208', 'C5-P.4', 'Mampu melakukan pengembangan konsep, teori dan kebijakan Serah Terima Hasil Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('209', 'C5-P.5', 'Mampu melakukan pengembangan konsep, teori dan kebijakan Evaluasi Kinerja Penyedia Pengadaan Barang/Jasa Pemerintah.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('210', 'D1-P.1', 'Memahami tentang Penyusunan Rencana dan Persiapan Pengadaan Barang/Jasa Pemerintah secara Swakelola.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('211', 'D1-P.2', 'Memahami tentang Pelaksanaan Pengadaan Barang/Jasa Pemerintah secara Swakelola.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('212', 'D1-P.3', 'Memahami tentang Pengawasan, Pengendalian, dan Pelaporan Pengadaan Barang/Jasa Pemerintah secara Swakelola.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('213', 'D1-P.4', 'Memahami resiko pengelolaan Pengadaan Barang/Jasa Pemerintah secara Swakelola. ', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('214', 'D1-P.5', 'Mampu mengidentifikasi dan mengumpulkan bahan dan/atau data dan/atau informasi yang dibutuhkan untuk melakukan perencanaan,  pelaksanaan, dan pengawasan Pengadaan Barang/Jasa Pemerintah secara Swakelola.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('215', 'D2-P.1', 'Mampu melakukan pekerjaan Penyusunan Rencana, Persiapan, Pelaksanaan, dan Pengawasan pada Pengadaan Barang/Jasa Pemerintah secara Swakelola sesuai pedoman/panduan dan ketentuan.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('216', 'D2-P.2', 'Mampu Melakukan pengelolaan Pengadaan Barang/Jasa secara Swakelola untuk pekerjaan dengan masukan (input) berupa personil dengan keahlian tertentu dan bahan/material sederhana, proses/metode pelaksanaan telah jelas standar/pedomannya dengan variasi pelaksanaan yang rendah dan keluaran (output) yang dapat diukur secara kuantitatif.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('217', 'D3-P.1', 'Mampu melakukan analisis dan pemecahan masalah teknis operasional Penyusunan Rencana, Persiapan, Pelaksanaan, dan Pengawasan pada Pengadaan Barang/Jasa Pemerintah secara Swakelola.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('218', 'D3-P.2', 'Mampu melakukan pengelolaan Pengadaan Barang/Jasa secara Swakelola untuk pekerjaan dengan masukan (input) berupa personil dengan keahlian tertentu dan bahan/material umum, proses/metode pelaksanaan telah memiliki standar/pedoman yang spesifik sesuai bidang pekerjaan dengan variasi pelaksanaan tergantung pada kondisi lapangan, dan/atau keluaran (output) yang akan dihasilkan dapat diukur secara kuantitatif dan/atau kualitatif.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('219', 'D4-P.1', 'Mampu melakukan evaluasi efektifitas terhadap penggunaan sumber daya pada Pengadaan Barang/Jasa Pemerintah secara Swakelola.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('220', 'D4-P.2', 'Mampu melakukan evaluasi efektifitas terhadap pencapaian sasaran/tujuan Pengadaan Barang/Jasa  Pemerintah secara Swakelola.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('221', 'D4-P.3', 'Mampu melakukan evaluasi kinerja terhadap instansi pemerintah/organisasi masyarakat/kelompok masyarakat pelaksana swakelola.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('222', 'D4-P.4', 'Mampu melakukan pengelolaan Pengadaan Barang/Jasa secara Swakelola untuk pekerjaan dengan masukan (input) berupa personil dengan keahlian tertentu dan bahan/material spesifik sesuai jenis pekerjaan, proses/metode pelaksanaan mengacu pada kaidah keilmuan dibidang tertentu dengan variasi pelaksanaan yang tinggi, dan/atau keluaran (output) yang akan dihasilkan dapat diukur secara kuantitatif dan/atau diukur secara kualitatif yang ukurannya ditetapkan secara terbuka karena sulit didefinisikan.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('223', 'D5-P.1', 'Mampu melakukan pengembangan konsep, teori, dan kebijakan Penyusunan Rencana dan Persiapan Pengadaan Barang/Jasa Pemerintah secara Swakelola.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('224', 'D5-P.2', 'Mampu melakukan pengembangan konsep, teori, dan kebijakan Pelaksanaan Pengadaan Barang/Jasa Pemerintah secara Swakelola.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('225', 'D5-P.3', 'Mampu melakukan pengembangan konsep, teori, dan kebijakan Pengawasan, Pengendalian, dan Pelaporan Pengadaan Barang/Jasa  Pemerintah secara Swakelola.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `m_perilaku` VALUES ('226', 'D5-P.4', 'Mampu melakukan pengembangan konsep, teori, dan kebijakan tentang Pengadaan Barang/Jasa  secara Swakelola.', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);

-- ----------------------------
-- Table structure for m_program
-- ----------------------------
DROP TABLE IF EXISTS `m_program`;
CREATE TABLE `m_program` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `program` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_program
-- ----------------------------
INSERT INTO `m_program` VALUES ('1', 'PPK', 'Program Pelatihan Kompetensi', '2019-08-25 16:50:04', '2019-08-25 16:50:49', null, '1', null, null);
INSERT INTO `m_program` VALUES ('2', 'PPT', 'Program Pelatihan Teknis', '2019-08-25 16:50:08', '2019-08-25 16:50:45', null, '1', null, null);

-- ----------------------------
-- Table structure for m_regulasi
-- ----------------------------
DROP TABLE IF EXISTS `m_regulasi`;
CREATE TABLE `m_regulasi` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nomor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tentang` text COLLATE utf8mb4_unicode_ci,
  `dokumen` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_regulasi
-- ----------------------------
INSERT INTO `m_regulasi` VALUES ('1', '38', '2017', 'Standar Kompetensi Jabatan Aparatur Sipil Negara', 'files/PERMENPANRB NO 38 Tahun 2017.pdf', '2019-08-18 10:48:22', '2019-08-19 15:25:30', null, '1', null, null);
INSERT INTO `m_regulasi` VALUES ('2', 'xx', '2019', 'Kamus Kompetensi Teknis', 'files/Test dokumen.pdf', '2019-08-22 08:09:49', '2019-08-22 08:09:49', null, '1', null, null);

-- ----------------------------
-- Table structure for m_ruang_lingkup
-- ----------------------------
DROP TABLE IF EXISTS `m_ruang_lingkup`;
CREATE TABLE `m_ruang_lingkup` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ruang_lingkup` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_ruang_lingkup
-- ----------------------------
INSERT INTO `m_ruang_lingkup` VALUES ('1', 'TM.01', 'Identifikasi/Reviu Kebutuhan dan Penetapan Barang/Jasa', '2019-08-23 20:09:35', '2019-08-23 20:37:18', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('2', 'TM.01', 'Penyusunan Spesifikasi Teknis Sederhana', '2019-08-23 20:09:41', '2019-09-01 10:00:54', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('3', 'TM.01', 'Kerangka Acuan Kerja (KAK) Jasa Konsultansi Perorangan', '2019-08-23 20:10:09', '2019-09-01 09:52:11', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('4', 'TM.01', 'Perkiraan Harga Berbasis Harga Pasar, Standar Harga, dan Harga Paket Pekerjaan Sejenis', '2019-08-23 20:10:47', '2019-09-01 10:08:14', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('5', 'TM.01', 'Proses PBJ yang Sederhana', '2019-08-23 20:17:13', '2019-09-01 10:00:58', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('6', 'TM.01', 'Merumuskan Kontrak PBJ dalam Bentuk yang Sederhana', '2019-08-23 20:17:34', '2019-09-01 10:20:55', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('7', 'TM.01', 'Melakukan Pengendalian Kontrak untuk Pekerjaan PBJ yang Sederhana', '2019-08-23 20:17:45', '2019-09-01 10:20:51', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('8', 'TM.01', 'Melaksanakan Evaluasi Kinerja Penyedia PBJ Pemerintah', '2019-08-23 20:18:08', '2019-09-01 09:51:21', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('9', 'TM.01', 'Kegiatan Melaksanakan Tahapan Perencanaan', '2019-08-23 20:19:21', '2019-09-01 10:20:36', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('10', 'TM.01', 'Kegiatan Melaksanakan Tahapan Pelaksanaan', '2019-08-23 20:19:33', '2019-09-01 10:20:26', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('11', 'TM.01', 'Kegiatan Melaksanakan Tahapan Pengawasan', '2019-08-23 20:19:50', '2019-09-01 10:20:20', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('12', 'MD.01', 'Penyusunan Spesifikasi Teknis Tidak Kompleks', '2019-08-23 20:20:12', '2019-09-01 10:01:28', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('13', 'MD.02', 'Penyusunan Kerangka Acuan Kerja (KAK) Pekerjaan Tidak Kompleks', '2019-08-23 20:20:45', '2019-09-01 09:51:37', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('14', 'MD.03', 'Penyusunan Perkiraan Harga Pekerjaan Tidak Kompleks', '2019-08-23 20:21:55', '2019-09-01 09:51:39', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('15', 'MD.04', 'Merumuskan Pemaketan dan Cara Pengadaan', '2019-08-23 20:22:18', '2019-09-01 09:51:41', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('16', 'MD.05', 'Reviu Dokumen Persiapan PBJ yang Tidak Sederhana', '2019-08-23 20:23:07', '2019-09-01 10:04:59', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('17', 'MD.06', 'Proses PBJ yang Tidak Sederhana', '2019-08-23 20:27:45', '2019-09-01 10:04:54', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('18', 'MD.07', 'Merumuskan Kontrak PBJ dalam Bentuk yang Tidak Sederhana', '2019-08-23 20:29:00', '2019-09-01 10:05:23', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('19', 'MD.07', 'Melakukan Pengendalian Kontrak untuk Pekerjaan PBJ yang Tidak Kompleks', '2019-08-23 20:29:18', '2019-09-01 10:05:14', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('20', 'MD.08', 'Analisis dan Memecahkan Masalah Teknis Operasional Pada Tahapan Perencanaan', '2019-08-23 20:29:35', '2019-09-01 10:05:45', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('21', 'MD.09', 'Analisis dan Memecahkan Masalah Teknis Operasional pada Tahapan Pelaksanaan', '2019-08-23 20:29:51', '2019-09-01 10:19:40', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('22', 'MD.10', 'Analisis dan Memecahkan Masalah Teknis Operasional pada Tahapan Pengawasan', '2019-08-23 20:30:01', '2019-09-01 10:19:55', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('23', 'MY.01', 'Penyusunan Spesifikasi Teknis Kompleks', '2019-08-23 20:30:59', '2019-09-01 10:08:47', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('24', 'MY.02', 'Penyusunan Kerangka Acuan Kerja (KAK) Kompleks', '2019-08-23 20:33:30', '2019-09-01 10:08:49', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('25', 'MY.03', 'Penyusunan Perkiraan Harga Pekerjaan Kompleks', '2019-08-23 20:33:41', '2019-09-01 10:08:51', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('26', 'MY.04', 'Merumuskan Strategi Pengadaan dan Organisasi Pengadaan', '2019-08-23 20:34:14', '2019-09-01 10:08:54', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('27', 'MY.05', 'Proses PBJ yang Memiliki Persyaratan Khusus dan/atau Spesifik', '2019-08-23 20:34:47', '2019-09-01 10:09:59', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('28', 'MY.06', 'Merumuskan Kontrak PBJ untuk Pekerjaan yang Memiliki Kriteria/Persyaratan Khusus dan/atau Spesifik', '2019-08-23 20:35:03', '2019-09-01 10:09:44', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('29', 'MY.07', 'Melakukan Pengendalian Kontrak untuk Pekerjaan PBJ yang Kompleks', '2019-08-23 20:35:49', '2019-09-01 10:09:21', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('30', 'MY.08', 'Menyusun Instrumen Evaluasi Kinerja Penyedia PBJ', '2019-08-23 20:36:03', '2019-09-01 10:19:13', null, '1', null, null);
INSERT INTO `m_ruang_lingkup` VALUES ('31', 'MY.09', 'Evaluasi Terhadap Efektifitas Pelaksanaan PBJ', '2019-08-23 20:36:43', '2019-09-01 10:10:13', null, '1', null, null);

-- ----------------------------
-- Table structure for m_sertifikasi
-- ----------------------------
DROP TABLE IF EXISTS `m_sertifikasi`;
CREATE TABLE `m_sertifikasi` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sertifikasi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_sertifikasi
-- ----------------------------
INSERT INTO `m_sertifikasi` VALUES ('1', '001', 'Sertifikasi Pengadaan Barang/Jasa Pemerintah Tingkat Dasar', '2019-08-28 12:55:17', '2019-08-29 09:00:39', null, '1', null, null);
INSERT INTO `m_sertifikasi` VALUES ('2', '002', 'Sertifikasi Kompetensi', '2019-08-28 12:56:54', '2019-08-28 12:56:54', null, '1', null, null);
INSERT INTO `m_sertifikasi` VALUES ('3', '003', 'Sertifikasi Kompetensi Okupasi', '2019-08-28 12:58:57', '2019-08-28 12:58:57', null, '1', null, null);

-- ----------------------------
-- Table structure for m_tingkat_penting
-- ----------------------------
DROP TABLE IF EXISTS `m_tingkat_penting`;
CREATE TABLE `m_tingkat_penting` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tingkat_penting` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_tingkat_penting
-- ----------------------------
INSERT INTO `m_tingkat_penting` VALUES ('1', '001', 'Mutlak', '2019-08-25 18:51:49', '2019-08-25 18:51:49', null, '1', null, null);
INSERT INTO `m_tingkat_penting` VALUES ('2', '002', 'Penting', '2019-08-25 18:52:00', '2019-08-25 18:52:00', null, '1', null, null);
INSERT INTO `m_tingkat_penting` VALUES ('3', '003', 'Perlu', '2019-08-25 18:52:04', '2019-08-25 18:52:13', null, '1', null, null);

-- ----------------------------
-- Table structure for m_unit_kerja
-- ----------------------------
DROP TABLE IF EXISTS `m_unit_kerja`;
CREATE TABLE `m_unit_kerja` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_kerja` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_unit_kerja
-- ----------------------------
INSERT INTO `m_unit_kerja` VALUES ('1', 'D31', 'Bangprof', 'Direktorat Pengembangan Profesi', '2019-08-26 10:28:31', '2019-08-26 10:29:06', null, '1', null, null);
INSERT INTO `m_unit_kerja` VALUES ('2', 'D33', 'DSP', 'Direktorat Sertifikasi Profesi', '2019-08-26 10:28:50', '2019-08-26 10:29:33', null, '1', null, null);
INSERT INTO `m_unit_kerja` VALUES ('3', 'PDL', 'Pusdiklat', 'Pusat Pendidikan dan Pelatihan Pengadaan Barang/Jasa', '2019-08-26 10:28:55', '2019-08-26 10:29:16', null, '1', null, null);

-- ----------------------------
-- Table structure for p_profil_ikhtisar_jabatan
-- ----------------------------
DROP TABLE IF EXISTS `p_profil_ikhtisar_jabatan`;
CREATE TABLE `p_profil_ikhtisar_jabatan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `m_jabatan_id` int(11) NOT NULL,
  `m_kategori_jabatan_id` int(11) NOT NULL,
  `t_ikhtisar_jabatan_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of p_profil_ikhtisar_jabatan
-- ----------------------------
INSERT INTO `p_profil_ikhtisar_jabatan` VALUES ('1', 'JF-P.PBJ01', '1', '10', '1,4,7,10', '2019-08-23 17:28:01', '2019-09-01 10:35:22', null, '1', '1', null);
INSERT INTO `p_profil_ikhtisar_jabatan` VALUES ('2', 'JF-P.PBJ02', '1', '9', '2,5,8,11', '2019-08-23 18:35:31', '2019-09-01 10:36:47', null, '1', '1', null);
INSERT INTO `p_profil_ikhtisar_jabatan` VALUES ('3', 'JF-P.PBJ03', '1', '8', '3,6,9,12', '2019-08-23 19:32:44', '2019-09-01 10:37:08', null, '1', '1', null);

-- ----------------------------
-- Table structure for p_profil_persyaratan_jabatan
-- ----------------------------
DROP TABLE IF EXISTS `p_profil_persyaratan_jabatan`;
CREATE TABLE `p_profil_persyaratan_jabatan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `p_profil_ikhtisar_jabatan_id` int(11) NOT NULL,
  `t_syarat_pendidikan_id` int(11) DEFAULT NULL,
  `t_syarat_pelatihan_manajerial_id` int(11) DEFAULT NULL,
  `tingkat_penting_manajerial` int(11) DEFAULT NULL,
  `t_syarat_pelatihan_teknis_id` int(11) DEFAULT NULL,
  `tingkat_penting_teknis` int(11) DEFAULT NULL,
  `t_syarat_pelatihan_fungsional1` int(11) DEFAULT NULL,
  `tingkat_penting_fungsional1` int(11) DEFAULT NULL,
  `t_syarat_pelatihan_fungsional2` int(11) DEFAULT NULL,
  `tingkat_penting_fungsional2` int(11) DEFAULT NULL,
  `t_syarat_pelatihan_fungsional3` int(11) DEFAULT NULL,
  `tingkat_penting_fungsional3` int(11) DEFAULT NULL,
  `t_syarat_sertifikasi1` int(11) DEFAULT NULL,
  `tingkat_penting_sertifikasi1` int(11) DEFAULT NULL,
  `t_syarat_sertifikasi2` int(11) DEFAULT NULL,
  `tingkat_penting_sertifikasi2` int(11) DEFAULT NULL,
  `t_syarat_pengalaman_kerja_id` int(11) DEFAULT NULL,
  `tingkat_penting_pengalaman_kerja` int(11) DEFAULT NULL,
  `t_syarat_pangkat_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of p_profil_persyaratan_jabatan
-- ----------------------------
INSERT INTO `p_profil_persyaratan_jabatan` VALUES ('1', '1', '1', '1', '3', '1', '3', '1', '3', '2', '3', '3', '3', '1', '1', '2', '1', '1', '1', '1', '2019-08-29 14:52:31', '2019-08-29 14:56:01', null, '1', '1', null);
INSERT INTO `p_profil_persyaratan_jabatan` VALUES ('2', '2', '1', '2', '3', '1', '3', '3', '2', null, null, null, null, '2', '1', null, null, '2', '1', '2', '2019-08-30 08:32:36', '2019-08-30 08:32:36', null, '1', null, null);
INSERT INTO `p_profil_persyaratan_jabatan` VALUES ('3', '3', '1', '3', '3', '1', '2', '3', '2', null, null, null, null, '2', '1', null, null, '3', '1', '3', '2019-08-30 09:02:49', '2019-08-30 09:02:49', null, '1', null, null);

-- ----------------------------
-- Table structure for p_profil_standar_kompetensi
-- ----------------------------
DROP TABLE IF EXISTS `p_profil_standar_kompetensi`;
CREATE TABLE `p_profil_standar_kompetensi` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `p_profil_ikhtisar_jabatan_id` int(11) NOT NULL,
  `t_kompetensi_level_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of p_profil_standar_kompetensi
-- ----------------------------
INSERT INTO `p_profil_standar_kompetensi` VALUES ('1', '1', '2', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('2', '1', '7', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('3', '1', '12', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('4', '1', '17', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('5', '1', '22', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('6', '1', '27', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('7', '1', '32', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('8', '1', '37', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('9', '1', '42', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('10', '1', '47', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('11', '1', '52', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('12', '1', '57', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('13', '1', '62', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('14', '2', '3', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('15', '2', '8', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('16', '2', '13', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('17', '2', '18', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('18', '2', '23', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('19', '2', '28', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('20', '2', '33', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('21', '2', '38', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('22', '2', '43', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('23', '2', '48', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('24', '2', '53', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('25', '2', '58', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('26', '2', '63', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('27', '3', '4', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('28', '3', '9', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('29', '3', '14', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('30', '3', '19', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('31', '3', '24', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('32', '3', '29', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('33', '3', '34', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('34', '3', '39', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('35', '3', '44', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('36', '3', '49', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('37', '3', '54', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('38', '3', '59', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `p_profil_standar_kompetensi` VALUES ('39', '3', '64', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);

-- ----------------------------
-- Table structure for t_ikhtisar_jabatan
-- ----------------------------
DROP TABLE IF EXISTS `t_ikhtisar_jabatan`;
CREATE TABLE `t_ikhtisar_jabatan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `m_kompetensi_jenis_id` int(11) NOT NULL,
  `m_ruang_lingkup_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_ikhtisar_jabatan
-- ----------------------------
INSERT INTO `t_ikhtisar_jabatan` VALUES ('1', 'A01', '10', '1,2,3,4', '2019-09-01 09:48:16', '2019-09-01 09:56:51', null, '1', '1', null);
INSERT INTO `t_ikhtisar_jabatan` VALUES ('2', 'A02', '10', '12,13,14,15', '2019-09-01 10:00:35', '2019-09-01 10:00:35', null, '1', null, null);
INSERT INTO `t_ikhtisar_jabatan` VALUES ('3', 'A03', '10', '23,24,25,26', '2019-09-01 10:07:26', '2019-09-01 10:07:26', null, '1', null, null);
INSERT INTO `t_ikhtisar_jabatan` VALUES ('4', 'B01', '11', '5', '2019-09-01 10:11:11', '2019-09-01 10:11:11', null, '1', null, null);
INSERT INTO `t_ikhtisar_jabatan` VALUES ('5', 'B02', '11', '16,17', '2019-09-01 10:12:45', '2019-09-01 10:12:45', null, '1', null, null);
INSERT INTO `t_ikhtisar_jabatan` VALUES ('6', 'B03', '11', '27', '2019-09-01 10:13:21', '2019-09-01 10:13:21', null, '1', null, null);
INSERT INTO `t_ikhtisar_jabatan` VALUES ('7', 'C01', '12', '6,7,8', '2019-09-01 10:17:28', '2019-09-01 10:17:28', null, '1', null, null);
INSERT INTO `t_ikhtisar_jabatan` VALUES ('8', 'C02', '12', '18,19', '2019-09-01 10:18:20', '2019-09-01 10:18:20', null, '1', null, null);
INSERT INTO `t_ikhtisar_jabatan` VALUES ('9', 'C03', '12', '28,29,30', '2019-09-01 10:19:02', '2019-09-01 10:19:02', null, '1', null, null);
INSERT INTO `t_ikhtisar_jabatan` VALUES ('10', 'D01', '13', '9,10,11', '2019-09-01 10:22:37', '2019-09-01 10:22:37', null, '1', null, null);
INSERT INTO `t_ikhtisar_jabatan` VALUES ('11', 'D02', '13', '20,21,22', '2019-09-01 10:23:08', '2019-09-01 10:23:08', null, '1', null, null);
INSERT INTO `t_ikhtisar_jabatan` VALUES ('12', 'D03', '13', '31', '2019-09-01 10:23:36', '2019-09-01 10:23:36', null, '1', null, null);

-- ----------------------------
-- Table structure for t_kompetensi_jenis
-- ----------------------------
DROP TABLE IF EXISTS `t_kompetensi_jenis`;
CREATE TABLE `t_kompetensi_jenis` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `m_kompetensi_jenis_id` int(11) NOT NULL,
  `t_kompetensi_nama_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_kompetensi_jenis
-- ----------------------------
INSERT INTO `t_kompetensi_jenis` VALUES ('1', '1', '1', '2019-08-24 12:30:11', '2019-08-24 12:30:11', null, '1', null, null);
INSERT INTO `t_kompetensi_jenis` VALUES ('2', '2', '1', '2019-08-24 12:43:08', '2019-08-24 12:43:08', null, '1', null, null);
INSERT INTO `t_kompetensi_jenis` VALUES ('3', '3', '1', '2019-08-24 12:43:18', '2019-08-24 12:43:18', null, '1', null, null);
INSERT INTO `t_kompetensi_jenis` VALUES ('4', '4', '1', '2019-08-24 12:43:27', '2019-08-24 12:43:27', null, '1', null, null);
INSERT INTO `t_kompetensi_jenis` VALUES ('5', '5', '1', '2019-08-24 12:43:36', '2019-08-24 12:43:36', null, '1', null, null);
INSERT INTO `t_kompetensi_jenis` VALUES ('6', '6', '1', '2019-08-24 12:43:49', '2019-08-24 12:43:49', null, '1', null, null);
INSERT INTO `t_kompetensi_jenis` VALUES ('7', '7', '1', '2019-08-24 12:44:15', '2019-08-24 12:44:15', null, '1', null, null);
INSERT INTO `t_kompetensi_jenis` VALUES ('8', '8', '1', '2019-08-24 12:44:22', '2019-08-24 12:44:22', null, '1', null, null);
INSERT INTO `t_kompetensi_jenis` VALUES ('9', '9', '2', '2019-08-24 12:44:33', '2019-08-24 12:44:41', null, '1', '1', null);
INSERT INTO `t_kompetensi_jenis` VALUES ('10', '10', '3', '2019-08-24 12:44:50', '2019-08-24 12:44:50', null, '1', null, null);
INSERT INTO `t_kompetensi_jenis` VALUES ('11', '11', '3', '2019-08-24 15:04:44', '2019-08-24 15:04:44', null, '1', null, null);
INSERT INTO `t_kompetensi_jenis` VALUES ('12', '12', '3', '2019-08-24 15:05:24', '2019-08-24 15:05:24', null, '1', null, null);
INSERT INTO `t_kompetensi_jenis` VALUES ('13', '13', '3', '2019-08-24 15:05:36', '2019-08-24 15:05:36', null, '1', null, null);

-- ----------------------------
-- Table structure for t_kompetensi_level
-- ----------------------------
DROP TABLE IF EXISTS `t_kompetensi_level`;
CREATE TABLE `t_kompetensi_level` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `t_kompetensi_jenis_id` int(11) NOT NULL,
  `m_deskripsi_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_kompetensi_level
-- ----------------------------
INSERT INTO `t_kompetensi_level` VALUES ('1', '1', '1', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('2', '1', '2', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('3', '1', '3', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('4', '1', '4', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('5', '1', '5', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('6', '2', '6', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('7', '2', '7', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('8', '2', '8', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('9', '2', '9', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('10', '2', '10', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('11', '3', '11', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('12', '3', '12', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('13', '3', '13', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('14', '3', '14', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('15', '3', '15', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('16', '4', '16', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('17', '4', '17', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('18', '4', '18', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('19', '4', '19', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('20', '4', '20', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('21', '5', '21', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('22', '5', '22', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('23', '5', '23', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('24', '5', '24', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('25', '5', '25', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('26', '6', '26', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('27', '6', '27', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('28', '6', '28', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('29', '6', '29', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('30', '6', '30', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('31', '7', '31', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('32', '7', '32', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('33', '7', '33', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('34', '7', '34', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('35', '7', '35', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('36', '8', '36', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('37', '8', '37', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('38', '8', '38', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('39', '8', '39', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('40', '8', '40', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('41', '9', '41', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('42', '9', '42', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('43', '9', '43', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('44', '9', '44', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('45', '9', '45', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('46', '10', '46', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('47', '10', '47', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('48', '10', '48', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('49', '10', '49', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('50', '10', '50', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('51', '11', '51', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('52', '11', '52', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('53', '11', '53', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('54', '11', '54', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('55', '11', '55', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('56', '12', '56', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('57', '12', '57', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('58', '12', '58', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('59', '12', '59', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('60', '12', '60', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('61', '13', '61', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('62', '13', '62', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('63', '13', '63', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('64', '13', '64', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_level` VALUES ('65', '13', '65', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);

-- ----------------------------
-- Table structure for t_kompetensi_nama
-- ----------------------------
DROP TABLE IF EXISTS `t_kompetensi_nama`;
CREATE TABLE `t_kompetensi_nama` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `m_regulasi_id` int(11) NOT NULL,
  `m_kompetensi_jabatan_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_kompetensi_nama
-- ----------------------------
INSERT INTO `t_kompetensi_nama` VALUES ('1', '1', '1', '2019-08-24 11:49:05', '2019-08-24 11:49:05', null, '1', null, null);
INSERT INTO `t_kompetensi_nama` VALUES ('2', '1', '2', '2019-08-24 11:53:48', '2019-08-24 11:53:48', null, '1', null, null);
INSERT INTO `t_kompetensi_nama` VALUES ('3', '2', '3', '2019-08-24 11:53:59', '2019-08-24 11:53:59', null, '1', null, null);

-- ----------------------------
-- Table structure for t_kompetensi_perilaku
-- ----------------------------
DROP TABLE IF EXISTS `t_kompetensi_perilaku`;
CREATE TABLE `t_kompetensi_perilaku` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `t_kompetensi_level_id` int(11) NOT NULL,
  `m_perilaku_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=228 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_kompetensi_perilaku
-- ----------------------------
INSERT INTO `t_kompetensi_perilaku` VALUES ('1', '1', '1', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('2', '1', '2', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('3', '1', '3', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('4', '2', '4', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('5', '2', '5', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('6', '2', '6', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('7', '3', '7', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('8', '3', '8', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('9', '3', '9', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('10', '4', '10', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('11', '4', '11', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('12', '4', '12', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('13', '5', '13', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('14', '5', '14', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('15', '5', '15', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('16', '6', '16', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('17', '6', '17', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('18', '6', '18', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('19', '7', '19', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('20', '7', '20', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('21', '7', '21', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('22', '8', '22', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('23', '8', '23', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('24', '8', '24', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('25', '9', '25', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('26', '9', '26', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('27', '9', '27', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('28', '10', '28', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('29', '10', '29', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('30', '10', '30', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('31', '11', '31', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('32', '11', '32', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('33', '11', '33', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('34', '12', '34', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('35', '12', '35', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('36', '12', '36', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('37', '13', '37', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('38', '13', '38', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('39', '13', '39', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('40', '14', '40', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('41', '14', '41', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('42', '14', '42', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('43', '15', '43', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('44', '15', '44', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('45', '15', '45', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('46', '16', '46', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('47', '16', '47', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('48', '16', '48', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('49', '17', '49', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('50', '17', '50', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('51', '17', '51', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('52', '18', '52', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('53', '18', '53', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('54', '18', '54', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('55', '19', '55', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('56', '19', '56', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('57', '19', '57', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('58', '20', '58', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('59', '20', '59', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('60', '20', '60', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('61', '21', '61', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('62', '21', '62', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('63', '21', '63', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('64', '22', '64', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('65', '22', '65', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('66', '22', '66', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('67', '23', '67', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('68', '23', '68', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('69', '23', '69', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('70', '24', '70', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('71', '24', '71', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('72', '24', '72', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('73', '25', '73', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('74', '25', '74', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('75', '25', '75', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('76', '26', '76', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('77', '26', '77', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('78', '26', '78', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('79', '27', '79', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('80', '27', '80', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('81', '27', '81', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('82', '28', '82', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('83', '28', '83', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('84', '28', '84', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('85', '29', '85', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('86', '29', '86', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('87', '29', '87', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('88', '30', '88', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('89', '30', '89', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('90', '30', '90', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('91', '31', '91', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('92', '31', '92', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('93', '31', '93', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('94', '32', '94', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('95', '32', '95', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('96', '32', '96', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('97', '33', '97', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('98', '33', '98', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('99', '33', '99', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('100', '34', '100', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('101', '34', '101', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('102', '34', '102', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('103', '35', '103', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('104', '35', '104', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('105', '35', '105', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('106', '36', '106', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('107', '36', '107', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('108', '36', '108', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('109', '37', '109', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('110', '37', '110', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('111', '37', '111', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('112', '38', '112', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('113', '38', '113', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('114', '38', '114', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('115', '39', '115', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('116', '39', '116', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('117', '39', '117', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('118', '40', '118', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('119', '40', '119', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('120', '40', '120', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('121', '41', '121', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('122', '41', '122', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('123', '41', '123', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('124', '42', '124', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('125', '42', '125', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('126', '42', '126', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('127', '43', '127', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('128', '43', '128', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('129', '43', '129', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('130', '44', '130', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('131', '44', '131', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('132', '44', '132', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('133', '45', '133', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('134', '45', '134', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('135', '45', '135', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('136', '46', '136', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('137', '46', '137', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('138', '46', '138', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('139', '46', '139', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('140', '46', '140', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('141', '46', '141', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('142', '46', '142', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('143', '47', '143', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('144', '47', '144', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('145', '47', '145', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('146', '48', '146', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('147', '48', '147', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('148', '48', '148', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('149', '49', '149', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('150', '49', '150', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('151', '49', '151', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('152', '49', '152', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('153', '50', '153', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('154', '50', '154', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('155', '50', '155', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('156', '50', '156', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('157', '50', '157', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('158', '51', '158', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('159', '51', '159', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('160', '51', '160', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('161', '51', '161', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('162', '51', '162', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('163', '51', '163', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('164', '51', '164', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('165', '52', '165', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('166', '52', '166', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('167', '52', '167', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('168', '52', '168', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('169', '52', '169', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('170', '52', '170', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('171', '53', '171', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('172', '53', '172', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('173', '53', '173', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('174', '53', '174', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('175', '53', '175', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('176', '53', '176', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('177', '53', '177', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('178', '54', '178', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('179', '54', '179', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('180', '54', '180', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('181', '55', '181', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('182', '55', '182', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('183', '55', '183', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('184', '55', '184', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('185', '56', '185', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('186', '56', '186', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('187', '56', '187', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('188', '56', '188', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('189', '56', '189', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('190', '56', '190', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('191', '56', '191', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('192', '56', '192', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('193', '57', '193', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('194', '57', '194', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('195', '57', '195', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('196', '57', '196', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('197', '58', '197', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('198', '58', '198', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('199', '58', '199', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('200', '59', '200', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('201', '59', '201', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('202', '59', '202', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('203', '59', '203', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('204', '59', '204', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('205', '60', '205', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('206', '60', '206', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('207', '60', '207', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('208', '60', '208', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('209', '60', '209', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('210', '61', '210', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('211', '61', '211', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('212', '61', '212', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('213', '61', '213', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('214', '61', '214', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('215', '62', '215', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('216', '62', '216', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('217', '63', '217', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('218', '63', '218', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('219', '64', '219', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('220', '64', '220', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('221', '64', '221', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('222', '64', '222', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('223', '65', '223', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('224', '65', '224', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('225', '65', '225', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('226', '65', '226', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);
INSERT INTO `t_kompetensi_perilaku` VALUES ('227', '65', '227', '2019-08-22 00:00:00', '2019-08-22 00:00:00', null, '1', null, null);

-- ----------------------------
-- Table structure for t_syarat_pangkat
-- ----------------------------
DROP TABLE IF EXISTS `t_syarat_pangkat`;
CREATE TABLE `t_syarat_pangkat` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pangkat_min` int(11) NOT NULL,
  `pangkat_maks` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_syarat_pangkat
-- ----------------------------
INSERT INTO `t_syarat_pangkat` VALUES ('1', '9', '8', '2019-08-25 22:00:42', '2019-08-25 22:00:42', null, '1', null, null);
INSERT INTO `t_syarat_pangkat` VALUES ('2', '7', '6', '2019-08-25 22:04:18', '2019-08-25 22:04:18', null, '1', null, null);
INSERT INTO `t_syarat_pangkat` VALUES ('3', '5', '3', '2019-08-25 22:04:43', '2019-08-25 22:04:43', null, '1', null, null);

-- ----------------------------
-- Table structure for t_syarat_pelatihan_fungsional
-- ----------------------------
DROP TABLE IF EXISTS `t_syarat_pelatihan_fungsional`;
CREATE TABLE `t_syarat_pelatihan_fungsional` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `m_kategori_layanan_id` int(11) NOT NULL,
  `m_pelatihan_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_syarat_pelatihan_fungsional
-- ----------------------------
INSERT INTO `t_syarat_pelatihan_fungsional` VALUES ('1', '3', '5', '2019-08-28 12:27:59', '2019-08-28 12:27:59', null, '1', null, null);
INSERT INTO `t_syarat_pelatihan_fungsional` VALUES ('2', '3', '1', '2019-08-28 12:30:52', '2019-08-29 12:51:59', null, '1', '1', null);
INSERT INTO `t_syarat_pelatihan_fungsional` VALUES ('3', '3', '2', '2019-08-29 12:52:12', '2019-08-29 12:52:12', null, '1', null, null);

-- ----------------------------
-- Table structure for t_syarat_pelatihan_manajerial
-- ----------------------------
DROP TABLE IF EXISTS `t_syarat_pelatihan_manajerial`;
CREATE TABLE `t_syarat_pelatihan_manajerial` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `m_kategori_layanan_id` int(11) NOT NULL,
  `m_pelatihan_id` int(11) NOT NULL,
  `level` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_syarat_pelatihan_manajerial
-- ----------------------------
INSERT INTO `t_syarat_pelatihan_manajerial` VALUES ('1', '1', '11', '2', '2019-08-28 07:50:34', '2019-08-28 07:50:34', null, '1', null, null);
INSERT INTO `t_syarat_pelatihan_manajerial` VALUES ('2', '1', '11', '3', '2019-08-28 07:52:36', '2019-08-28 07:52:36', null, '1', null, null);
INSERT INTO `t_syarat_pelatihan_manajerial` VALUES ('3', '1', '11', '4', '2019-08-28 07:52:49', '2019-08-28 07:52:49', null, '1', null, null);

-- ----------------------------
-- Table structure for t_syarat_pelatihan_teknis
-- ----------------------------
DROP TABLE IF EXISTS `t_syarat_pelatihan_teknis`;
CREATE TABLE `t_syarat_pelatihan_teknis` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `m_kategori_layanan_id` int(11) NOT NULL,
  `m_pelatihan_id` int(11) NOT NULL,
  `level` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_syarat_pelatihan_teknis
-- ----------------------------
INSERT INTO `t_syarat_pelatihan_teknis` VALUES ('1', '2', '12', null, '2019-08-28 12:01:31', '2019-08-28 12:01:31', null, '1', null, null);

-- ----------------------------
-- Table structure for t_syarat_pendidikan
-- ----------------------------
DROP TABLE IF EXISTS `t_syarat_pendidikan`;
CREATE TABLE `t_syarat_pendidikan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `m_jenjang_id` int(11) NOT NULL,
  `m_bidang_ilmu_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_syarat_pendidikan
-- ----------------------------
INSERT INTO `t_syarat_pendidikan` VALUES ('1', '8', '1,2,3,4,5,6,7,8,9,10,11,12,13', '2019-08-31 21:12:17', '2019-08-31 21:54:28', null, '1', '1', null);

-- ----------------------------
-- Table structure for t_syarat_pengalaman_kerja
-- ----------------------------
DROP TABLE IF EXISTS `t_syarat_pengalaman_kerja`;
CREATE TABLE `t_syarat_pengalaman_kerja` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `m_pengalaman_kerja_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_syarat_pengalaman_kerja
-- ----------------------------
INSERT INTO `t_syarat_pengalaman_kerja` VALUES ('1', '1', '2019-08-26 19:38:50', '2019-08-26 19:38:50', null, '1', null, null);
INSERT INTO `t_syarat_pengalaman_kerja` VALUES ('2', '2', '2019-08-26 19:48:17', '2019-08-26 19:48:17', null, '1', null, null);
INSERT INTO `t_syarat_pengalaman_kerja` VALUES ('3', '3', '2019-08-26 19:48:32', '2019-08-26 19:48:32', null, '1', null, null);

-- ----------------------------
-- Table structure for t_syarat_sertifikasi
-- ----------------------------
DROP TABLE IF EXISTS `t_syarat_sertifikasi`;
CREATE TABLE `t_syarat_sertifikasi` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `m_kategori_layanan_id` int(11) NOT NULL,
  `m_sertifikasi_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_syarat_sertifikasi
-- ----------------------------
INSERT INTO `t_syarat_sertifikasi` VALUES ('1', '3', '1', '2019-08-29 09:40:25', '2019-08-29 09:40:25', null, '1', null, null);
INSERT INTO `t_syarat_sertifikasi` VALUES ('2', '3', '2', '2019-08-29 09:41:32', '2019-08-29 09:41:32', null, '1', null, null);
