/*
Navicat MySQL Data Transfer

Source Server         : LOCAL1
Source Server Version : 50532
Source Host           : localhost:3306
Source Database       : ppsdm_pusdiklat

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2019-09-17 10:45:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for m_jenis_pelatihan
-- ----------------------------
DROP TABLE IF EXISTS `m_jenis_pelatihan`;
CREATE TABLE `m_jenis_pelatihan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_pelatihan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_jenis_pelatihan
-- ----------------------------
INSERT INTO `m_jenis_pelatihan` VALUES ('1', '001', 'Manajerial', '2019-08-25 18:00:31', '2019-08-25 18:00:31', null, '1', null, null);
INSERT INTO `m_jenis_pelatihan` VALUES ('2', '002', 'Teknis', '2019-08-25 18:00:57', '2019-08-25 18:01:12', null, '1', null, null);
INSERT INTO `m_jenis_pelatihan` VALUES ('3', '003', 'Fungsional', '2019-08-25 18:01:16', '2019-08-25 18:01:28', null, '1', null, null);

-- ----------------------------
-- Table structure for m_pelatihan
-- ----------------------------
DROP TABLE IF EXISTS `m_pelatihan`;
CREATE TABLE `m_pelatihan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pelatihan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_pelatihan
-- ----------------------------
INSERT INTO `m_pelatihan` VALUES ('1', '001', 'Pelatihan Pembentukan Jabatan Fungsional Pengelola PBJ', '2019-08-25 16:58:13', '2019-08-25 16:58:13', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('2', '002', 'Pelatihan Penjenjangan Jabatan Fungsional Pengelola PBJ', '2019-08-25 16:58:24', '2019-08-25 16:58:42', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('3', '003', 'Pelatihan dalam Jabatan Organisasi (Okupasi) PBJ', '2019-08-25 16:58:46', '2019-08-25 16:59:22', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('4', '004', 'Pelatihan berdasarkan Standar Kompetensi Jabatan', '2019-08-25 17:00:16', '2019-08-25 17:00:42', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('5', '005', 'Pelatihan Keahlian PBJ Tingkat Dasar', '2019-08-25 17:00:54', '2019-08-25 17:01:19', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('6', '006', 'Pelatihan bagi Calon Pelatih (Training of Trainers - TOT) PBJ', '2019-08-25 17:01:22', '2019-08-25 17:01:59', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('7', '007', 'Pelatihan bagi Pengelola Kelas (Training Officer Course)', '2019-08-25 17:02:01', '2019-08-25 17:03:32', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('8', '008', 'Pelatihan bagi Pengelola Pelatihan (Management of Training)', '2019-08-25 17:14:18', '2019-08-25 17:15:06', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('9', '009', 'Pelatihan PBJ di Desa', '2019-08-25 17:15:13', '2019-08-25 17:15:28', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('10', '010', 'Pelatihan Pengadaan Kerja Sama Pemerintah dan Badan Usaha (KPBU)', '2019-08-25 17:15:31', '2019-08-25 17:16:36', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('11', '011', 'Pelatihan Manajerial Level 2', '2019-08-27 13:09:00', '2019-08-27 13:09:07', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('12', '012', 'Pelatihan Manajerial Level 3', '2019-08-27 13:09:15', '2019-08-27 13:09:47', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('13', '013', 'Pelatihan Manajerial Level 4', '2019-08-27 13:09:51', '2019-08-27 13:10:00', null, '1', null, null);
INSERT INTO `m_pelatihan` VALUES ('14', '014', 'Pelatihan Teknis Pendukung', '2019-08-27 13:17:31', '2019-08-27 13:17:31', null, '1', null, null);

-- ----------------------------
-- Table structure for m_program
-- ----------------------------
DROP TABLE IF EXISTS `m_program`;
CREATE TABLE `m_program` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `program` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of m_program
-- ----------------------------
INSERT INTO `m_program` VALUES ('1', 'PPK', 'Program Pelatihan Kompetensi', '2019-08-25 16:50:04', '2019-08-25 16:50:49', null, '1', null, null);
INSERT INTO `m_program` VALUES ('2', 'PPT', 'Program Pelatihan Teknis', '2019-08-25 16:50:08', '2019-08-25 16:50:45', null, '1', null, null);
