@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Selangkah lagi, lengkapi data diri Anda di bawah ini ...') }}</div>
                
                <div class="card-body">
                    <form method="POST" action="{{ route('konfirmasi') }}">
                        @csrf

                        @if(session()->has('message'))
                        <div class="form-group row">
                            <div class="col-md-12 alert alert-success">
                              {{ session()->get('message') }}
                            </div>
                        </div>
                        @endif

                        <div class="form-group row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Nama Lengkap') }}</label>

                                    <div class="col-md-8">
                                        <input id="nama" type="text" placeholder="tanpa gelar" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama') }}" required autocomplete="nama" autofocus>

                                        @error('nama')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="tmp_lahir" class="col-md-4 col-form-label text-md-right">{{ __('Tempat Lahir') }}</label>

                                    <div class="col-md-8">
                                        <input id="tmp_lahir" type="text" class="form-control @error('tmp_lahir') is-invalid @enderror" name="tmp_lahir" value="{{ old('tmp_lahir') }}" required>

                                        @error('tmp_lahir')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="tgl_lahir" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Lahir') }}</label>

                                    <div class="col-md-8">
                                        <input id="tgl_lahir" type="text" class="form-control @error('tgl_lahir') is-invalid @enderror" name="tgl_lahir" value="{{ old('tgl_lahir') }}" required>

                                        @error('tgl_lahir')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="status" class="col-md-4 col-form-label text-md-right">{{ __('Status Kepegawaian') }}</label>

                                    <div class="col-md-8">
                                        <input id="status" type="text" class="form-control @error('status') is-invalid @enderror" name="status" required>

                                        @error('status')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="nip" class="col-md-4 col-form-label text-md-right">{{ __('NIP') }}</label>

                                    <div class="col-md-8">
                                        <input id="nip" type="text" class="form-control" name="nip" required>

                                        @error('nip')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div> 

                                <div class="form-group row">
                                    <label for="instansi" class="col-md-4 col-form-label text-md-right">{{ __('Instansi') }}</label>

                                    <div class="col-md-8">
                                        <input id="instansi" type="text" class="form-control @error('instansi') is-invalid @enderror" name="instansi" value="{{ old('instansi') }}" required>

                                        @error('instansi')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!-- right column -->
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="no_hp" class="col-md-4 col-form-label text-md-right">{{ __('No. HP') }}</label>

                                    <div class="col-md-8">
                                        <input id="no_hp" type="text" class="form-control @error('no_hp') is-invalid @enderror" name="no_hp" value="{{ old('no_hp') }}" required>

                                        @error('no_hp')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="alamat" class="col-md-4 col-form-label text-md-right">{{ __('Alamat') }}</label>

                                    <div class="col-md-8">
                                        <input id="alamat" type="text" class="form-control @error('alamat') is-invalid @enderror" name="alamat" value="{{ old('alamat') }}" required autocomplete="alamat">

                                        @error('alamat')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="prov" class="col-md-4 col-form-label text-md-right">{{ __('Provinsi') }}</label>

                                    <div class="col-md-8">
                                        <input id="prov" type="text" class="form-control @error('prov') is-invalid @enderror" name="prov" value="{{ old('prov') }}" required autocomplete="prov">

                                        @error('prov')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="kota" class="col-md-4 col-form-label text-md-right">{{ __('Kota') }}</label>

                                    <div class="col-md-8">
                                        <input id="kota" type="text" class="form-control @error('kota') is-invalid @enderror" name="kota" value="{{ old('kota') }}" required autocomplete="kota">

                                        @error('kota')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-block btn-primary">
                                            {{ __('Kirim') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
